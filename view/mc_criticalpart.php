<div id="section">
	<?php
		session_start();
		$session_value=(isset($_SESSION['username']))?$_SESSION['username']:'';
		include_once 'history_mc.php';
		include_once 'mc_expired.php';
		include_once 'mc_issue.php';
	?>
</div>
<script type="text/javascript">
	Ext.Loader.setConfig({enabled:true});
	Ext.Loader.setPath('Ext.ux','../../framework/extjs-5.1.1/build/examples/ux');

	Ext.onReady(function(){
		var tab_mccritical = Ext.create('Ext.tab.Panel',{
			activeTab 	: 0, // remove after finish develop
			tabRotation	: 0,
			tabPosition	: 'top',
			plain 		: true,
			tabBar 		: {
		        flex 	: 1,
		        layout 	: {
		        	pack 			: 'center',
		            align 			: 'stretch',
		            overflowHandler : 'none'
		        }
		    },
			defaults: {
				bodyPadding: 5,
				bodyStyle: {
	            	background: '#ADD2ED'
	            },
			},
			items: [
		        {
		        	title: 'ISSUE PART',
		        	layout: 'fit',
		        	items: tab_mcissue
		        },
				{
		            title: 'REGISTER PART',
		            layout: 'fit',
		            items: tab_mcexpired
		        },
				{
		        	title: 'HISTORIES',
		        	layout: 'fit',
		        	items: tab_histories
		        },
				{
		        	title: 'MANUAL INSTRUCTION',
					layout: 'fit',
					items: { 
							xtype: 'box', 
							autoEl: { 
								tag: 'iframe', 
								style: 'height: 100%; width: 100%', 
								src: 'resources/WI.pdf'
							} 
						}
		        }
		        
		  //       {
		  //       	title: 'ADMINISTRATOR',
		  //       	layout: 'fit',
		  //       	items: [win_login,win_logout],
		  //       	id: 'form-login',
		  //       	listeners: {
				// 		activate: function() {
				// 			console.log('Activated');
				// 		}
				// 	}
				// }
		    ]
		});

		Ext.create('Ext.container.Viewport', {
		    layout: 'border',
		    renderTo: 'section',
		    defaults: {
		    	split: true
		    },
		    items: [{
		        region: 'north',
		        html: '<h1 class="x-panel-header" style="text-align:center;">CRITICAL PART CONTROL</h1>',
		        bodyStyle: {
		        	background: '#157FCC',
		        	color: '#ffffff'
		        },
		        height: 50,
		        maxHeight: 50,
		        minHeight: 50,
		    }, {
		        region: 'center',
		        layout: 'fit',
		        bodyStyle: {
		        	background: '#4B9CD7',
		        	color: '#ffffff'
		        },
		        items: tab_mccritical
		    }],
		    listeners: {
		    	render: function() {
		    		// console.log('berak');
		    		// var username = '<?php //echo $session_value;?>';

		   //  		if ( username == null || username == '') {
					// 	win_logout.hide();
					// 	Ext.getCmp('delete-master').setHidden(true);
					// 	console.log('logout');
					// } else {
					// 	win_login.hide();
					// 	Ext.getCmp('delete-master').setHidden(false);
					// 	console.log('login');
					// }
		    	}
		    }
		});
	})
</script>