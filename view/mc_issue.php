<?php
    include_once('mc_oll.php');
    include_once('mc_issueRecord.php');
?>
<script type="text/javascript">
	
	var tab_mcissue = Ext.create('Ext.tab.Panel',{
		activeTab    : 0
		,plain       : true
		,tabePosition: 'top'
		,tabBar      : {
			flex     : 1
			,layout  : {
				pack    : 'center'
				,align  : 'stretch'
			}
		}
		,defaults    : {
			bodyStyle: 'background: #ADD2ED',
		}
		,items      : [
            {
                title   : 'START ISSUE',
                layout  : 'fit',
                items   : panel_issueOll
            }
            ,{
                title   : 'ISSUE RECORD',
                layout  : 'fit',
                items   : panel_issuePart
            }
        ]
	});

</script>
<style type="text/css">
	.settings {
		height: 64px;
	}
</style>