
<?php
	include_once 'mc_expDuration.php';
	include_once 'mc_expMaster.php';
	include_once 'mc_expData.php';
?>
<script type="text/javascript">
	
	var tab_mcexpired = Ext.create('Ext.tab.Panel',{
		activeTab: 0,
		plain: true,
		tabePosition: 'top',
		tabBar: {
			flex: 1,
			layout: {
				pack: 'center',
				align: 'stretch',
				// overflowHandler: 'none'
			}
		},
		defaults: {
			bodyStyle: 'background: #ADD2ED',
		},
		items: [
		{
			title: 'REGISTER BARCODE',
			layout: 'fit',
			items: panel_expData
		},
		{
			title: 'SUPPLIER EXPIRED',
			layout: 'fit',
			items: panel_expMaster
		},
		{
			title: 'DURATION AFTER BAKING',
			layout: 'fit',
			items: panel_expUpdate
		}, 
		]
	});

</script>
<style type="text/css">
	.settings {
		height: 64px;
	}
</style>