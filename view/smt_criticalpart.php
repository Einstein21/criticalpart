<!--
	control_drypart.php
	control_drpbaking.php
	control_detail.php
	control_setpart.php
-->

<div id="section">
	<?php
		session_start();
		$session_value=(isset($_SESSION['username']))?$_SESSION['username']:'';
		include_once 'control_drypart.php';
	?>
</div>
<script type="text/javascript">
	Ext.Loader.setConfig({enabled:true});
	Ext.Loader.setPath('Ext.ux','../../framework/extjs-5.1.1/build/examples/ux');

	Ext.onReady(function(){
		var win_login = Ext.create('Ext.form.Panel',{
			title: 'FORM LOGIN',
			layout: {
				type: 'vbox',
			    pack: 'center',
			    align: 'center'
			},
			items: [{
				xtype: 'textfield',
	            name: 'username',
	            fieldLabel: 'User Name',
	            allowBlank: false
			},{
				xtype: 'textfield',
	            name: 'password',
	            fieldLabel: 'Password',
	            inputType: 'password',
	            allowBlank: false
			},{
				xtype: 'button',
                formBind: true,
                disabled: true,
                scale: 'medium',
                text: 'LOGIN',
                width: 280,
                handler: function() {
                    var form = this.up('form').getForm();
					if (form.isValid()) {
						form.submit({
							url: 'login.php',
							waitMsg: 'Check for authentication, Please wait..',
							success: function(form, action) {
								Ext.Msg.show({
		                        	title   : 'SUCCESS',
		                        	msg     : action.result.msg,
		                        	buttons : Ext.Msg.OK
		                        });
		                        form.reset();
		                        location.reload();
		                        // Ext.getCmp('form-login').hide();
		                        // Ext.getCmp('form-logout').show();
		                        console.log(action.result);

		                    },
		                    failure : function(form, action) {
		                        Ext.Msg.show({
			                        title   : 'WARNING',
			                        icons   : Ext.Msg.ERROR,
			                        msg     : action.result.msg,
			                        buttons : Ext.Msg.OK
		                        });
		                        console.log(action.result);
		                    }
						});
					}
				
                }
			}]
		});

		win_logout = Ext.create('Ext.form.Panel',{
			title: 'FORM LOGOUT',
			layout: {
				type: 'vbox',
			    pack: 'center',
			    align: 'center'
			},
			items: [{
				html: '<h1>SILAKAN KLIK TOMBOL DI BAWAH UNTUK LOGOUT</h1>'
			},{
				xtype: 'button',
                // formBind: true,
                // disabled: true,
                scale: 'medium',
                text: 'LOGOUT',
                // width: 280,
                handler: function() {
                    var form = this.up('form').getForm();
					if (form.isValid()) {
						form.submit({
							url: 'logout.php',
							waitMsg: 'Check for authentication, Please wait..',
							success: function(form, action) {
								Ext.Msg.show({
		                        	title   : 'SUCCESS',
		                        	msg     : action.result.msg,
		                        	buttons : Ext.Msg.OK
		                        });
		                        // form.reset();
		                        location.reload();
		                        // Ext.getCmp('form-login').hide();
		                        // Ext.getCmp('form-logout').show();
		                        // console.log(action.result);

		                    },
		                    failure : function(form, action) {
		                        Ext.Msg.show({
			                        title   : 'WARNING',
			                        icons   : Ext.Msg.ERROR,
			                        msg     : action.result.msg,
			                        buttons : Ext.Msg.OK
		                        });
		                        // console.log(action.result);
		                    }
						});
					}
				
                }
			}]
		});

		var tab_exp = Ext.create('Ext.tab.Panel',{
			activeTab: 1, // remove after finish develop
			tabRotation: 0,
			tabPosition: 'top',
			plain: true,
			tabBar: {
		        flex: 1,
		        layout: {
		        	pack: 'center',
		            align: 'stretch',
		            overflowHandler: 'none'
		        }
		    },
			defaults: {
				bodyPadding: 5,
				bodyStyle: {
	            	background: '#ADD2ED'
	            },
			},
			items: [
		        {
		        	title: 'DRY PART CONTROL',
		        	layout: 'fit',
		        	items: tab_drypart
		        },
		        {
		        	title: 'ADMINISTRATOR',
		        	layout: 'fit',
		        	items: [win_login,win_logout],
		        	id: 'form-login',
		        	listeners: {
						activate: function() {
							console.log('Activated');
						}
					}
				}
		    ]
		});

		Ext.create('Ext.container.Viewport', {
		    layout: 'border',
		    renderTo: 'section',
		    defaults: {
		    	split: true
		    },
		    items: [{
		        region: 'north',
		        html: '<h1 class="x-panel-header" style="text-align:center;">CRITICAL PART CONTROL</h1>',
		        bodyStyle: {
		        	background: '#157FCC',
		        	color: '#ffffff'
		        },
		        height: 50,
		        maxHeight: 50,
		        minHeight: 50,
		    }, {
		        region: 'center',
		        layout: 'fit',
		        bodyStyle: {
		        	background: '#4B9CD7',
		        	color: '#ffffff'
		        },
		        items: tab_exp
		    }],
		    listeners: {
		    	render: function() {
		    		var username = '<?php echo $session_value;?>';

		    		if ( username == null || username == '') {
						win_logout.hide();
						Ext.getCmp('delete-master').setHidden(true);
						console.log('logout');
					} else {
						win_login.hide();
						Ext.getCmp('delete-master').setHidden(false);
						console.log('login');
					}
		    	}
		    }
		});
	})
</script>