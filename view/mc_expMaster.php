<script type="text/javascript">
	function combineInput(value, meta, record, rowIndex, colIndex, store)  {
		value2 = record.get('CREATED_NAME');
		value3 = record.get('CREATED_AT');
		if (value == '---' || value == '-' || value == '' || value == null) {
			return '<font style="white-space:normal; line-height:1.5; color:red;"> _ </font>';
		}
		else{
			return '<font style="color:blue;">' + value3 + '</font><br><font style="color:gray;">' + value + '</font><font> '+ value2 +'</font>';
		}
	}
	function combineUpdate(value, meta, record, rowIndex, colIndex, store)  {
		value2 = record.get('UPDATED_NAME');
		value3 = record.get('UPDATED_AT');
		if (value == '---' || value == '-' || value == '' || value == null) {
			return '<font style="white-space:normal; line-height:1.5; color:red;"> _ </font>';
		}
		else{
			return '<font style="color:blue;">' + value3 + '</font><br><font style="color:gray;">' + value + '</font><font> - '+ value2 +'</font>';
		}
	}
	function combineSupp(value, meta, record, rowIndex, colIndex, store)  {
		value2 = record.get('SUPPNAME');
		if (value == '---' || value == '-' || value == '' || value == null) {
			return '<font style="white-space:normal; line-height:1.5; color:red;"> _ </font>';
		}
		else{
			return '<font style="color:gray;">' + value + '</font><br><font>'+ value2 +'</font>';
		}
	}
	function getPlural(number, word){
		return number === 1 && word.one || word.other;
	}
	function addmonth(value, meta, record, rowIndex, colIndex, store){
		if (value == '---' || value == '-' || value == '' || value == null) {
			return '<font style="white-space:normal; line-height:1.5; color:red;"> _ </font>';
		}
		else {
			var months 	= {"one":"MONTH" , "other":"MONTHS"},
				years 	= {"one":"YEAR" , "other":"YEARS"},
				m = value % 12,
				y = Math.floor(value / 12),
				before,after,
				result = [];

			y && result.push(y + ' ' + getPlural(y,years));
			m && result.push(m + ' ' + getPlural(m,months));
			console.log(result);
			return result.join(' AND ');
		}
	}

	var store_expMaster = Ext.create('Ext.data.Store',{
		fields  	:[]
		,autoLoad 	: true
		,pageSize 	: 20
		,proxy 		: { type 	: 'ajax',
						url 	: 'json/mc_dispExpMaster.php',
						reader	: {
							type 			: 'json',
							root    		: 'rows',
							totalProperty	: 'TOTCOUNT'
						}
					}
	});

	var toolbar_expMaster = Ext.create('Ext.toolbar.Toolbar',{
		dock:'bottom',
		ui: 'footer',
		defaults: {
			defaultType: 'button',
			scale: 'large'
		},
		items: [
			/*{	name: 'update',
				icon: 'resources/save.png',
				handler: function() {
					var getForm = this.up('form').getForm();
					if (getForm.isValid()) {
						getForm.submit({
							url: 'response/updateExp.php',
							waitMsg : 'Now transfering data, please wait..',
							success : function(form, action) {
		                        Ext.Msg.show({
		                        	title   : 'SUCCESS',
		                        	msg     : action.result.msg,
		                        	buttons : Ext.Msg.OK
		                        });
		                        exp_control.loadPage(1);
		                     },
		                    failure : function(form, action) {
		                        Ext.Msg.show({
			                        title   : 'OOPS, AN ERROR JUST HAPPEN !',
			                        icons   : Ext.Msg.ERROR,
			                        msg     : action.result.msg,
			                        buttons : Ext.Msg.OK
		                        });
		                      }
						});
					}
				}
			},*/
			{	name: 'delete',
				icon: 'resources/delete.png',
				handler: function() {
					var rec = grid_expMaster.getSelectionModel().getSelection();
					var len = rec.length;
					if (rec == 0) {
						Ext.Msg.show({
							title: 'Failure - Select Data',
							icon: Ext.Msg.ERROR,
							msg: 'Select any field you desire to delete',
							buttons: Ext.Msg.OK
						});
					} else {
						Ext.Msg.confirm('Confirm', 'Are you sure want to delete data ?', function(btn) {
							if(btn == 'yes') {
								for (var i=0;i<len;i++) {
									Ext.Ajax.request({
										url: 'response/mc_delExpMst.php',
										method: 'POST',
										params: 'id='+rec[i].data.ID,
										success: function(obj) {
											var resp = obj.responseText;
											if(resp !=0) {
												store_expMaster.loadPage(1);
											} else {
												Ext.Msg.show({
													title: 'Delete Data',
													icon: Ext.Msg.ERROR,
													msg: resp,
													buttons: Ext.Msg.OK
												});
											}
										}
									});
								}
							}
						});
					}
				}
			},
			/*{	name: 'print',
				icon: 'resources/print.png',
				handler	: function(widget, event) {
					var rec = grid_exp.getSelectionModel().getSelection();
					var len = rec.length;
					
					if(len == "") {
						Ext.Msg.show({
							title		:'Message',
							icon		: Ext.Msg.ERROR,
							msg			: "No data selected.",
							buttons		: Ext.Msg.OK
						});
					} else {
						//	get selected data
						var i = 0; // initial variable for looping
						var a = ''; // empty string 
						var b = ''; // empty string
						var total = 0;
						
						for (var i=0; i < len; i++) {
							cb 	= a + '' + rec[i].data.id;
							a 	= a + '' + rec[i].data.id + '/';
							
							lbl = b + '' + rec[i].data.id;
							b 	= b + '' + rec[i].data.id + ', ';
							
							total++;
						}
						window.open ("response/printExp_sato.php?total="+total+"&cb="+cb+"");
					}
				}
			},*/
			'->',
			{	name: 'create',
				icon: 'resources/create.png',
				formBind: true,
				handler: function() {
					var getForm = this.up('form').getForm();
					if (getForm.isValid()) {
						getForm.submit({
							url: 'response/mc_inputExpMst.php',
							waitMsg : 'Now transfering data, please wait..',
							success : function(form, action) {
		                        store_expMaster.loadPage(1);
		                        Ext.Msg.show({
		                        	title   : 'SUCCESS',
		                        	msg     : action.result.msg,
		                        	buttons : Ext.Msg.OK
		                        });
		                     },
		                    failure : function(form, action) {
		                        Ext.Msg.show({
			                        title   : 'OOPS, AN ERROR JUST HAPPEN !',
			                        icons   : Ext.Msg.ERROR,
			                        msg     : action.result.msg,
			                        buttons : Ext.Msg.OK
		                        });
		                      }
						});
					}
				}
			}, 
			{	name: 'reset',
				icon: 'resources/reset.png',
				handler: function() {
					this.up('form').getForm().reset();
					Ext.ComponentQuery.query('textfield[name=src_expMstSupplier]')[0].reset();
					Ext.ComponentQuery.query('textfield[name=src_expMstDuration]')[0].reset();
					Ext.ComponentQuery.query('textfield[name=mc_expMstNik]')[0].focus(true,1);
					Ext.ComponentQuery.query('textfield[name=mc_expMstNik]')[0].setEditable(true);
					store_expMaster.proxy.setExtraParam('supplier','');
					store_expMaster.proxy.setExtraParam('duration','');
					store_expMaster.loadPage(1);
				}
			}
		]
	});

	var form_expMaster = Ext.create('Ext.form.Panel',{
			name: 'form_expMaster',
			layout: 'anchor',
			width: 400,
			bodyStyle: {
	        	background: 'rgba(255, 255, 255, 0)'
	        },
		    defaults: {
		        anchor: '100%',
		        labelWidth: 150
		    },
		    defaultType: 'textfield',
		    items: [
			    /*{
			    	xtype: 'hiddenfield',
			    	name: 'unid'
			    },
			    {
			    	xtype: 'hiddenfield',
			    	name: 'stdpack',
			    	listeners: {
			    		change: function(field) {
			    			if (field.getValue() == 999999) {
			    				Ext.Msg.alert('WARNING','Please update Std. Packing !');
			    				Ext.ComponentQuery.query('combobox[name=partno]')[0].reset();
			    				field.reset();
			    			}
			    		}
			    	}
			    },*/
			    {
			    	fieldLabel: 'NIK',
			    	name: 'mc_expMstNik',
			    	allowBlank: false,
			    	afterLabelTextTpl: required,
			    	minLength: 5,
			    	// maxLength: 8,
			    	emptyText: 'SCAN NIK HERE...',
			    	listeners: {
		                afterrender: function(field) { field.focus(true,500); },
		                specialkey: function(field, e) {
							if (e.getKey() == e.ENTER) {
								Ext.ComponentQuery.query('textfield[name=mc_expMstSupplier]')[0].focus(true,1);
							}
		                },
		                change:function(field){
			                field.setValue(field.getValue().toUpperCase());
			            }
		            }
			    },
			    {
			    	xtype		: 'combobox',
			    	name		: 'mc_expMstSupplier',
			    	fieldLabel	: 'SUPPLIER',
			    	store 		: exp_supp,
			    	queryMode	: 'proxy',
			    	queryParam	: 'supplier',
	                displayField: 'suppname',
	                valueField	: 'suppcode',
	                allowBlank	: false,
	                afterLabelTextTpl: required,
	                listConfig	: {
	                    loadingText	: 'Searching...',
	                    emptyText	: 'No data found.',
	                    getInnerTpl	: function() {
	                        return '<div> {suppname} &#8212; <b>( {suppcode} )</b></div>';
	                    }
	                },
	                listeners	: {
	                	select: function(field) {
	                		var val = field.getValue();
	                		exp_part.proxy.setExtraParam('supplier', val); // set parameter for exp_part store
	                		exp_part.load();
	                	},
	                	specialkey: function(field, e) {
							if (e.getKey() == e.ENTER) {
								Ext.ComponentQuery.query('textfield[name=mc_expMstDuration]')[0].focus(true,1);
							}
		                }
	                }
			    },
			    {
					xtype		: 'textfield',
					name		: 'mc_expMstDuration',
					fieldLabel	: 'DURATION ( MONTH )',
					maskRe 		: /[0-9.,]/,
	                allowBlank	: false,
	                afterLabelTextTpl: required,
					listeners	: {
				    	change:function(field){
			                field.setValue(field.getValue().toUpperCase());
			            },
			            specialkey: function(field, e) {
							if (e.getKey() == e.ENTER) {
								Ext.ComponentQuery.query('textfield[name=mc_expMstRemark]')[0].focus(true,1);
							}
		                }
				    }
				},
			    {
					xtype: 'textareafield',
					name: 'mc_expMstRemark',
					fieldLabel: 'REMARK',
					grow: true,
					listeners: {
				    	change:function(field){
			                field.setValue(field.getValue().toUpperCase());
			            }
				    }
				}
			],
		    dockedItems: [toolbar_expMaster]
	});

	var grid_expMaster = Ext.create('Ext.grid.Panel', {
		store 		: store_expMaster,
		selModel 	: Ext.create('Ext.selection.CheckboxModel'),
	    viewConfig 	: { enableTextSelection  : true },
	    width 		: 400,
	    columns 	: [
	    	{ header: 'NO', xtype: 'rownumberer', width: 55, sortable: false },
	    	{ text: 'ID', dataIndex: 'id', flex: 1, hidden:true },
	    	{ 	text 		: 'SUPPLIER'
	    		,dataIndex	: 'SUPPCODE'
	    		,flex 		: 1
	    		,renderer 	: combineSupp
	    		,layout 	: { type  :'hbox'
	    						,align :'stretch'
								,pack  :'center' }
	    		,items 		: [{ 	xtype 		: 'textfield'
									,name 		: 'src_expMstSupplier'
									,emptyText 	: 'Search...'
									,flex       : 1
									,margin     : '0 10 10 10'
									,listeners 	: {
					    				specialkey: function(field, e) {
											if (e.getKey() == e.ENTER) {
												store_expMaster.proxy.setExtraParam('supplier', field.getValue());
												store_expMaster.proxy.setExtraParam('duration',Ext.ComponentQuery.query('textfield[name=src_expMstDuration]')[0].getValue());
												store_expMaster.loadPage(1);
											}
				                		}
					    			}
								}]
    		},
	    	{ 	text 		: 'EXPIRED DURATION' 
    			,dataIndex 	: 'EXPPERIOD'
    			,flex 		: 1
    			,renderer 	: addmonth
    			,align 		: 'right'
    			,layout 	: { type   :'hbox'
	    						,align :'stretch'
								,pack  :'center' }
	    		,items 		: [{ 	xtype 		: 'textfield'
									,name 		: 'src_expMstDuration'
									,emptyText 	: 'Search...'
									,flex       : 1
									,margin     : '0 10 10 10'
									,listeners 	: {
					    				specialkey: function(field, e) {
											if (e.getKey() == e.ENTER) {
												store_expMaster.proxy.setExtraParam('supplier', Ext.ComponentQuery.query('textfield[name=src_expMstSupplier]')[0].getValue());
												store_expMaster.proxy.setExtraParam('duration', field.getValue());
												store_expMaster.loadPage(1);
											}
				                		}
					    			}
								}] 
    		},
	    	{ text: 'REMARK', dataIndex: 'REMARK', flex: 1 },
			{ 	header 		: 'CREATE BY',	
				dataIndex 	: 'CREATED_BY',	
				componentCls: 'headergrid',		
				flex 		: 1, 	
				renderer 	: combineInput
			},
			{ 	header  	: 'UPDATE BY', 	
				dataIndex 	: 'UPDATED_BY', 	
				componentCls: 'headergrid',		
				flex 		: 1,
				hidden 		: true, 	
				renderer 	: combineUpdate
			}
	    ],
	    bbar: {
	    	xtype: 'pagingtoolbar',
	    	displayInfo	: true,
	    	store: store_expMaster,
	    	/*items: ['->',{
	    		xtype: 'textfield',
	    		name: 'detail_fldsrc',
	    		width: 600,
	    		emptyText: 'Search part number in here...',
	    		fieldStyle: 'text-align:center;',
	    		listeners: {
	    			specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							store_expMaster.proxy.setExtraParam('detail_fldsrc',field.getValue());
							store_expMaster.loadPage(1);
							// console.log(field.value);
						}
	                },
	                change:function(field){
		                field.setValue(field.getValue().toUpperCase());
		            }
	    		}
	    	}]*/
	    },
	    listeners: {
	    	select: function(grid, rowIndex, colIndex) {
                var rec = this.getSelectionModel().getSelection();
            	
                // var valnik 		= rec[0].data.CREATED_BY;
            	var valsuppcode = rec[0].data.SUPPCODE;
            	var valexpPeriod= rec[0].data.EXPPERIOD;
            	var valremark 	= rec[0].data.REMARK;
                
               	// var txt_nik 	= Ext.ComponentQuery.query('textfield[name=mc_expMstNik]')[0];
                var txt_supp 	= Ext.ComponentQuery.query('combobox[name=mc_expMstSupplier]')[0];
                var txt_duration= Ext.ComponentQuery.query('textfield[name=mc_expMstDuration]')[0];
                var txt_remark 	= Ext.ComponentQuery.query('textfield[name=mc_expMstRemark]')[0];

                // txt_nik.setValue(valnik);
                txt_supp.setValue(valsuppcode);
                txt_duration.setValue(valexpPeriod);
                txt_remark.setValue(valremark);
                
          		// exp_part.proxy.setExtraParam('supplier', suppcode); // set parameter for exp_part store
        		// exp_part.load();
        		Ext.ComponentQuery.query('textfield[name=mc_expMstNik]')[0].focus(true,1);
                // Ext.ComponentQuery.query('textfield[name=nik]')[0].setEditable(false);
            }
	    }
	});

	var panel_expMaster = Ext.create('Ext.panel.Panel',{
		border: true,
		layout: 'border',
	    defaults: {
	    	split: false,
	    	plain: true
	    },
	    items: [{
	        region: 'north',
	        layout: {
	        	type: 'hbox',
	        	pack: 'center'
	        },
	        bodyPadding: 10,
	        bodyStyle: {
	        	background 		: 'url("resources/bg-image.jpg") no-repeat center left',
	        	backgroundSize: 'cover'
	        },
	        items: form_expMaster
	    }, {
	        region: 'center',
	        layout: 'fit',
	        items: grid_expMaster
	    }]
	});

</script>