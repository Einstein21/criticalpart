<script type="text/javascript">
	function combineInput(value, meta, record, rowIndex, colIndex, store)  {
		value2 = record.get('CREATED_NAME');
		value3 = record.get('CREATED_AT');
		if (value == '---' || value == '-' || value == '' || value == null) {
			return '<font style="white-space:normal; line-height:1.5; color:red;"> _ </font>';
			}
		else{
			return '<font style="color:blue;">' + value3 + '</font><br><font style="color:gray;">' + value + '</font><font> '+ value2 +'</font>';
			}
		}	
	function combineUpdate(value, meta, record, rowIndex, colIndex, store)  {
		value2 = record.get('UPDATED_NAME');
		value3 = record.get('UPDATED_AT');
		if (value == '---' || value == '-' || value == '' || value == null) {
			return '<font style="white-space:normal; line-height:1.5; color:red;"> _ </font>';
			}
		else{
			return '<font style="color:blue;">' + value3 + '</font><br><font style="color:gray;">' + value + '</font><font> - '+ value2 +'</font>';
			}
		}
	function addmonth(value, meta, record, rowIndex, colIndex, store){
		if (value == '---' || value == '-' || value == '' || value == null) {
			return '<font style="white-space:normal; line-height:1.5; color:red;"> _ </font>';
			}
		else {
			var months 	= {"one":"MONTH" , "other":"MONTHS"},
				years 	= {"one":"YEAR" , "other":"YEARS"},
				m = value % 12,
				y = Math.floor(value / 12),
				before,after,
				result = [];

			y && result.push(y + ' ' + getPlural(y,years));
			m && result.push(m + ' ' + getPlural(m,months));
			// console.log(result);
			// console.log(months);
			return result.join(' AND ');
			}
		}
	var store_expDuration 	= Ext.create('Ext.data.Store',{
		fields  	:[]
		,autoLoad 	: true
		,pageSize 	: 20
		,proxy 		: { type 	: 'ajax',
						url 	: 'json/mc_dispExpDuration.php',
						reader	: {
							type 			: 'json',
							root    		: 'rows',
							totalProperty	: 'TOTCOUNT'
							}
						,listeners : {
							exception: function (reader,rawData) {
								// console.log(rawData);
								// alert(rawData.responseText);
								window.rawData = rawData;
								let data =  JSON.parse(rawData.responseText)
								if(!data.success) {
									Ext.Msg.show({
										title   : 'OOPS, AN ERROR JUST HAPPEN !'
										,icons  : Ext.Msg.ERROR
										,msg    : data.msg
										,buttons: Ext.Msg.OK
										});
									console.log(data.msg)
									}
								}
							}
						}						
		});
	var toolbar_expDuration = Ext.create('Ext.toolbar.Toolbar',{
		dock 		:'bottom'
		,ui			: 'footer'
		,defaults 	: {	defaultType : 'button'
						,scale 		: 'large' 
						}
		,items 		: [	
						{	name: 'delete',
							icon: 'resources/delete.png',
							handler: function() {
								var rec = grid_expDuration.getSelectionModel().getSelection();
								var len = rec.length;
								if (rec == 0) {
									Ext.Msg.show({
										title: 'Failure - Select Data',
										icon: Ext.Msg.ERROR,
										msg: 'Select any field you desire to delete',
										buttons: Ext.Msg.OK
									});
								} else {
									Ext.Msg.confirm('Confirm', 'Are you sure want to delete data ?', function(btn) {
										if(btn == 'yes') {
											for (var i=0;i<len;i++) {
												Ext.Ajax.request({
													url: 'response/mc_delExpDuration.php',
													method: 'POST',
													params: 'id='+rec[i].data.ID,
													success: function(obj) {
														var resp = obj.responseText;
														if(resp !=0) {
															store_expDuration.loadPage(1);
														} else {
															Ext.Msg.show({
																title 	: 'Delete Data',
																icon 	: Ext.Msg.ERROR,
																msg 	: resp,
																buttons	: Ext.Msg.OK
															});
														}
													}
												});
											}
										}
									});
								}
							}
							}
						,{	name: 'print',
							icon: 'resources/print.png',
							handler	: function(widget, event) {
								var rec = grid_expDuration.getSelectionModel().getSelection();
								var len = rec.length;
								
								if(len == "") {
									Ext.Msg.show({
										title		:'Message',
										icon		: Ext.Msg.ERROR,
										msg			: "No data selected.",
										buttons		: Ext.Msg.OK
									});
								} else {
									//	get selected data
									var i = 0; // initial variable for looping
									var a = ''; // empty string 
									var b = ''; // empty string
									var total = 0;
									
									for (var i=0; i < len; i++) {
										cb 	= a + '' + rec[i].data.ID;
										a 	= a + '' + rec[i].data.ID + '/';
										
										lbl = b + '' + rec[i].data.ID;
										b 	= b + '' + rec[i].data.ID + ', ';
										
										total++;
									}
									window.open ("response/MC_printDuration.php?total="+total+"&cb="+cb+"");
								}
							}
							}
						,'->'
						,{ 	name 	 	: 'create'
							,icon 	 	: 'resources/create.png'
							,formBind 	: true
							,handler 	: function() {
								var getForm = this.up('form').getForm();
								if (getForm.isValid()) {
									console.log('INPUT NEW DATA');
									getForm.submit({
										url 	: 'response/mc_inputExpDuration.php'
										,waitMsg: 'Now transfering data, please wait..'
										,success: function(form, action) {
											Ext.Msg.show({
												title   : 'SUCCESS'
												,msg    : action.result.msg
												,buttons: Ext.Msg.OK
												});
											store_expDuration.loadPage(1);
											}
										,failure: function(form, action) {
											Ext.Msg.show({
												title   : 'OOPS, AN ERROR JUST HAPPEN !'
												,icons  : Ext.Msg.ERROR
												,msg    : action.result.msg
												,buttons: Ext.Msg.OK
												});
											}
										});
									}
								}
							}
						,{	name 	: 'reset'
							,icon 	: 'resources/reset.png'
							,handler: function() {
								this.up('form').getForm().reset();
								Ext.getCmp('mc_expDurationNik').focus();
								Ext.getCmp('src_expDurationRemark').reset();
								Ext.getCmp('src_expDuration').reset();
								
								store_expDuration.proxy.setExtraParam('remark','');
								store_expDuration.proxy.setExtraParam('duration','');
								store_expDuration.loadPage(1);

								// Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0].setEditable(true);
                				// Ext.ComponentQuery.query('textfield[name=mc_expDataNik]')[0].focus();
								
                				// Ext.getCmp('src_expDataStsinsp').reset();
                				// Ext.getCmp('src_expDataPartno').reset();
                				// Ext.getCmp('src_expDataPO').reset();
                				// Ext.getCmp('src_expDataInvoice').reset();
                				// Ext.getCmp('src_expDataLocation').reset();
                				// Ext.getCmp('src_expDataSupplier').reset();
								// store_expData.proxy.setExtraParam('supplier','');
								// store_expData.proxy.setExtraParam('partno', '');
								// store_expData.proxy.setExtraParam('po', '');
								// store_expData.proxy.setExtraParam('invoice', '');
								// store_expData.proxy.setExtraParam('stsinsp', '');
								// store_expData.proxy.setExtraParam('location', '');
								// store_expData.loadPage(1);
								}
							}
						]
		});
	var form_expDuration 	= Ext.create('Ext.form.Panel',{
		name 		: 'form_expDuration'
		,layout		: 'anchor'
		,width 		: 550
		,bodyStyle 	: { background 	: 'rgba(255, 255, 255, 0)' }
	    ,defaults 	: { anchor		: '100%'
	    				,labelWidth	: 150 }
	    ,defaultType: 'textfield'
	    ,dockedItems: [toolbar_expDuration]
	    ,items 		: [ 
			{	fieldLabel			: 'NIK'
				,xtype 				: 'textfield'
				,name 				: 'mc_expDurationNik'
				,id 				: 'mc_expDurationNik'
				,allowBlank 		: false
				,afterLabelTextTpl 	: required
				,minLength 			: 5
				,maxLength 			: 8
				,emptyText 			: 'Scan NIK here...'
				,listeners 			: {
					afterrender : function(field) { field.focus(true,500); },
					specialkey 	:  function(field, e) {
						if (e.getKey() == e.ENTER) {
							Ext.ComponentQuery.query('textfield[name=mc_expDuration]')[0].focus(true,1);
							}
						},
					change:function(field){
						field.setValue(field.getValue().toUpperCase());
						}
					}
				},
			{	fieldLabel			: 'DURATION ( MONTH )'
				,xtype				: 'textfield'
				,name				: 'mc_expDuration'
				,maskRe 			: /[0-9.,]/
				,allowBlank			: false
				,afterLabelTextTpl	: required
				,listeners			: {
					change:function(field){
						field.setValue(field.getValue().toUpperCase());
						},
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							Ext.ComponentQuery.query('textareafield[name=mc_expDurationRemark]')[0].focus(true,1);
							}
						}
					}
				},
			{	fieldLabel	: 'REMARK'
				,xtype  	: 'textareafield'
				,name 		: 'mc_expDurationRemark'
				,grow 		: true
				,listeners 	: {
					change:function(field){
						field.setValue(field.getValue().toUpperCase());
						}
					}
				}
			]
		});
	var grid_expDuration 	= Ext.create('Ext.grid.Panel', {
		store 		: store_expDuration
		,selModel 	: Ext.create('Ext.selection.CheckboxModel')
	    ,viewConfig	: { enableTextSelection  : true }
	    ,width 		: 400
	    ,columns 	: [
	    	{ header: 'NO', xtype: 'rownumberer', width: 55, sortable: false },
	    	{ text: 'ID', dataIndex: 'id', flex: 1, hidden:true },
	    	{ 	text 		: 'REMARK'
				,dataIndex 	: 'REMARK'
				,flex 		: 1
				,layout 	: { type   :'hbox'
	    						,align :'stretch'
								,pack  :'center' }
	    		,items 		: [{ 	xtype 		: 'textfield'
									,name 		: 'src_expDurationRemark'
									,id 		: 'src_expDurationRemark'
									,emptyText 	: 'Search...'
									,flex       : 1
									,margin     : '0 10 10 10'
									,listeners 	: {
					    				specialkey: function(field, e) {
											if (e.getKey() == e.ENTER) {
												store_expDuration.proxy.setExtraParam('remark', field.getValue());
												store_expDuration.proxy.setExtraParam('duration', Ext.getCmp('src_expDuration').getValue());
												store_expDuration.loadPage(1);
												}
				                			}
					    				}
									}] 
				},
	    	{ 	text 		: 'AFTER BAKING DURATION ( Month )' 
    			,dataIndex 	: 'DURATION'
    			,flex 		: 1
    			,renderer 	: addmonth
    			,layout 	: { type   :'hbox'
	    						,align :'stretch'
								,pack  :'center' }
	    		,items 		: [{ 	xtype 		: 'textfield'
									,name 		: 'src_expDuration'
									,id 		: 'src_expDuration'
									,emptyText 	: 'Search...'
									,flex       : 1
									,margin     : '0 10 10 10'
									,listeners 	: {
					    				specialkey: function(field, e) {
											if (e.getKey() == e.ENTER) {
												store_expDuration.proxy.setExtraParam('remark', Ext.getCmp('src_expDurationRemark').getValue());
												store_expDuration.proxy.setExtraParam('duration', field.getValue());
												store_expDuration.loadPage(1);
												}
				                			}
					    				}
									}] 
    			},
			{ 	header 			: 'CREATE BY'	
				,dataIndex 		: 'CREATED_BY'	
				,componentCls	: 'headergrid'		
				,flex 			: 1 	
				,renderer 		: combineInput
				},
			{ 	header  		: 'UPDATE BY' 	
				,dataIndex 		: 'UPDATED_BY'	
				,componentCls	: 'headergrid'		
				,flex 			: 1
				,hidden 		: true	
				,renderer 		: combineUpdate
				}
	    	],
	    bbar: {
	    	xtype 		: 'pagingtoolbar'
	    	,displayInfo	: true
	    	// ,store 		: store_expDuration
	    	},
	    listeners: {
	    	select: function(grid, rowIndex, colIndex) {
                var rec = this.getSelectionModel().getSelection();
            	
                // var valnik 		= rec[0].data.CREATED_BY;
            	// var valsuppcode = rec[0].data.SUPPCODE;
            	var valDuration = rec[0].data.DURATION;
            	var valremark 	= rec[0].data.REMARK;
                
               	// var txt_nik 	= Ext.ComponentQuery.query('textfield[name=mc_expMstNik]')[0];
                // var txt_supp 	= Ext.ComponentQuery.query('combobox[name=mc_expMstSupplier]')[0];
                var txt_duration= Ext.ComponentQuery.query('textfield[name=mc_expDuration]')[0];
                var txt_remark 	= Ext.ComponentQuery.query('textfield[name=mc_expDurationRemark]')[0];

                // txt_nik.setValue(valnik);
                // txt_supp.setValue(valsuppcode);
                txt_duration.setValue(valDuration);
                txt_remark.setValue(valremark);
                
          		// exp_part.proxy.setExtraParam('supplier', suppcode); // set parameter for exp_part store
        		// exp_part.load();
        		Ext.ComponentQuery.query('textfield[name=mc_expDurationNik]')[0].focus(true,1);
                // Ext.ComponentQuery.query('textfield[name=nik]')[0].setEditable(false);
            	}
	    	}	
		});
	var panel_expUpdate 	= Ext.create('Ext.panel.Panel',{
		border 	 	: true
		,layout 	: 'border'
	    ,defaults 	: {
	    	split 	: false
	    	,plain 	: true
			}
	    ,items 		: [
			{	region: 'north'
	        	,layout: {
					type: 'hbox'
	        		,pack: 'center'
	        		}
	        	,bodyPadding: 10
	        	,bodyStyle: {
	        		background 		: 'url("resources/bg-image.jpg") no-repeat center left'
	        		,backgroundSize: 'cover'
	        	}
	        	,items: form_expDuration
	    		} 
			,{
				region: 'center'
				,layout: 'fit'
				,items: grid_expDuration
				}
			]
		});
</script>
