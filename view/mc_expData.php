<script type="text/javascript">
	function whitespace(val) {
		return '<div style="white-space: pre;">' + val + '</div>';
	}

	function combineInput(value, meta, record, rowIndex, colIndex, store) {
		value2 = record.get('CREATED_NAME');
		value3 = record.get('CREATED_AT');
		if (value === '---' || value === '-' || value === '' || value === null) {
			return '<font style="white-space:normal; line-height:1.5; color:red;"> _ </font>';
		} else {
			return '<font style="color:blue;">' + value3 + '</font><br><font style="color:gray;">' + value + '</font><font> ' + value2 + '</font>';
		}
	}

	function combineUpdate(value, meta, record, rowIndex, colIndex, store) {
		value2 = record.get('UPDATED_NAME');
		value3 = record.get('UPDATED_AT');
		if (value === '---' || value === '-' || value === '' || value === null) {
			return '<font style="white-space:normal; line-height:1.5; color:red;"> _ </font>';
		} else {
			return '<font style="color:blue;">' + value3 + '</font><br><font style="color:gray;">' + value + '</font><font> ' + value2 + '</font>';
		}
	}

	function combineSupp(value, meta, record, rowIndex, colIndex, store) {
		value2 = record.get('SUPPNAME');
		if (value === '---' || value === '-' || value === '' || value === null) {
			return '<font style="white-space:normal; line-height:1.5; color:red;"> _ </font>';
		} else {
			return '<font style="color:gray;">' + value + '</font><br><font>' + value2 + '</font>';
		}
	}
	var expStatus = function(val) {
		// Harus di maintain karna status EXPIRED di hardcode
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!
		var yyyy = today.getFullYear();

		if (dd < 10) {
			dd = '0' + dd
		}

		if (mm < 10) {
			mm = '0' + mm
		}

		today = yyyy + '-' + mm + '-' + dd;

		if (mm == val.substr(5, 2) - 1) {
			return ("<span style=color:#ff6f00;><b>" + val + "</b><br>( WILL BE EXPIRED SOON )</span>");
		} else if (today > val) {
			return ("<span style=color:red;>" + val + "<br>( EXPIRED PART )</span>");
		} else {
			return (val);
		}
	}
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
	var store_expData = Ext.create('Ext.data.Store', {
		fields: [],
		autoLoad: true,
		pageSize: 25,
		proxy: {
			type: 'ajax',
			url: 'json/mc_dispExpData.php',
			reader: {
				type: 'json',
				root: 'rows',
				totalProperty: 'TOTCOUNT'
			}
		}
	});
	var form_splitlabel = Ext.create('Ext.form.Panel', {
		name: 'form_splitlabel',
		layout: 'anchor',
		defaults: {
			anchor: '100%',
			padding: '5 5 5 5',
			fieldStyle: 'font-size:20px;text-align:center;'
		},
		defaultType: 'textfield',
		items: [{
				emptyText: 'SCAN NIK',
				id: 'splitnik',
				name: 'splitnik',
				// value: '37297',
				minLength: 5,
				maxLength: 8,
				allowBlank: false,
				selectOnFocus: true,
				listeners: {
					afterrender: function(field) {
						field.focus(true);
					},
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							var form = this.up('form').getForm();
							var txtval = field.getValue();
							var len = txtval.length;
							if (len != 0) {
								if (form.isValid()) {
									form.submit({
										url: 'response/mc_splitlabel.php',
										waitMsg: 'Checking data, please wait..',
										success: function(form, action) {
											Ext.getCmp('splitlabel').setDisabled(false);
											Ext.getCmp('splitlabel').focus(true);
										},
										failure: function(form, action) {
											Ext.Msg.show({
												title: 'OOPS, AN ERROR JUST HAPPEN !',
												icons: Ext.Msg.ERROR,
												msg: action.result.msg,
												buttons: Ext.Msg.OK
											});
											field.setValue("");
										}
									});
								}
							} else {
								Ext.getCmp('splitlabel').setDisabled(false);
							}
						}
					}
				}
			}, {
				emptyText: 'SCAN LABEL',
				id: 'splitlabel',
				name: 'splitlabel',
				disabled: true,
				listeners: {
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							var form = this.up('form').getForm();
							var txtval = field.getValue();
							var len = txtval.length;
							if (len != 0) {
								if (form.isValid()) {
									form.submit({
										url: 'response/mc_splitlabel.php',
										waitMsg: 'Checking data, please wait..',
										success: function(form, action) {
											var qtynext = txtval.substr(24, 5);
											if (qtynext == 1) {
												Ext.Msg.show({
													title: 'OOPS, AN ERROR JUST HAPPEN !',
													icons: Ext.Msg.ERROR,
													msg: '<h2 style=\"text-align: center; color: red;\"><b>ZERO QUANTITY</b></h2>',
													buttons: Ext.Msg.OK
												});
												field.setValue("");
											} else {
												Ext.getCmp('splitqty').setDisabled(false);
												Ext.getCmp('splitqty').focus(true);
												Ext.getCmp('splitqty').setValue(qtynext - 1);
												Ext.getCmp('splitqty').setMaxValue(qtynext - 1);
											}
										},
										failure: function(form, action) {
											Ext.Msg.show({
												title: 'OOPS, AN ERROR JUST HAPPEN !',
												icons: Ext.Msg.ERROR,
												msg: action.result.msg,
												buttons: Ext.Msg.OK
											});
											field.setValue("");
										}
									});
								}
							} else {
								Ext.getCmp('splitqty').setDisabled(true);
							}
						}
					},
					change: function(field) {
						var txtval = field.getValue();
						var len = txtval.length;
						if (len == 0) {
							Ext.getCmp('splitqty').setDisabled(true);
						} else {
							field.setValue(field.getValue().toUpperCase());
						}

					}
				}
			}, {
				emptyText: 'SPLIT QTY',
				id: 'splitqty',
				name: 'splitqty',
				xtype: 'numberfield',
				disabled: true,
				listeners: {
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							var form = this.up('form').getForm();
							if (form.isValid()) {
								form.submit({
									url: 'response/mc_splitlabel.php',
									waitMsg: 'Now transfering data, please wait..',
									success: function(form, action) {
										Ext.toast({
											html: 'Data Saved',
											title: 'SUCCESS - INOFRMATION',
											width: 200,
											align: 't'
										});
										store_expData.loadPage(1);
										Ext.getCmp('splitqty').setValue("");
										Ext.getCmp('splitlabel').setValue("");
										Ext.getCmp('splitnik').setValue("");
										Ext.getCmp('splitqty').setDisabled(true);
										Ext.getCmp('splitlabel').setDisabled(true);
										Ext.getCmp('splitnik').focus(true);
										field.reset();
									},
									failure: function(form, action) {
										Ext.Msg.show({
											title: 'OOPS, AN ERROR JUST HAPPEN !',
											icons: Ext.Msg.ERROR,
											msg: action.result.msg,
											buttons: Ext.Msg.OK
										});
									}
								});
							}
						}
					}
				}
			},
			{
				xtype: 'button',
				text: 'RESET',
				id: 'splitreset',
				name: 'splitreset',
				anchor: '47%',
				margin: '0 0 0 15',
				handler: function() {
					this.up('form').getForm().reset();
					Ext.getCmp('splitqty').setDisabled(true);
					Ext.getCmp('splitlabel').setDisabled(true);
					Ext.getCmp('splitnik').focus(true);
				}
			},
			{
				xtype: 'button',
				text: 'SPLIT',
				id: 'splitstart',
				name: 'splitstart',
				anchor: '47%',
				margin: '0 0 0 10',
				handler: function() {
					var form = this.up('form').getForm();
					if (form.isValid()) {
						form.submit({
							url: 'response/mc_splitlabel.php',
							waitMsg: 'Now transfering data, please wait..',
							success: function(form, action) {
								Ext.toast({
									html: 'Data Saved',
									title: 'SUCCESS - INOFRMATION',
									width: 200,
									align: 't'
								});
								store_expData.loadPage(1);
								Ext.getCmp('splitqty').setValue("");
								Ext.getCmp('splitlabel').setValue("");
								Ext.getCmp('splitnik').setValue("");
								Ext.getCmp('splitqty').setDisabled(true);
								Ext.getCmp('splitlabel').setDisabled(true);
								Ext.getCmp('splitnik').focus(true);
								// field.reset();
							},
							failure: function(form, action) {
								Ext.Msg.show({
									title: 'OOPS, AN ERROR JUST HAPPEN !',
									icons: Ext.Msg.ERROR,
									msg: action.result.msg,
									buttons: Ext.Msg.OK
								});
							}
						});
					}
				}
			}
		]
	});
	var form_updExpPart = Ext.create('Ext.form.Panel', {
		name: 'form_updExpPart',
		layout: 'anchor',
		defaults: {
			anchor: '100%',
			padding: '5 5 5 5',
			fieldStyle: 'font-size:20px;text-align:center;'
		},
		defaultType: 'textfield',
		items: [{
			emptyText: 'SCAN NIK',
			id: 'updexpnik',
			name: 'updexpnik',
			// value: '37297',
			minLength: 5,
			maxLength: 8,
			allowBlank: false,
			selectOnFocus: true,
			listeners: {
				afterrender: function(field) {
					field.focus(true);
				},
				specialkey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						var form = this.up('form').getForm();
						var txtval = field.getValue();
						var len = txtval.length;
						if (len != 0) {
							if (form.isValid()) {
								form.submit({
									url: 'response/mc_updExpBaking.php',
									waitMsg: 'Checking data, please wait..',
									success: function(form, action) {
										Ext.getCmp('updexpdate').setDisabled(false);
										Ext.getCmp('updexpdate').focus(true);
									},
									failure: function(form, action) {
										Ext.Msg.show({
											title: 'OOPS, AN ERROR JUST HAPPEN !',
											icons: Ext.Msg.ERROR,
											msg: action.result.msg,
											buttons: Ext.Msg.OK
										});
										field.setValue("");
									}
								});
							}
						} else {
							Ext.getCmp('updexpdate').setDisabled(false);
						}
					}
				}
			}
		}, {
			emptyText: 'TANGGAL BAKING OUT',
			xtype: 'datefield',
			id: 'updexpdate',
			name: 'updexpdate',
			format: 'Y-m-d',
			allowBlank: false,
			disabled: true,
			listeners: {
				select: function(field) {
					var form = this.up('form').getForm();
					var txtval = field.getValue();
					var len = txtval.length;
					if (len != 0) {
						Ext.getCmp('updexplabel').setDisabled(false);
						Ext.getCmp('updexplabel').focus(true);
					} else {
						Ext.getCmp('updexplabel').setDisabled(false);
					}
				},
				specialkey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						var form = this.up('form').getForm();
						var txtval = field.getValue();
						var len = txtval.length;
						if (len != 0) {
							if (form.isValid()) {
								form.submit({
									url: 'response/mc_updExpBaking.php',
									waitMsg: 'Checking data, please wait..',
									success: function(form, action) {
										Ext.getCmp('updexplabel').setDisabled(false);
										Ext.getCmp('updexplabel').focus(true);
									},
									failure: function(form, action) {
										Ext.Msg.show({
											title: 'OOPS, AN ERROR JUST HAPPEN !',
											icons: Ext.Msg.ERROR,
											msg: action.result.msg,
											buttons: Ext.Msg.OK
										});
										field.setValue("");
									}
								});
							}
						} else {
							Ext.getCmp('updexplabel').setDisabled(false);
						}
					}
				}
			}
		}, {
			emptyText: 'SCAN LABEL',
			id: 'updexplabel',
			name: 'updexplabel',
			disabled: true,
			listeners: {
				specialkey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						var form = this.up('form').getForm();
						var txtval = field.getValue();
						var len = txtval.length;
						if (len != 0) {
							if (form.isValid()) {
								form.submit({
									url: 'response/mc_updExpBaking.php',
									waitMsg: 'Checking data, please wait..',
									success: function(form, action) {
										Ext.getCmp('updexpduration').setDisabled(false);
										Ext.getCmp('updexpduration').focus(true);
									},
									failure: function(form, action) {
										Ext.Msg.show({
											title: 'OOPS, AN ERROR JUST HAPPEN !',
											icons: Ext.Msg.ERROR,
											msg: action.result.msg,
											buttons: Ext.Msg.OK
										});
										field.setValue("");
									}
								});
							}
						} else {
							Ext.getCmp('updexpduration').setDisabled(true);
						}
					}
				},
				change: function(field) {
					var txtval = field.getValue();
					var len = txtval.length;
					if (len == 0) {
						Ext.getCmp('updexpduration').setDisabled(true);
					} else {
						field.setValue(field.getValue().toUpperCase());
					}

				}
			}
		}, {
			emptyText: 'SCAN DURATION AFTER BAKING',
			id: 'updexpduration',
			name: 'updexpduration',
			xtype: 'textfield',
			disabled: true,
			listeners: {
				specialkey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						var form = this.up('form').getForm();
						if (form.isValid()) {
							form.submit({
								url: 'response/mc_updExpBaking.php',
								waitMsg: 'Now transfering data, please wait..',
								success: function(form, action) {
									Ext.toast({
										html: 'Updating Expired Success',
										title: 'SUCCESS - INOFRMATION',
										width: 200,
										align: 't'
									});
									store_expData.loadPage(1);
									Ext.getCmp('updexpduration').setValue("");
									Ext.getCmp('updexplabel').setValue("");
									Ext.getCmp('updexpnik').setValue("");
									Ext.getCmp('updexpdate').setValue("");
									Ext.getCmp('updexpduration').setDisabled(true);
									Ext.getCmp('updexplabel').setDisabled(true);
									Ext.getCmp('updexpdate').setDisabled(true);
									Ext.getCmp('updexpnik').focus(true);
									field.reset();
								},
								failure: function(form, action) {
									Ext.Msg.show({
										title: 'OOPS, AN ERROR JUST HAPPEN !',
										icons: Ext.Msg.ERROR,
										msg: action.result.msg,
										buttons: Ext.Msg.OK
									});
								}
							});
						}
					}
				}
			}
		}, {
			xtype: 'button',
			text: 'RESET',
			id: 'updexpreset',
			name: 'updexpreset',
			handler: function() {
				// this.up('form').getForm().reset();

				Ext.getCmp('updexpnik').reset();
				Ext.getCmp('updexpdate').reset();
				Ext.getCmp('updexplabel').reset();
				Ext.getCmp('updexpduration').reset();

				Ext.getCmp('updexpduration').setDisabled(true);
				Ext.getCmp('updexpdate').setDisabled(true);
				Ext.getCmp('updexplabel').setDisabled(true);
				Ext.getCmp('updexpnik').focus(true);
			}
		}]
	});
	var toolbar_expData = Ext.create('Ext.toolbar.Toolbar', {
		dock: 'bottom',
		ui: 'footer',
		defaults: {
			defaultType: 'button',
			scale: 'large'
		},
		items: [{
			name: 'delete',
			icon: 'resources/delete.png',
			handler: function() {
				var rec = grid_expData.getSelectionModel().getSelection();
				var len = rec.length;
				if (rec == 0) {
					Ext.Msg.show({
						title: 'Failure - Select Data',
						icon: Ext.Msg.ERROR,
						msg: 'Select any field you desire to delete',
						buttons: Ext.Msg.OK
					});
				} else {
					Ext.Msg.confirm('Confirm', 'ANDA YAKIN UNTUK HAPUS DATA INI ?', function(btn) {
						if (btn == 'yes') {
							for (var i = 0; i < len; i++) {
								var alreadyIssue = rec[i].data.LABELSCAN;
								if (alreadyIssue != 'NOT') {
									Ext.Msg.show({
										title: 'Delete Data',
										icon: Ext.Msg.ERROR,
										msg: 'PART SUDAH DI ISSUE, PART TIDAK BOLEH DI HAPUS !',
										buttons: Ext.Msg.OK
									});
								} else {
									Ext.Ajax.request({
										url: 'response/mc_delExpData.php',
										method: 'POST',
										params: 'id=' + rec[i].data.ID,
										success: function(obj) {
											var resp = obj.responseText;
											if (resp != 0) {
												store_expData.loadPage(1);
											} else {
												Ext.Msg.show({
													title: 'Delete Data',
													icon: Ext.Msg.ERROR,
													msg: resp,
													buttons: Ext.Msg.OK
												});
											}
										}
									});
								}

							}
						}
					});
				}
			}
		}, {
			name: 'print',
			icon: 'resources/print.png',
			handler: function(widget, event) {
				var rec = grid_expData.getSelectionModel().getSelection();
				var len = rec.length;

				if (len == "") {
					Ext.Msg.show({
						title: 'Message',
						icon: Ext.Msg.ERROR,
						msg: "No data selected.",
						buttons: Ext.Msg.OK
					});
				} else {
					//	get selected data
					var i = 0; // initial variable for looping
					var a = ''; // empty string 
					var b = ''; // empty string
					var total = 0;

					for (var i = 0; i < len; i++) {
						cb = a + '' + rec[i].data.ID;
						a = a + '' + rec[i].data.ID + '/';

						lbl = b + '' + rec[i].data.ID;
						b = b + '' + rec[i].data.ID + ', ';

						total++;
					}
					window.open("response/MC_printsato.php?total=" + total + "&cb=" + cb + "");
				}
			}
		}, '->', {
			text: 'SPLIT QTY LABEL',
			name: 'splitQtyLabel',
			// icon: 'resources/splice.png',
			handler: function() {
				Ext.create('Ext.window.Window', {
					title: 'FORM SPLIT LABEL',
					// height: 200,
					border: false,
					padding: '5 5 5 5',
					width: 400,
					layout: 'fit',
					animateTarget: this,
					items: form_splitlabel,
					closeAction: 'hide',
					listeners: {
						activate: function() {
							Ext.ComponentQuery.query('button[name=splitQtyLabel]')[0].setDisabled(true);
						},
						close: function() {
							Ext.ComponentQuery.query('button[name=splitQtyLabel]')[0].setDisabled(false);
						}
					}
				}).show();
			}
		}, {
			text: 'UPD. EXPIRED',
			name: 'updateExpired',
			// icon: 'resources/splice.png',
			handler: function() {
				Ext.create('Ext.window.Window', {
					title: 'FORM UPDATE EXPIRED PART',
					// height: 200,
					border: false,
					padding: '5 5 5 5',
					width: 400,
					layout: 'fit',
					animateTarget: this,
					items: form_updExpPart,
					closeAction: 'hide',
					listeners: {
						activate: function() {
							Ext.ComponentQuery.query('button[name=updateExpired]')[0].setDisabled(true);
						},
						close: function() {
							Ext.ComponentQuery.query('button[name=updateExpired]')[0].setDisabled(false);
						}
					}
				}).show();
			}
		}, '->', {
			name: 'create',
			icon: 'resources/create.png',
			formBind: true,
			handler: function() {
				var getForm = this.up('form').getForm();
				if (getForm.isValid()) {
					var chkID = Ext.getCmp('mc_expDataID').getValue();
					console.log(chkID);
					if (chkID != "") {
						console.log('UPDATE DATA');
						getForm.submit({
							url: 'response/mc_updExpData.php',
							waitMsg: 'Now transfering data, please wait..',
							success: function(form, action) {
								Ext.Msg.show({
									title: 'SUCCESS',
									msg: action.result.msg,
									buttons: Ext.Msg.OK
								});
								// this.up('form').getForm().reset();
								Ext.getCmp('mc_expDataID').reset();
								Ext.getCmp('mc_expDataNik').reset();
								Ext.getCmp('mc_expDataInvoice').reset();
								Ext.getCmp('mc_expDataProdDate').reset();
								Ext.getCmp('mc_expDataRemark').reset();
								Ext.getCmp('mc_expDataLabel').reset();

								Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0].setEditable(true);
								Ext.ComponentQuery.query('textfield[name=mc_expDataNik]')[0].focus();
								store_expData.loadPage(1);
							},
							failure: function(form, action) {
								Ext.Msg.show({
									title: 'OOPS, AN ERROR JUST HAPPEN !',
									icons: Ext.Msg.ERROR,
									msg: action.result.msg,
									buttons: Ext.Msg.OK
								});
							}
						});
					} else {
						console.log('INPUT NEW DATA');
						getForm.submit({
							url: 'response/mc_inputExpData.php',
							waitMsg: 'Now transfering data, please wait..',
							success: function(form, action) {
								Ext.Msg.show({
									title: 'SUCCESS',
									msg: action.result.msg,
									buttons: Ext.Msg.OK
								});

								Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0].setEditable(true);
								Ext.ComponentQuery.query('textfield[name=mc_expDataNik]')[0].focus();
								store_expData.loadPage(1);
							},
							failure: function(form, action) {
								Ext.Msg.show({
									title: 'OOPS, AN ERROR JUST HAPPEN !',
									icons: Ext.Msg.ERROR,
									msg: action.result.msg,
									buttons: Ext.Msg.OK
								});
							}
						});
					}
				}
			}
		}, {
			name: 'reset',
			icon: 'resources/reset.png',
			handler: function() {
				this.up('form').getForm().reset();
				Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0].setEditable(true);
				Ext.ComponentQuery.query('textfield[name=mc_expDataNik]')[0].focus();

				Ext.getCmp('src_expDataStsinsp').reset();
				Ext.getCmp('src_expDataPartno').reset();
				Ext.getCmp('src_expDataPO').reset();
				Ext.getCmp('src_expDataInvoice').reset();
				Ext.getCmp('src_expDataLocation').reset();
				Ext.getCmp('src_expDataSupplier').reset();
				store_expData.proxy.setExtraParam('supplier', '');
				store_expData.proxy.setExtraParam('partno', '');
				store_expData.proxy.setExtraParam('po', '');
				store_expData.proxy.setExtraParam('invoice', '');
				store_expData.proxy.setExtraParam('stsinsp', '');
				store_expData.proxy.setExtraParam('location', '');
				store_expData.loadPage(1);
			}
		}]
	});
	var form_expData = Ext.create('Ext.form.Panel', {
		name: 'form_expData',
		layout: 'anchor',
		width: 550,
		bodyStyle: {
			background: 'rgba(255, 255, 255, 0)'
		},
		defaults: {
			anchor: '100%',
			labelWidth: 150
		},
		defaultType: 'textfield',
		dockedItems: [toolbar_expData],
		items: [{
				fieldLabel: 'ID',
				id: 'mc_expDataID',
				name: 'mc_expDataID',
				xtype: 'hiddenfield'
				// ,hidden 			: true
			}, {
				fieldLabel: 'NIK',
				xtype: 'textfield',
				name: 'mc_expDataNik',
				id: 'mc_expDataNik',
				allowBlank: false,
				afterLabelTextTpl: required,
				minLength: 5,
				maxLength: 8,
				emptyText: 'Scan NIK here...',
				listeners: {
					afterrender: function(field) {
						field.focus(true, 500);
					},
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							Ext.ComponentQuery.query('textfield[name=mc_expDataInvoice]')[0].focus(true, 1);
						}
					},
					change: function(field) {
						field.setValue(field.getValue().toUpperCase());
					}
				}
			}, {
				name: 'reset',
				xtype: 'button',
				scale: 'medium',
				text: 'NEXT PART',
				icon: 'resources/reset.png',
				margin: '0 0 0 155',
				handler: function() {
					// this.up('form').getForm().reset();
					// Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0].setEditable(true);
					// Ext.ComponentQuery.query('textfield[name=mc_expDataNik]')[0].focus();

					Ext.getCmp('mc_expDataLabel').reset();
					Ext.getCmp('mc_expDataInvoice').reset();
					Ext.getCmp('mc_expDataProdDate').reset();
					Ext.getCmp('mc_expDataRemark').reset();
					Ext.getCmp('mc_expDataLabel').focus();

					Ext.getCmp('src_expDataStsinsp').reset();
					Ext.getCmp('src_expDataPartno').reset();
					Ext.getCmp('src_expDataPO').reset();
					Ext.getCmp('src_expDataInvoice').reset();
					Ext.getCmp('src_expDataLocation').reset();
					Ext.getCmp('src_expDataSupplier').reset();
					store_expData.proxy.setExtraParam('supplier', '');
					store_expData.proxy.setExtraParam('partno', '');
					store_expData.proxy.setExtraParam('po', '');
					store_expData.proxy.setExtraParam('invoice', '');
					store_expData.proxy.setExtraParam('stsinsp', '');
					store_expData.proxy.setExtraParam('location', '');
					store_expData.loadPage(1);
				}
			}, {
				xtype: 'tbspacer',
				height: 5
			}, {
				fieldLabel: 'INVOICE',
				xtype: 'textfield',
				name: 'mc_expDataInvoice',
				id: 'mc_expDataInvoice',
				listeners: {
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							Ext.ComponentQuery.query('textfield[name=mc_expDataProdDate]')[0].focus(true, 1);
						}
					},
					change: function(field) {
						field.setValue(field.getValue().toUpperCase());
					}
				}
			}, {
				fieldLabel: 'PRODUCTION DATE',
				xtype: 'datefield',
				name: 'mc_expDataProdDate',
				id: 'mc_expDataProdDate',
				format: 'Y-m-d',
				allowBlank: false,
				editable: false,
				afterLabelTextTpl: required
			}, {
				fieldLabel: 'REMARK',
				xtype: 'textareafield',
				name: 'mc_expDataRemark',
				id: 'mc_expDataRemark',
				grow: true,
				listeners: {
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0].focus(true, 1);
						}
					},
					change: function(field) {
						field.setValue(field.getValue().toUpperCase());
					}
				}
			}, {
				fieldLabel: 'SCAN LABEL',
				xtype: 'textfield',
				name: 'mc_expDataLabel',
				id: 'mc_expDataLabel',
				allowBlank: false,
				minLength: 76,
				afterLabelTextTpl: required,
				emptyText: 'Scan Label Here...',
				listeners: {
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							var getForm = this.up('form').getForm();
							if (getForm.isValid()) {
								var chkID = Ext.getCmp('mc_expDataID').getValue();
								console.log(chkID);
								if (chkID != "") {
									console.log('UPDATE DATA');
									getForm.submit({
										url: 'response/mc_updExpData.php',
										waitMsg: 'Now transfering data, please wait..',
										success: function(form, action) {
											Ext.Msg.show({
												title: 'SUCCESS',
												msg: action.result.msg,
												buttons: Ext.Msg.OK
											});
											this.up('form').getForm().reset();
											Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0].setEditable(true);
											Ext.ComponentQuery.query('textfield[name=mc_expDataNik]')[0].focus();
											store_expData.loadPage(1);
										},
										failure: function(form, action) {
											Ext.Msg.show({
												title: 'OOPS, AN ERROR JUST HAPPEN !',
												icons: Ext.Msg.ERROR,
												msg: action.result.msg,
												buttons: Ext.Msg.OK
											});
										}
									});
								} else {
									console.log('INPUT NEW DATA');
									getForm.submit({
										url: 'response/mc_inputExpData.php',
										waitMsg: 'Now transfering data, please wait..',
										success: function(form, action) {
											// Ext.Msg.show({
											// 	title   : 'SUCCESS',
											// 	msg     : action.result.msg,
											// 	buttons : Ext.Msg.OK
											// });
											Ext.getCmp('mc_expDataLabel').reset();
											Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0].setEditable(true);
											Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0].focus();
											store_expData.loadPage(1);
										},
										failure: function(form, action) {
											Ext.Msg.show({
												title: 'OOPS, AN ERROR JUST HAPPEN !',
												icons: Ext.Msg.ERROR,
												msg: action.result.msg,
												buttons: Ext.Msg.OK
											});

											Ext.getCmp('mc_expDataLabel').reset();
											Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0].focus();
										}
									});
								}
							}
						}
					},
					change: function(field) {
						field.setValue(field.getValue().toUpperCase());
					}
				}
			}

		]
	});
	var grid_expData = Ext.create('Ext.grid.Panel', {
		store: store_expData,
		selModel: Ext.create('Ext.selection.CheckboxModel'),
		autoScroll: true,
		viewConfig: {
			enableTextSelection: true,
			getRowClass: function(record) {
				var labelstatus = record.get('LABELSCAN');
				if (labelstatus != 'NOT') {
					return 'issued';
				}
			}
		},
		columns: [{
			header: 'NO',
			xtype: 'rownumberer',
			width: 55,
			sortable: false
		}, {
			text: 'ID',
			dataIndex: 'ID',
			flex: 1,
			hidden: true
		}, {
			text: 'PART LABEL',
			dataIndex: 'PARTLABEL',
			flex: 1,
			hidden: true,
			renderer: whitespace
		}, {
			text: 'PART LABEL OLD',
			dataIndex: 'PARTLABELOLD',
			flex: 1,
			hidden: true,
			renderer: whitespace
		}, {
			text: 'PART NO',
			dataIndex: 'PARTNO',
			width: 135,
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack: 'center'
			},
			items: [{
				xtype: 'textfield',
				name: 'src_expDataPartno',
				id: 'src_expDataPartno',
				emptyText: 'Search...',
				flex: 1,
				margin: '0 10 10 10',
				listeners: {
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							store_expData.proxy.setExtraParam('supplier', Ext.getCmp('src_expDataSupplier').getValue());
							store_expData.proxy.setExtraParam('partno', field.getValue());
							store_expData.proxy.setExtraParam('po', Ext.getCmp('src_expDataPO').getValue());
							store_expData.proxy.setExtraParam('invoice', Ext.getCmp('src_expDataInvoice').getValue());
							store_expData.proxy.setExtraParam('stsinsp', Ext.getCmp('src_expDataStsinsp').getValue());
							store_expData.proxy.setExtraParam('location', Ext.getCmp('src_expDataLocation').getValue());
							store_expData.loadPage(1);
						}
					}
				}
			}]
		}, {
			text: 'PO',
			dataIndex: 'PO',
			width: 70,
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack: 'center'
			},
			items: [{
				xtype: 'textfield',
				id: 'src_expDataPO',
				name: 'src_expDataPO',
				emptyText: 'Search...',
				flex: 1,
				margin: '0 10 10 10',
				listeners: {
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							store_expData.proxy.setExtraParam('supplier', Ext.getCmp('src_expDataSupplier').getValue());
							store_expData.proxy.setExtraParam('partno', Ext.getCmp('src_expDataPartno').getValue());
							store_expData.proxy.setExtraParam('po', field.getValue());
							store_expData.proxy.setExtraParam('invoice', Ext.getCmp('src_expDataInvoice').getValue());
							store_expData.proxy.setExtraParam('stsinsp', Ext.getCmp('src_expDataStsinsp').getValue());
							store_expData.proxy.setExtraParam('location', Ext.getCmp('src_expDataLocation').getValue());
							store_expData.loadPage(1);
						}
					}
				}
			}]
		}, {
			text: 'QTY',
			dataIndex: 'QTY',
			width: 60
		}, {
			text: 'SUPPLIER',
			dataIndex: 'SUPPCODE',
			flex: 1,
			renderer: combineSupp,
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack: 'center'
			},
			items: [{
				xtype: 'textfield',
				name: 'src_expDataSupplier',
				id: 'src_expDataSupplier',
				emptyText: 'Search...',
				flex: 1,
				margin: '0 10 10 10',
				listeners: {
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							store_expData.proxy.setExtraParam('supplier', field.getValue());
							store_expData.proxy.setExtraParam('partno', Ext.getCmp('src_expDataPartno').getValue());
							store_expData.proxy.setExtraParam('po', Ext.getCmp('src_expDataPO').getValue());
							store_expData.proxy.setExtraParam('invoice', Ext.getCmp('src_expDataInvoice').getValue());
							store_expData.proxy.setExtraParam('stsinsp', Ext.getCmp('src_expDataStsinsp').getValue());
							store_expData.proxy.setExtraParam('location', Ext.getCmp('src_expDataLocation').getValue());
							store_expData.loadPage(1);
						}
					}
				}
			}]
		}, {
			text: 'INVOICE',
			dataIndex: 'INVOICE',
			flex: 1,
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack: 'center'
			},
			items: [{
				xtype: 'textfield',
				name: 'src_expDataInvoice',
				id: 'src_expDataInvoice',
				emptyText: 'Search...',
				flex: 1,
				margin: '0 10 10 10',
				listeners: {
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							store_expData.proxy.setExtraParam('supplier', Ext.getCmp('src_expDataSupplier').getValue());
							store_expData.proxy.setExtraParam('partno', Ext.getCmp('src_expDataPartno').getValue());
							store_expData.proxy.setExtraParam('po', Ext.getCmp('src_expDataPO').getValue());
							store_expData.proxy.setExtraParam('invoice', field.getValue());
							store_expData.proxy.setExtraParam('stsinsp', Ext.getCmp('src_expDataStsinsp').getValue());
							store_expData.proxy.setExtraParam('location', Ext.getCmp('src_expDataLocation').getValue());
							store_expData.loadPage(1);
						}
					}
				}
			}]
		}, {
			text: 'PROD. DATE',
			dataIndex: 'PRODDATE',
			width: 90
		}, {
			text: 'EXPIRED DATE',
			dataIndex: 'EXPDATE',
			flex: 1,
			renderer: expStatus
		}, {
			text: 'REMARK',
			dataIndex: 'REMARK',
			flex: 1
		}, {
			text: 'INSP. STATUS',
			dataIndex: 'STSINSP',
			width: 95,
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack: 'center'
			},
			items: [{
				xtype: 'textfield',
				name: 'src_expDataStsinsp',
				id: 'src_expDataStsinsp',
				emptyText: 'Search...',
				flex: 1,
				margin: '0 10 10 10',
				listeners: {
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							store_expData.proxy.setExtraParam('supplier', Ext.getCmp('src_expDataStsinsp').getValue());
							store_expData.proxy.setExtraParam('partno', Ext.getCmp('src_expDataPartno').getValue());
							store_expData.proxy.setExtraParam('po', Ext.getCmp('src_expDataPO').getValue());
							store_expData.proxy.setExtraParam('invoice', Ext.getCmp('src_expDataInvoice').getValue());
							store_expData.proxy.setExtraParam('stsinsp', field.getValue());
							store_expData.proxy.setExtraParam('location', Ext.getCmp('src_expDataLocation').getValue());
							store_expData.loadPage(1);
						}
					}
				}
			}]
		}, {
			text: 'LOCATION',
			dataIndex: 'LOCATION',
			width: 75,
			layout: {
				type: 'hbox',
				align: 'stretch',
				pack: 'center'
			},
			items: [{
				xtype: 'textfield',
				name: 'src_expDataLocation',
				id: 'src_expDataLocation',
				emptyText: 'Search...',
				flex: 1,
				margin: '0 10 10 10',
				listeners: {
					specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							store_expData.proxy.setExtraParam('supplier', Ext.getCmp('src_expDataStsinsp').getValue());
							store_expData.proxy.setExtraParam('partno', Ext.getCmp('src_expDataPartno').getValue());
							store_expData.proxy.setExtraParam('po', Ext.getCmp('src_expDataPO').getValue());
							store_expData.proxy.setExtraParam('invoice', Ext.getCmp('src_expDataInvoice').getValue());
							store_expData.proxy.setExtraParam('stsinsp', Ext.getCmp('src_expDataStsinsp').getValue());
							store_expData.proxy.setExtraParam('location', field.getValue());
							store_expData.loadPage(1);
						}
					}
				}
			}]
		}, {
			header: 'CREATE BY',
			dataIndex: 'CREATED_BY',
			componentCls: 'headergrid',
			flex: 1,
			renderer: combineInput
		}, {
			header: 'UPDATE BY',
			dataIndex: 'UPDATED_BY',
			componentCls: 'headergrid',
			flex: 1,
			hidden: true,
			renderer: combineUpdate
		}, {
			header: 'LABELSCAN',
			dataIndex: 'LABELSCAN',
			componentCls: 'headergrid',
			width: 250,
			hidden: true
		}],
		bbar: {
			xtype: 'pagingtoolbar',
			displayInfo: true,
			pageSize: 25,
			store: store_expData,
			displayInfo: true,
			displayMsg: 'Data {0} - {1} from {2} data',
			emptyMsg: "Page not found",
			beforePageText: 'Page',
			afterPageText: 'from {0} Pages',
			firstText: 'First Page',
			prevText: 'Previous Page',
			nextText: 'Next page',
			lastText: 'Last Page',
			// plugins: Ext.create('Ext.ux.ProgressBarPager', {}),
			listeners: {
				afterrender: function(cmp) {
					cmp.getComponent("refresh").hide();
				}
			}
		},
		listeners: {
			select: function(grid, rowIndex, colIndex) {
				var rec = this.getSelectionModel().getSelection();
				// if (!rec.data) {

				// } else {

				var val_id = rec[0].data.ID;
				// var valnik 		= rec[0].data.CREATED_BY;
				var valpartlabel = rec[0].data.PARTLABEL;
				var valinvoice = rec[0].data.INVOICE;
				var valprodDate = rec[0].data.PRODDATE;
				var valremark = rec[0].data.REMARK;

				var txt_id = Ext.ComponentQuery.query('hiddenfield[name=mc_expDataID]')[0];
				// var txt_nik 	= Ext.ComponentQuery.query('textfield[name=mc_expDataNik]')[0];
				var txt_label = Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0];
				var txt_invoice = Ext.ComponentQuery.query('textfield[name=mc_expDataInvoice]')[0];
				var txt_proddate = Ext.ComponentQuery.query('datefield[name=mc_expDataProdDate]')[0];
				var txt_remark = Ext.ComponentQuery.query('textfield[name=mc_expDataRemark]')[0];

				txt_id.setValue(val_id);
				// txt_nik.setValue(valnik);
				txt_label.setValue(valpartlabel);
				txt_invoice.setValue(valinvoice);
				txt_proddate.setValue(valprodDate.substring(0, 10));
				txt_remark.setValue(valremark);

				//       exp_part.proxy.setExtraParam('supplier', suppcode); // set parameter for exp_part store
				// exp_part.load();
				// Ext.ComponentQuery.query('textfield[name=nik]')[0].setEditable(false);
				Ext.ComponentQuery.query('textfield[name=mc_expDataLabel]')[0].setEditable(false);
				Ext.ComponentQuery.query('textfield[name=mc_expDataNik]')[0].focus();
				// }
			}
		}
	});
	var panel_expData = Ext.create('Ext.panel.Panel', {
		border: true,
		layout: 'border',
		defaults: {
			split: false,
			plain: true
		},
		items: [{
			region: 'north',
			bodyPadding: 10,
			items: form_expData,
			layout: {
				type: 'hbox',
				pack: 'center'
			},
			bodyStyle: {
				background: 'url("resources/bg-image.jpg") no-repeat center left',
				backgroundSize: 'cover'
			},
		}, {
			region: 'center',
			layout: 'fit',
			items: grid_expData
		}]
	});
</script>