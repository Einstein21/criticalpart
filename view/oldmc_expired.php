<script type="text/javascript">
	var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
	// function whitespace(val) {
	// 	return '<div style="white-space: pre;">'+val+'</div>';
	// }

		Ext.define('exp_control',{
			extend: 'Ext.data.Model',
		    fields: ['unid','id','part_no','qty','balance','lotno','prod_date','exp_date','exp_after','nik']
		});

		Ext.define('exp_supp',{
			extend: 'Ext.data.Model',
			fields: ['suppcode','suppname']
		});

		Ext.define('exp_part',{
			extend: 'Ext.data.Model',
			fields: ['partno','parname','stdpack']
		});

		var exp_control = Ext.create('Ext.data.Store', {
			model: 'exp_control',
			autoLoad: true,
			pageSize: 25,
		    proxy: {
		        type: 'ajax',
		        url: 'json/displayExp.php',	
		        reader: {
		            type: 'json',
		            rootProperty: 'data',
		            totalProperty: 'totalcount'
		        }
		    },
		    listeners: {
		    	load: function(store) {
		    		store.proxy.setExtraParam('fldsrc','');
		    	}
		    }
		});


	// Ext.define('detail_part',{
	// 	extend: 'Ext.data.Model',
	// 	fields: ['unid','id','partno','htempmin','htempmax','humidmin','humidmax','lifetime','btempmin','btempmax','periodmin','periodmax','nik']
	// });

	// var store_detail = Ext.create('Ext.data.Store',{
	// 	model: 'detail_part',
	// 	autoLoad: true,
	// 	pageSize: 20,
	// 	proxy: {
	// 		type: 'ajax',
	// 		url: 'json/displayDetailPart.php',
	// 		reader: {
	// 			type: 'json',
	// 			rootProperty: 'data',
	// 			totalProperty: 'totalcount'
	// 		}
	// 	},
	// 	listeners: {
	// 		load: function (store) {
	// 			store.proxy.setExtraParam('detail_fldsrc','');
	// 		}
	// 	}
	// });
	
	// var toolbar_detail = Ext.create('Ext.toolbar.Toolbar',{
	// 	dock: 'right',
	// 	ui: 'footer',
	// 	defaults: {
	// 		defaultType: 'button',
	// 		scale: 'small'
	// 	},
	// 	items: [
	// 	// {
	// 	// 	name: 'update',
	// 	// 	text: 'UPDATE',
	// 	// 	icon: 'resources/save_s.png',
	// 	// 	handler: function() {
	// 	// 		var getForm = this.up('form').getForm();
	// 	// 		if (getForm.isValid()) {
	// 	// 			getForm.submit({
	// 	// 				url: 'response/updateExp.php',
	// 	// 				waitMsg : 'Now transfering data, please wait..',
	// 	// 				success : function(form, action) {
	//  //                        Ext.Msg.show({
	//  //                        	title   : 'SUCCESS',
	//  //                        	msg     : action.result.msg,
	//  //                        	buttons : Ext.Msg.OK
	//  //                        });
	//  //                        exp_control.loadPage(1);
	//  //                     },
	//  //                    failure : function(form, action) {
	//  //                        Ext.Msg.show({
	// 	//                         title   : 'OOPS, AN ERROR JUST HAPPEN !',
	// 	//                         icons   : Ext.Msg.ERROR,
	// 	//                         msg     : action.result.msg,
	// 	//                         buttons : Ext.Msg.OK
	//  //                        });
	//  //                      }
	// 	// 			});
	// 	// 		}
	// 	// 	}
	// 	// },
	// 	// '->',
	// 	{
	// 		name: 'create',
	// 		text: 'INPUT',
	// 		icon: 'resources/create_s.png',
	// 		formBind: true,
	// 		handler: function() {
	// 			var getForm = this.up('form').getForm();
	// 			if (getForm.isValid()) {
	// 				getForm.submit({
	// 					url: 'response/inputDetail.php',
	// 					waitMsg : 'Now transfering data, please wait..',
	// 					success : function(form, action) {
	//                         Ext.Msg.show({
	//                         	title   : 'SUCCESS',
	//                         	msg     : action.result.msg,
	//                         	buttons : Ext.Msg.OK
	//                         });
	//                         store_detail.loadPage(1);
	//                         form.reset();
	//                      },
	//                     failure : function(form, action) {
	//                         Ext.Msg.show({
	// 	                        title   : 'OOPS, AN ERROR JUST HAPPEN !',
	// 	                        icons   : Ext.Msg.ERROR,
	// 	                        msg     : action.result.msg,
	// 	                        buttons : Ext.Msg.OK
	//                         });
	//                       }
	// 				});
	// 			}
	// 		}
	// 	}, {
	// 		name: 'delete',
	// 		text: 'DELETE',
	// 		id: 'delete-master',
	// 		hidden: true,
	// 		icon: 'resources/delete_s.png',
	// 		handler: function() {
	// 			var rec = grid_detail.getSelectionModel().getSelection();
	// 			var len = rec.length;
	// 			if (rec == 0) {
	// 				Ext.Msg.show({
	// 					title: 'Failure - Select Data',
	// 					icon: Ext.Msg.ERROR,
	// 					msg: 'Select any field you desire to delete',
	// 					buttons: Ext.Msg.OK
	// 				});
	// 			} else {
	// 				Ext.Msg.confirm('Confirm', 'Are you sure want to delete data ?', function(btn) {
	// 					if(btn == 'yes') {
	// 						for (var i=0;i<len;i++) {
	// 							Ext.Ajax.request({
	// 								url: 'response/deleteDetails.php',
	// 								method: 'POST',
	// 								params: 'unid='+rec[i].data.unid,
	// 								success: function(obj) {
	// 									var resp = obj.responseText;
	// 									if(resp !=0) {
	// 										store_detail.loadPage(1);
	// 									} else {
	// 										Ext.Msg.show({
	// 											title: 'Delete Data',
	// 											icon: Ext.Msg.ERROR,
	// 											msg: resp,
	// 											buttons: Ext.Msg.OK
	// 										});
	// 									}
	// 								}
	// 							});
	// 						}
	// 					}
	// 				});
	// 			}
	// 		}
	// 	}, {
	// 		name: 'reset',
	// 		text: 'RESET',
	// 		icon: 'resources/reset_s.png',
	// 		handler: function() {
	// 			this.up('form').getForm().reset();
	// 			// Ext.ComponentQuery.query('textfield[name=nik]')[0].setEditable(true);
				
	// 		}
	// 	}]
	// });

	// var form_detail = Ext.create('Ext.form.Panel',{
	// 	name: 'form_detail',
	// 	layout: 'anchor',
	// 	bodyStyle: {
	// 		background: 'rgba(255, 255, 255, 0)'
	// 	},
	// 	// width: '50%',
	// 	items: [{
	// 		xtype: 'container',
	// 		name: 'cont-top',
	// 		layout: { type: 'hbox', pack: 'center', align: 'stretch' },
	// 		defaults: {
	// 		    width: 275,
	// 		    padding: '0 5 5 5',
	// 		    fieldStyle: 'text-align:center;'
	// 		},
	// 		defaultType: 'textfield',
	// 		items: [{
	// 			xtype: 'hiddenfield',
	// 			name: 'detunid'
	// 		}, {
	// 			emptyText: 'SCAN NIK',
	// 			name: 'detnik',  // NIK
	// 			// value: '37297',
	// 			selectOnFocus: true,
	// 			allowBlank: false,
	// 			listeners: {
	// 				afterrender: function(field) { field.focus(true,500); },
	// 		        specialkey: function(field, e) {
	// 					if (e.getKey() == e.ENTER) {
	// 						var txtval = field.getValue();
	// 						var len = txtval.length;
	// 						if (len == 0) {
	// 							// Ext.ComponentQuery.query('textfield[name=detpart]')[0].setDisabled(true);
	// 						} else {

	// 							Ext.ComponentQuery.query('textfield[name=detpart]')[0].setDisabled(false);
	// 							Ext.ComponentQuery.query('textfield[name=detpart]')[0].focus(true,1);
	// 						}
	// 					}
	// 		        },
	// 		        change: function(field) {
	// 					var txtval = field.getValue();
	// 					var len = txtval.length;
	// 					if (len == 0) {
	// 						Ext.ComponentQuery.query('textfield[name=detpart]')[0].setDisabled(true);
	// 					}
	// 				}
	// 			}
	// 		}, {
	// 			emptyText: 'PART NUMBER',
	// 			name: 'detpart', // PART NUMBER
	// 			allowBlank: false,
	// 			disabled: true,
	// 			listeners: {
	// 	        	change:function(field){
	// 	                field.setValue(field.getValue().toUpperCase());
	// 	            }
	// 	        }

	// 		}
	// 		]
	// 	}, {
	// 		xtype: 'fieldset',
	// 		cls: 'customFieldSet',
	// 		title: '<span style="color:#263238;letter-spacing:5px;font-weight:bold">CONDITION AFTER OPEN</span>',
	// 		layout: { type: 'hbox', pack: 'center', align: 'stretch' },
	// 		height: 75,
	// 		items: [{
	// 			xtype: 'fieldcontainer',
	// 			cls: 'customLabel',
	// 			fieldLabel: 'TEMPERATURE [ °C ]',
	// 			labelAlign: 'top',
	// 			labelStyle: 'color:#263238;letter-spacing:1px',
	// 			layout: { type: 'hbox', pack: 'center', align: 'stretch' },
	// 			defaults: {
	// 			    fieldStyle: 'text-align:center;',
	// 			    hideTrigger: true,
	// 		        keyNavEnabled: false,
	// 		        mouseWheelEnabled: false,
	// 			    minValue: 0,
	// 			    width: 85
	// 			},
	// 			defaultType: 'numberfield',
	// 			items: [{
	// 				emptyText: 'MIN',
	// 				name: 'htempmin', // HUMIDITY TEMPERATURE MIN VALUE
	// 				allowBlank: false
	// 			}, {
	// 				xtype: 'label',
	// 				text: '_',
	// 				width: 10
	// 			}, {
	// 				emptyText: 'MAX',
	// 				name: 'htempmax', // HUMIDITY TEMPERATURE MAX VALUE
	// 				allowBlank: false
	// 			}]
	// 		}, {xtype:'tbspacer',width:20},
	// 		{
	// 			xtype: 'fieldcontainer',
	// 			cls: 'customLabel',
	// 			fieldLabel: 'HUMIDITY [ % ]',
	// 			labelAlign: 'top',
	// 			labelStyle: 'color:#263238;letter-spacing:1px',
	// 			layout: { type: 'hbox', pack: 'center', align: 'stretch' },
	// 			defaults: {
	// 			    fieldStyle: 'text-align:center;',
	// 			    hideTrigger: true,
	// 		        keyNavEnabled: false,
	// 		        mouseWheelEnabled: false,
	// 			    minValue: 0,
	// 			    width: 85
	// 			},
	// 			defaultType: 'numberfield',
	// 			items: [{
	// 				emptyText: 'MIN',
	// 				name: 'humidmin', // HUMIDITY MIN VALUE
	// 				allowBlank: false
	// 			}, {
	// 				xtype: 'label',
	// 				text: '_',
	// 				width: 10
	// 			}, {
	// 				emptyText: 'MAX',
	// 				name: 'humidmax', // HUMIDITY MAX VALUE
	// 				allowBlank: false
	// 			}]
	// 		}, {xtype:'tbspacer',width:20},
	// 		{
	// 			xtype: 'fieldcontainer',
	// 			cls: 'customLabel',
	// 			fieldLabel: 'TIME LIMIT [ HOUR ]',
	// 			labelAlign: 'top',
	// 			labelStyle: 'color:#263238;letter-spacing:1px',
	// 			layout: { type: 'hbox', pack: 'center', align: 'stretch' },
	// 			defaults: {
	// 			    fieldStyle: 'text-align:center;',
	// 			    hideTrigger: true,
	// 		        keyNavEnabled: false,
	// 		        mouseWheelEnabled: false,
	// 			    minValue: 0
	// 			},
	// 			defaultType: 'numberfield',
	// 			items: [{
	// 				emptyText: 'FLOOR LIFE',
	// 				name: 'lifetime', // FLOOR LIFE
	// 				allowBlank: false
	// 			}]
	// 		}]
	// 	}, {
	// 		xtype: 'fieldset',
	// 		cls: 'customFieldSet',
	// 		title: '<span style="color:#263238;letter-spacing:5px;font-weight:bold">BAKING CONDITION</span>',
	// 		layout: { type: 'hbox', pack: 'center', align: 'stretch' },
	// 		height: 75,
	// 		items: [{
	// 			xtype: 'fieldcontainer',
	// 			cls: 'customLabel',
	// 			fieldLabel: 'TEMPERATURE [ °C ]',
	// 			labelAlign: 'top',
	// 			labelStyle: 'color:#263238;letter-spacing:1px',
	// 			layout: { type: 'hbox', pack: 'center', align: 'stretch' },
	// 			defaults: {
	// 			    fieldStyle: 'text-align:center;',
	// 			    hideTrigger: true,
	// 		        keyNavEnabled: false,
	// 		        mouseWheelEnabled: false,
	// 			    minValue: 0,
	// 			    width: 100
	// 			},
	// 			defaultType: 'numberfield',
	// 			items: [{
	// 				emptyText: 'MIN',
	// 				name: 'btempmin', // BAKING TEMPERATURE MIN
	// 				allowBlank: false
	// 			}, {
	// 				xtype: 'label',
	// 				text: '_',
	// 				width: 10
	// 			}, {
	// 				emptyText: 'MAX',
	// 				name: 'btempmax', // BAKING TEMPERATURE MAX
	// 				allowBlank: false
	// 			}]
	// 		}, {xtype:'tbspacer',width:20}, 
	// 		{
	// 			xtype: 'fieldcontainer',
	// 			cls: 'customLabel',
	// 			fieldLabel: 'PERIOD [ HOUR ]',
	// 			labelAlign: 'top',
	// 			labelStyle: 'color:#263238;letter-spacing:1px',
	// 			layout: { type: 'hbox', pack: 'center', align: 'stretch' },
	// 			defaults: {
	// 			    fieldStyle: 'text-align:center;',
	// 			    hideTrigger: true,
	// 		        keyNavEnabled: false,
	// 		        mouseWheelEnabled: false,
	// 			    minValue: 0
	// 			},
	// 			defaultType: 'numberfield',
	// 			items: [{
	// 				emptyText: 'MIN',
	// 				name: 'periodmin', // BAKING PERIOD MIN
	// 				allowBlank: false
	// 			}, {
	// 				xtype: 'label',
	// 				text: '_',
	// 				width: 10
	// 			}, {
	// 				emptyText: 'MAX',
	// 				name: 'periodmax', // BAKING PERIOD MAX
	// 				allowBlank: false
	// 			}]
	// 		}]
	// 	}],
	// 	dockedItems: [toolbar_detail]
	// });
	
	var exp_supp = Ext.create('Ext.data.Store', {
		model: 'exp_supp',
		autoLoad: true,
	    proxy: {
	        type: 'ajax',
	        url: 'json/displaySupp.php',	
	        reader: {
	            type: 'json',
	            rootProperty: 'data'
	        }
	    }
	    // listeners: {
	    // 	load: function(store) {
	    // 		store.proxy.setExtraParam('supplier','');
	    // 	}
	    // }
	});
	var exp_part = Ext.create('Ext.data.Store', {
		model: 'exp_part',
	    proxy: {
	        type: 'ajax',
	        url: 'json/displayPart.php',	
	        reader: {
	            type: 'json',
	            rootProperty: 'data'
	        }
	    }
	    // listeners: {
	    // 	load: function(store, records) {
	    // 		// store.proxy.setExtraParam('partno','');
	    // 		if (records.length == 0) {
    	// 		// do nothing
    	// 		} else {
		   //  		var stdpack = exp_part.getAt(0).get('stdpack');
     //            	Ext.ComponentQuery.query('textfield[name=issue_stdpack]')[0].setValue(stdpack);
     //            }
	    // 	}
	    // }
	});

	var toolbar_exp = Ext.create('Ext.toolbar.Toolbar',{
			dock:'bottom',
			ui: 'footer',
			defaults: {
				defaultType: 'button',
				scale: 'large'
			},
			items: [{
				name: 'update',
				icon: 'resources/save.png',
				handler: function() {
					var getForm = this.up('form').getForm();
					if (getForm.isValid()) {
						getForm.submit({
							url: 'response/updateExp.php',
							waitMsg : 'Now transfering data, please wait..',
							success : function(form, action) {
		                        Ext.Msg.show({
		                        	title   : 'SUCCESS',
		                        	msg     : action.result.msg,
		                        	buttons : Ext.Msg.OK
		                        });
		                        exp_control.loadPage(1);
		                     },
		                    failure : function(form, action) {
		                        Ext.Msg.show({
			                        title   : 'OOPS, AN ERROR JUST HAPPEN !',
			                        icons   : Ext.Msg.ERROR,
			                        msg     : action.result.msg,
			                        buttons : Ext.Msg.OK
		                        });
		                      }
						});
					}
				}
			},{
				name: 'delete',
				icon: 'resources/delete.png',
				handler: function() {
					var rec = grid_exp.getSelectionModel().getSelection();
					var len = rec.length;
					if (rec == 0) {
						Ext.Msg.show({
							title: 'Failure - Select Data',
							icon: Ext.Msg.ERROR,
							msg: 'Select any field you desire to delete',
							buttons: Ext.Msg.OK
						});
					} else {
						Ext.Msg.confirm('Confirm', 'Are you sure want to delete data ?', function(btn) {
							if(btn == 'yes') {
								for (var i=0;i<len;i++) {
									Ext.Ajax.request({
										url: 'response/deleteExp.php',
										method: 'POST',
										params: 'unid='+rec[i].data.unid,
										success: function(obj) {
											var resp = obj.responseText;
											if(resp !=0) {
												exp_control.loadPage(1);
											} else {
												Ext.Msg.show({
													title: 'Delete Data',
													icon: Ext.Msg.ERROR,
													msg: resp,
													buttons: Ext.Msg.OK
												});
											}
										}
									});
								}
							}
						});
					}
				}
			},{
				name: 'print',
				icon: 'resources/print.png',
				handler	: function(widget, event) {
					var rec = grid_exp.getSelectionModel().getSelection();
					var len = rec.length;
					
					if(len == "") {
						Ext.Msg.show({
							title		:'Message',
							icon		: Ext.Msg.ERROR,
							msg			: "No data selected.",
							buttons		: Ext.Msg.OK
						});
					} else {
						//	get selected data
						var i = 0; // initial variable for looping
						var a = ''; // empty string 
						var b = ''; // empty string
						var total = 0;
						
						for (var i=0; i < len; i++) {
							cb 	= a + '' + rec[i].data.id;
							a 	= a + '' + rec[i].data.id + '/';
							
							lbl = b + '' + rec[i].data.id;
							b 	= b + '' + rec[i].data.id + ', ';
							
							total++;
						}
						window.open ("response/printExp_sato.php?total="+total+"&cb="+cb+"");
					}
				}
			},'->',{
				name: 'create',
				icon: 'resources/create.png',
				formBind: true,
				handler: function() {
					var getForm = this.up('form').getForm();
					if (getForm.isValid()) {
						getForm.submit({
							url: 'response/inputExp.php',
							waitMsg : 'Now transfering data, please wait..',
							success : function(form, action) {
		                        Ext.Msg.show({
		                        	title   : 'SUCCESS',
		                        	msg     : action.result.msg,
		                        	buttons : Ext.Msg.OK
		                        });
		                        exp_control.loadPage(1);
		                     },
		                    failure : function(form, action) {
		                        Ext.Msg.show({
			                        title   : 'OOPS, AN ERROR JUST HAPPEN !',
			                        icons   : Ext.Msg.ERROR,
			                        msg     : action.result.msg,
			                        buttons : Ext.Msg.OK
		                        });
		                      }
						});
					}
				}
			}, {
				name: 'reset',
				icon: 'resources/reset.png',
				handler: function() {
					this.up('form').getForm().reset();
					Ext.ComponentQuery.query('textfield[name=nik]')[0].setEditable(true);
					exp_supp.proxy.setExtraParam('supplier','');
					exp_part.proxy.setExtraParam('partno','');
					exp_part.proxy.setExtraParam('supplier','');
					exp_supp.load();
					exp_part.load();
				}
			}]
		});
	var form_exp = Ext.create('Ext.form.Panel',{
			name: 'form_exp',
			layout: 'anchor',
			width: 400,
			bodyStyle: {
	        	background: 'rgba(255, 255, 255, 0)'
	        },
		    defaults: {
		        anchor: '100%',
		        labelWidth: 150
		    },
		    defaultType: 'textfield',
		    items: [{
		    	xtype: 'hiddenfield',
		    	name: 'unid'
		    },{
		    	xtype: 'hiddenfield',
		    	name: 'stdpack',
		    	listeners: {
		    		change: function(field) {
		    			if (field.getValue() == 999999) {
		    				Ext.Msg.alert('WARNING','Please update Std. Packing !');
		    				Ext.ComponentQuery.query('combobox[name=partno]')[0].reset();
		    				field.reset();
		    			}
		    		}
		    	}
		    },{
		    	fieldLabel: 'NIK',
		    	name: 'nik',
		    	allowBlank: false,
		    	afterLabelTextTpl: required,
		    	minLength: 5,
		    	emptyText: 'SCAN NIK HERE...',
		    	listeners: {
	                afterrender: function(field) { field.focus(true,500); },
	                specialkey: function(field, e) {
						if (e.getKey() == e.ENTER) {
							Ext.ComponentQuery.query('textfield[name=supplier]')[0].focus(true,1);
						}
	                },
	                change:function(field){
		                field.setValue(field.getValue().toUpperCase());
		            }
	            }
		    },{
		    	xtype: 'combobox',
		    	name: 'supplier',
		    	fieldLabel: 'SUPPLIER',
		    	store: exp_supp,
		    	queryMode: 'proxy',
		    	queryParam: 'supplier',
                displayField: 'suppname',
                valueField: 'suppcode',
                allowBlank: false,
                afterLabelTextTpl: required,
                listConfig: {
                    loadingText: 'Searching...',
                    emptyText: 'No data found.',
                    getInnerTpl: function() {
                        return '<div> {suppname} &#8212; <b>( {suppcode} )</b></div>';
                    }
                },
                listeners: {
                	select: function(field) {
                		var val = field.getValue();
                		exp_part.proxy.setExtraParam('supplier', val); // set parameter for exp_part store
                		exp_part.load();
                	}
                }
		    },{
		    	xtype: 'combobox',
		        name: 'partno',
		        fieldLabel: 'PART NUMBER',
		        store: exp_part,
		        queryMode: 'proxy',
		        // queryParam: 'partno',
		        displayField: 'partno',
		        valueField: 'partno',
		        allowBlank: false,
		        afterLabelTextTpl: required,
		        listConfig: {
                    loadingText: 'Searching...',
                    emptyText: 'No data found.'
                },
		        listeners: {
                	change: function(field) {
	            		if(field.getValue() == null){
	            			exp_part.proxy.setExtraParam('partno', '');
							exp_part.loadPage(1);
						}
                	// 	var val = field.getValue();
                	// 	exp_part.proxy.setExtraParam('partno', val); // set parameter for exp_part store
                	// 	// exp_part.load();
                	},
                	select: function(combo, records, eOpts) {
                		// console.log(combo.getSelectedRecord().get('stdpack')); // this is how to get another record on combobox store

                		var val = combo.getValue();
                		exp_part.proxy.setExtraParam('partno', val); // set parameter for exp_part store
	                	exp_part.load();
                		var stdpack = combo.getSelectedRecord().get('stdpack');
	                	Ext.ComponentQuery.query('hiddenfield[name=stdpack]')[0].setValue(stdpack);

                	}
                }
		    },{
		        fieldLabel: 'QTY',
		        maskRe: /[0-9.,]/,
		        name: 'qty',
		        allowBlank: false,
		        afterLabelTextTpl: required
		    },{
		        fieldLabel: 'LOT NO. SUPPLIER',
		        name: 'lotno',
		        listeners: {
		        	change:function(field){
		                field.setValue(field.getValue().toUpperCase());
		            }
		        }
		    },{
		    	xtype: 'datefield',
		        fieldLabel: 'PRODUCTION DATE',
		        name: 'prod_date',
		        format: 'Y-m-d',
		        allowBlank: false,
		        editable: false,
		        afterLabelTextTpl: required
		    }],
		    dockedItems: [toolbar_exp]
		});
	var grid_exp = Ext.create('Ext.grid.Panel', {
		    store: exp_control,
		    selModel: Ext.create('Ext.selection.CheckboxModel'),
		    viewConfig: {
		    	enableTextSelection  : true
		    },
		    columns: [
		    	{ header: 'NO', xtype: 'rownumberer', width: 55, sortable: false },
		    	{ text: 'UNIQUE ID', dataIndex: 'unid', hidden: true },
		    	{ text: 'ID', dataIndex: 'id', hidden: true },
		    	{ text: 'SUPPLIER CODE', dataIndex: 'suppcode', hidden: true },
		    	{ text: 'SUPPLIER NAME', dataIndex: 'suppname', flex: 1 },
		        { text: 'PART NUMBER', dataIndex: 'part_no', flex: 1 },
		        { text: 'QTY', dataIndex: 'qty', width: 70 },
		        { text: 'BALANCE', dataIndex: 'balance', width: 70 },
		        { text: 'LOT NUMBER', dataIndex: 'lotno', width: 120 },
		        { text: 'PRODUCTION DATE', dataIndex: 'prod_date',flex: 1 },
		        { text: 'EXPIRED DATE',
		          dataIndex: 'exp_date',
		          flex: 1,
		          // renderer: expiredstatus
		    	},
		    	{ text: 'EXP. AFTER OPEN', dataIndex: 'exp_after', hidden: true},
		        { text: 'INPUT BY', dataIndex: 'nik',flex: 1 }
		    ],
		    bbar: {
		    	xtype: 'pagingtoolbar',
		    	displayInfo	: true,
		    	store: exp_control,
		    	items: ['->',{
		    		xtype: 'textfield',
		    		name: 'fldsrc',
		    		width: 600,
		    		emptyText: 'Search part number in here...',
		    		listeners: {
		    			specialkey: function(field, e) {
							if (e.getKey() == e.ENTER) {
								exp_control.proxy.setExtraParam('fldsrc',field.getValue());
								exp_control.loadPage(1);
								// console.log(field.value);
							}
		                }
		    		}
		    	}]
		    },
		    listeners: {
		    	select: function(grid, rowIndex, colIndex) {
                    var rec = this.getSelectionModel().getSelection();
                    // if (!rec.data) {

                    // } else {
	                    var unid = rec[0].data.unid;
	                    var suppcode = rec[0].data.suppcode;
	                    var partno = rec[0].data.part_no;
	                    var qty = rec[0].data.qty;
	                    var lotno = rec[0].data.lotno;
	                    var prod_date = rec[0].data.prod_date;
	                    var nik = rec[0].data.nik;


	                    var txt_unid = Ext.ComponentQuery.query('hiddenfield[name=unid]')[0];
	                    var txt_supp = Ext.ComponentQuery.query('combobox[name=supplier]')[0];
	                    var txt_partno = Ext.ComponentQuery.query('textfield[name=partno]')[0];
	                    var txt_qty = Ext.ComponentQuery.query('textfield[name=qty]')[0];
	                    var txt_lotno = Ext.ComponentQuery.query('textfield[name=lotno]')[0];
	                    var txt_prod_date = Ext.ComponentQuery.query('datefield[name=prod_date]')[0];
	                    var txt_nik = Ext.ComponentQuery.query('textfield[name=nik]')[0];

	                    txt_unid.setValue(unid);
	                    txt_supp.setValue(suppcode);
	                    txt_partno.setValue(partno);
	                    txt_qty.setValue(qty);
	                    txt_lotno.setValue(lotno);
	                    txt_prod_date.setValue(prod_date);
	                    txt_nik.setValue(nik);

	                    exp_part.proxy.setExtraParam('supplier', suppcode); // set parameter for exp_part store
                		exp_part.load();


	                    Ext.ComponentQuery.query('textfield[name=nik]')[0].setEditable(false);
                    // }
                }
		    }
		});
	var panel_exp = Ext.create('Ext.panel.Panel',{
		border: true,
		layout: 'border',
	    defaults: {
	    	split: false,
	    	plain: true
	    },
	    items: [{
	        region: 'north',
	        layout: {
	        	type: 'hbox',
	        	pack: 'center'
	        },
	        bodyPadding: 10,
	        bodyStyle: {
	        	// background: '#ADD2ED',
	        	background: 'url("resources/bg-image.jpg") no-repeat center left',
	        	backgroundSize: 'cover'
	        },
	        items: form_exp
	    }, {
	        region: 'center',
	        layout: 'fit',
	        items: grid_exp
	    }]
	});

</script>