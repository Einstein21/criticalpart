<?php
    date_default_timezone_set('Asia/Jakarta');
?>

<script type="text/javascript">
    Ext.Loader.setPath('Ext.ux','../../extjs-5.1.1/build/examples/ux');
    
    function whitespace(val) {
        return '<div style="white-space: pre;">'+val+'</div>';
    }
    function combineDate(value, meta, record, rowIndex, colIndex, store) {
        value2 = record.get('JOBTIME');
        if (value == '---' || value === '-' || value === '' || value == null){
            return '<font style="white-space:normal;line-height:1.5;color:red;"> - </font>';
        }
        else{
            return '<font style="color:gray;">' + value + '</font><br><font style="color:blue;">' + value2 + '</font>';
        }
    }
    function combineScan(value, meta, record, rowIndex, colIndex, store)  {
		value2 = record.get('SCAN_NAME');
		value3 = record.get('SCAN_DATE');
		if (value === '---' || value === '-' || value === '' || value === null) {
			return '<font style="white-space:normal; line-height:1.5; color:red;"> _ </font>';
		}
		else{
			return '<font style="color:blue;">' + value3 + '</font><br><font style="color:gray;">' + value + '</font><font> '+ value2 +'</font>';
		}
	}

    var required    = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
    var dateObj     = new Date();
    var month       = dateObj.getUTCMonth() + 1; //months from 1-12
    var day         = dateObj.getUTCDate();
    var year        = dateObj.getUTCFullYear();
    var today       = year + "-" + month + "-" + day;

    var store_issueHeader = Ext.create('Ext.data.Store',{
        fields      : []
        ,autoLoad   : true 
        ,pageSize   : 25
        ,proxy      : { type    : 'ajax'
                        ,url    : 'json/mc_issueHeader.php'
                        ,reader : { type            : 'json'
                                    ,root           : 'rows'
                                    ,totalProperty  : 'totcount'
                        }
        }
        });

    var store_issueDetail = Ext.create('Ext.data.Store',{
        fields      : []
        ,autoLoad   : true 
        ,pageSize   : 25
        ,proxy      : { type    : 'ajax'
                        ,url    : 'json/mc_issueDetail.php'
                        ,reader : { type            : 'json'
                                    ,root           : 'rows'
                                    ,totalProperty  : 'totcount'
                        }
        }
    });
    var store_scanDetail = Ext.create('Ext.data.Store',{
        fields      : []
        ,autoLoad   : true 
        ,pageSize   : 25
        ,proxy      : { type    : 'ajax'
                        ,url    : 'json/mc_scanDetail.php'
                        ,reader : { type            : 'json'
                                    ,root           : 'rows'
                                    ,totalProperty  : 'totcount'
                        }
        }
    });
    var grid_scanDetail = Ext.create('Ext.grid.Panel',{ // grid data issue
        title       : 'DETAIL SCAN PART',
        header      : { titleAlign: 'center', style : 'letter-spacing: 2px' },
        store       : store_scanDetail,
        autoScroll  : true,
        width       : '100%',
        height      : 'auto',
        cellWrap    : true,
        selModel    : Ext.create('Ext.selection.CheckboxModel'),
        // plugins  : [Issue_cellEditing],
        viewConfig  : {
            stripeRows          : true,
            emptyText           : '<div class="empty-txt-main">No data to display.</div>',
            deferEmptyText      : false,
            enableTextSelection : true,
            getRowClass         : function(record) {
                var issueStatusDetail = record.get('ISSUESTATUS');
                if (issueStatusDetail == 1){
                    return 'ongoing';
                }
                else if(issueStatusDetail == 2){
                    return 'clear';
                }
                else{
                    return '';
                }
            }
        },
        columns: [
            { header: 'NO.', xtype: 'rownumberer', width: 55, sortable: false }
            ,{ text: 'JOB NO', dataIndex: 'JOBNO', width: 250, hidden:true }
            ,{ text: 'PART LABEL', dataIndex: 'PARTLABEL', width: 250, hidden:true }
            ,{ text: 'PART NO', dataIndex: 'PARTNO', width: 100, renderer: whitespace }
            ,{ text: 'QTY', dataIndex: 'QTYSCAN', width: 80 }
            ,{ text: 'SUPPLIER', dataIndex: 'SUPPCODE', width: 220, renderer: combineSupp }
            ,{ text: 'PO', dataIndex: 'PO', width: 80 }
            ,{ text: 'INVOICE', dataIndex: 'INVOICE', width: 80 }
            ,{ text: 'PROD DATE', dataIndex: 'PRODDATE', width: 80 }
            ,{ text: 'EXPIRED', dataIndex: 'EXPDATE', width: 80 }
            ,{ text: 'SCAN BY', dataIndex: 'SCAN_BY', width: 180, renderer: combineScan }
        ],
        tbar: [
        {   xtype       : 'button',
            id          : 'scanRefresh',
            html        : '<b style="color:#1664BC">Refresh</b>',
            scale       : 'medium',
            icon		: 'resources/reset.png',
            iconAlign   : 'right',
            handler     : function() {
            
                store_scanDetail.proxy.setExtraParam('jobno',Ext.getCmp('scanDetailJobno').getValue());
                store_scanDetail.loadPage(1);
            }
        }
        ,{  id      : 'scanDetailJobno'
            ,name   : 'scanDetailJobno'
            ,xtype	: 'hiddenfield'
            ,width  : 400
        }
      ]
    });
    
    var win_scanDetail = Ext.create('Ext.window.Window', {
        title       : ''
        ,layout     : 'border'
        ,header     : { titleAlign: 'center', style : 'letter-spacing: 2px' }
        ,closable   : true
        ,closeAction: 'hide'
        ,maximizable: true
        ,resizable  : true
        ,modal      : false
        ,constrain  : true
        ,height     : 600
        ,width      : 1000
        ,items      : [
            {
                region  : 'center'
                ,layout : 'fit'
                ,items  : [grid_scanDetail]
            }
        ]
    });

    var grid_issueHeader = Ext.create('Ext.grid.Panel',{
        title       : 'MODEL ISSUE BY OLL'
        ,header     : { titleAlign: 'center', style : 'letter-spacing: 2px' }
        ,store       : store_issueHeader
        ,autoScroll  : true
        ,width       : '100%'
        ,maxHeight   : 250
        ,cellWrap    : true
        ,viewConfig  : { 
            stripeRows          : true,
            emptyText           : '<div class="empty-txt-main">No data to display.</div>',
            deferEmptyText      : false,
            enableTextSelection : true,
            getRowClass         : function(record) {
                var issueStatusHeader = record.get('ISSUESTATUS');
                if (issueStatusHeader == 1){
                    return 'ongoing';
                }
                else if(issueStatusHeader == 2){
                    return 'clear';
                }
                else{
                    return '';
                }
            }
        }
        ,tbar: [
        {   xtype       : 'button',
            id          : 'irRefresh',
            html        : '<b style="color:#1664BC">R E F R E S H</b>',
            scale       : 'small',
            // icon		: 'resources/reset.png',
            // iconAlign   : 'left',
            handler     : function() {
                Ext.getCmp('src_issJobdate').setValue(today);
                Ext.getCmp('src_issJobno').setValue('');
                Ext.getCmp('src_issJobmodelname').setValue('');
                store_issueHeader.proxy.setExtraParam('jobdate', today);
                store_issueHeader.proxy.setExtraParam('jobdate', '');
                store_issueHeader.proxy.setExtraParam('jobno', '');
                store_issueHeader.proxy.setExtraParam('jobmodelname', '');
                store_issueDetail.proxy.setExtraParam('jobdate', today);
                store_issueDetail.proxy.setExtraParam('jobdate', '');
                store_issueDetail.proxy.setExtraParam('jobno', '');
                store_issueDetail.proxy.setExtraParam('modelname', '');
                store_issueHeader.loadPage(1);
                store_issueDetail.loadPage(1);
            }
        }]
        ,columns: [
            { header: 'NO.',        xtype: 'rownumberer',       width: 55, sortable: false }
            ,{ text: 'DATE',        dataIndex: 'JOBDATE',       width: 130, renderer:combineDate
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id              : 'src_issJobdate'
                                    ,xtype          : 'datefield'
                                    ,format		    : 'Y-m-d'
                                    ,submitFormat   : 'Y-m-d'
                                    ,mode		    : 'local' 
                                    ,flex           : 1
                                    ,margin         : '0 10 10 10'
                                    ,value 		    : new Date()
                                    ,listeners      : {
                                                     change: function(){
                                                        if (Ext.getCmp('src_issJobdate').getValue() == ''){
                                                            store_issueHeader.proxy.setExtraParam('jobdate', today);
                                                            store_issueDetail.proxy.setExtraParam('jobdate', today);
                                                        }
                                                        else{
                                                            store_issueHeader.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                            store_issueDetail.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                        }
                                                        store_issueHeader.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                        store_issueHeader.proxy.setExtraParam('jobno', Ext.getCmp('src_issJobno').getValue());
                                                        store_issueHeader.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_issJobmodelname').getValue());
                                                        
                                                        store_issueDetail.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                        store_issueDetail.proxy.setExtraParam('jobno', Ext.getCmp('src_issJobno').getValue());
                                                        store_issueDetail.proxy.setExtraParam('modelname', Ext.getCmp('src_issJobmodelname').getValue());
                                                        
                                                        store_issueHeader.loadPage(1);
                                                        store_issueDetail.loadPage(1);
                                                        
                                                    }
                                    }
                                }
                ]
            }
            ,{ text: 'MODEL NAME',  dataIndex: 'JOBMODELNAME',  width: 120 
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id          : 'src_issJobmodelname'
                                    ,xtype      : 'textfield'
                                    ,emptyText  : 'Search...'
                                    ,flex       : 1
                                    ,margin     : '0 10 10 10'
                                    ,listeners  : {
                                                    specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                            if (Ext.getCmp('src_issJobdate').getValue() == ''){
                                                                store_issueHeader.proxy.setExtraParam('jobdate', today);
                                                                store_issueDetail.proxy.setExtraParam('jobdate', today);
                                                            }
                                                            else{
                                                                store_issueHeader.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                                store_issueDetail.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                            }
                                                            store_issueHeader.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                            store_issueHeader.proxy.setExtraParam('jobno', Ext.getCmp('src_issJobno').getValue());
                                                            store_issueHeader.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_issJobmodelname').getValue());
                                                            store_issueDetail.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                            store_issueDetail.proxy.setExtraParam('jobno', Ext.getCmp('src_issJobno').getValue());
                                                            store_issueDetail.proxy.setExtraParam('modelname', Ext.getCmp('src_issJobmodelname').getValue());
                                                            
                                                            store_issueHeader.loadPage(1);
                                                            store_issueDetail.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
            }
            ,{ text: 'S.SERIAL',    dataIndex: 'JOBSTARTSERIAL', width: 80 }
            ,{ text: 'LOT SIZE',    dataIndex: 'JOBLOTSIZE',    width: 70 }
            ,{ text: 'PROCESS',     dataIndex: 'PROCESS',       width: 70 }
            ,{ text: 'LINE',        dataIndex: 'JOBLINE',       width: 70 }
            ,{ text: 'PWB NO',      dataIndex: 'JOBPWBNO',      width: 180, hidden:true }
            ,{ text: 'PWB NAME',    dataIndex: 'JOBPWBNAME',    width: 180, hidden:true }
            ,{ text: 'LOT NO',      dataIndex: 'JOBLOTNO',      width: 70 }
            ,{ text: 'FILE NAME',   dataIndex: 'JOBFILE',       flex : 1 }
            ,{ text: 'JOB NO',      dataIndex: 'JOBNO',         width: 250
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id          : 'src_issJobno'
                                    ,xtype      : 'textfield'
                                    ,emptyText  : 'Search...'
                                    ,flex       : 1
                                    ,margin     : '0 10 10 10'
                                    ,listeners  : {
                                                    specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                            if (Ext.getCmp('src_issJobdate').getValue() == ''){
                                                                store_issueHeader.proxy.setExtraParam('jobdate', today);
                                                                store_issueDetail.proxy.setExtraParam('jobdate', today);
                                                            }
                                                            else{
                                                                store_issueHeader.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                                store_issueDetail.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                            }
                                                            store_issueHeader.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                            store_issueHeader.proxy.setExtraParam('jobno', Ext.getCmp('src_issJobno').getValue());
                                                            store_issueHeader.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_issJobmodelname').getValue());
                                                            store_issueDetail.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                                                            store_issueDetail.proxy.setExtraParam('jobno', Ext.getCmp('src_issJobno').getValue());
                                                            store_issueDetail.proxy.setExtraParam('modelname', Ext.getCmp('src_issJobmodelname').getValue());
                                                            
                                                            store_issueHeader.loadPage(1);
                                                            store_issueDetail.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
             }
            ,{ text: 'CAVITY',      dataIndex: 'CAVITY',        width: 180, hidden:true }
            ,{ text: 'MC RH',       dataIndex: 'JOBMCRH',       width: 180, hidden:true }
            ,{ text: 'SEQ ID',      dataIndex: 'SEQID',         width: 50, hidden:true }
            ,{ text: 'ISSUE STATUS', dataIndex: 'ISSUESTATUS',  width: 50, hidden:true }
            ,{ text: 'REMARK',      dataIndex: 'REMARK',        width: 180, hidden:true }
            ,{ text: 'CREATED BY',  dataIndex: 'CREATED_BY',    width: 180, hidden:true }
            ,{ text: 'CREATED NAME', dataIndex: 'CREATED_NAME', width: 180, hidden:true }
            ,{ text: 'CREATED AT',  dataIndex: 'CREATED_AT',    width: 180, hidden:true }
        ]
        ,listeners  : {
            itemdblclick: function(dv,record,item,index,e){
                var rec = grid_issueHeader.getSelectionModel().getSelection();
                if (rec!=0){
                    var issueJobno  = rec[0].data.JOBNO;
                    console.log(issueJobno);

                    if (Ext.getCmp('src_issJobdate').getValue() == ''){
                        store_issueHeader.proxy.setExtraParam('jobdate', today);
                        store_issueDetail.proxy.setExtraParam('jobdate', today);
                    }
                    else{
                        store_issueHeader.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                        store_issueDetail.proxy.setExtraParam('jobdate', Ext.getCmp('src_issJobdate').getValue());
                    }
                    store_issueHeader.proxy.setExtraParam('jobno', issueJobno);
                    store_issueDetail.proxy.setExtraParam('jobno', issueJobno);
                    
                    store_issueHeader.loadPage(1);
                    store_issueDetail.loadPage(1);
                }
                else{
                    Ext.Msg.show({
                        title   : 'Warning',
                        icon    : Ext.Msg.ERROR,
                        msg     : "No data selected.",
                        button  : Ext.Msg.OK  
                    });
                }
            }
        }
    });

    var grid_issueDetail = Ext.create('Ext.grid.Panel',{ // grid data issue
        title       : 'MC ISSUE PWB AND IC',
        header     : { titleAlign: 'center', style : 'letter-spacing: 2px' },
        store       : store_issueDetail,
        autoScroll  : true,
        width       : '100%',
        height      : 'auto',
        cellWrap    : true,
        // selModel    : Ext.create('Ext.selection.CheckboxModel'),
        viewConfig  : { 
                stripeRows          : true,
                emptyText           : '<div class="empty-txt-main">No data to display.</div>',
                deferEmptyText      : false,
                enableTextSelection : true,
                getRowClass         : function(record) {
                    var issueStatusHeader = record.get('ISSUESTATUS');
                    if (issueStatusHeader == 1){
                        return 'ongoing';
                    }
                    else if(issueStatusHeader == 2){
                        return 'clear';
                    }
                    else{
                        return '';
                    }
                }
        },
        columns: [
            { header: 'NO.', xtype: 'rownumberer', width: 55, sortable: false }
            ,{ text: 'JOB NO', dataIndex: 'JOBNO', width: 250, hidden:true }
            ,{ text: 'DATE', dataIndex: 'JOBDATE', width: 80, renderer:combineDate }
            ,{ text: 'MODEL NAME', dataIndex: 'MODELNAME', width: 120, hidden:true }
            ,{ text: 'PROCESS', dataIndex: 'PROCESS', width: 70, hidden:true }
            ,{ text: 'MC RH', dataIndex: 'JOBMCRH', width: 180, hidden:true }
            ,{ text: 'LOT SIZE', dataIndex: 'LOTSIZE', width: 70, hidden:true }
            ,{ text: 'CAVITY', dataIndex: 'CAVITY', width: 70, hidden:true }
            ,{ text: 'SEQ ID', dataIndex: 'SEQID', width: 50, hidden:true }
            ,{ text: 'ZFEEDER', dataIndex: 'ZFEEDER', width: 50, hidden:true }
            ,{ text: 'PART NO', dataIndex: 'PARTNO', flex: 1 }
            ,{ text: 'DEMAND', dataIndex: 'DEMAND', width: 80 }
            ,{ text: 'QTY ISSUE', dataIndex: 'QTYISSUE', width: 80 }
            ,{ text: 'ISSUE STATUS', dataIndex: 'ISSUESTATUS', width: 50, hidden:true }
            ,{ text: 'REMARK', dataIndex: 'REMARK', width: 180, hidden:true }
            ,{ text: 'CREATED BY', dataIndex: 'CREATED_BY', width: 180 }
            ,{ text: 'CREATED NAME', dataIndex: 'CREATED_NAME', width: 180 }
            ,{ text: 'CREATED AT', dataIndex: 'CREATED_AT', width: 180 }
        ]
        ,listeners  : {
            itemdblclick: function(dv,record,item,index,e){
                var rec = grid_issueDetail.getSelectionModel().getSelection();
                if (rec!=0){
                    var detailJobno  = rec[0].data.JOBNO;
                    console.log(detailJobno);
                    
                    if (win_scanDetail.isVisible()) { 
                        win_scanDetail.hide(); 
                        win_scanDetail.show(); 
                        Ext.getCmp('scanDetailJobno').setValue(detailJobno);
                        store_scanDetail.proxy.setExtraParam('jobno', detailJobno);
                        store_scanDetail.loadPage(1);
                    } 
                    else { 
                        win_scanDetail.show();
                        Ext.getCmp('scanDetailJobno').setValue(detailJobno);
                        store_scanDetail.proxy.setExtraParam('jobno', detailJobno);
                        store_scanDetail.loadPage(1);
                    }
                    
                }
                else{
                    Ext.Msg.show({
                        title   : 'Warning',
                        icon    : Ext.Msg.ERROR,
                        msg     : "No data selected.",
                        button  : Ext.Msg.OK  
                    });
                }
                
            }
        }
    });

    var panel_issuePart = Ext.create('Ext.panel.Panel',{
        border  : true
        ,layout : 'border'
        ,default: { split: false
                    ,plain: true }
        ,items  : [
            {   region      : 'north'
                ,layout     : 'fit'
                ,items      : grid_issueHeader    
            }
            ,{  region      : 'center'
                ,layout     : 'fit'
                ,items      : grid_issueDetail
            }
        ]
    });
</script>