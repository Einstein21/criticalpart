<?php
date_default_timezone_set('Asia/Jakarta');
?>
<script type="text/javascript">
    Ext.Loader.setPath('Ext.ux','../../extjs-5.1.1/build/examples/ux');
    function whitespace(val) {
		return '<div style="white-space: pre;">'+val+'</div>';
	}
    function combineDate(value, meta, record, rowIndex, colIndex, store) {
        value2 = record.get('JOBTIME');
        if (value == '---' || value === '-' || value === '' || value == null){
            return '<font style="white-space:normal;line-height:1.5;color:red;"> - </font>';
        }
        else{
            return value + '<br><font style="color:blue;">' + value2 + '</font>';
        }
    }
    // var clock = Ext.create('Ext.toolbar.TextItem', {
    //   text	:Ext.Date.format(new Date(), 'g:i:s A'), style : 'color:#C0392B;font-size:24px;font-family: \'Raleway\';'
    // });
    var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    var today = year + "-" + month + "-" + day;

    var store_oll = Ext.create('Ext.data.Store',{
        fields      : []
        ,autoLoad   : true 
        // ,pageSize   : 25
        ,proxy      : { type    : 'ajax'
                        ,url    : 'json/mc_dispIssueOll.php'
                        ,reader : { type    : 'json'
                                    ,root   : 'rows'
                                    ,totalProperty : 'totcount'
                        }
        }
    });
    var store_mcissueHeader = Ext.create('Ext.data.Store',{
        fields      : []
        // ,autoLoad   : true 
        // ,pageSize   : 25
        ,proxy      : { type    : 'ajax'
                        ,url    : 'json/mc_winIssueOllHeader.php'
                        ,reader : { type    : 'json'
                                    ,root   : 'rows'
                                    ,totalProperty : 'totcount'
                        }
        }
    });
    var store_mcissueDetail = Ext.create('Ext.data.Store',{
        fields      : []
        // ,autoLoad   : true 
        // ,pageSize   : 25
        ,proxy      : { type    : 'ajax'
                        ,url    : 'json/mc_winIssueOllDetail.php'
                        ,reader : { type    : 'json'
                                    ,root   : 'rows'
                                    ,totalProperty : 'totcount'
                        }
        }
    });
    var grid_mcIssueHeader = Ext.create('Ext.grid.Panel',{ // grid data issue
        title       : 'MODEL ISSUE FROM OLL',
        header      : { titleAlign: 'center', style : 'letter-spacing: 2px' },
        store       : store_mcissueHeader,
        autoScroll  : true,
        // width       : '100%',
        // height      : 150,
        // cellWrap    : true,
        // selModel    : Ext.create('Ext.selection.CheckboxModel'),
        // plugins  : [Issue_cellEditing],
        viewConfig  : {
            stripeRows          : true,
            emptyText           : '<div class="empty-txt-main">No data to display.</div>',
            deferEmptyText      : false,
            enableTextSelection : true,
            getRowClass         : function(record) {
                var issueStatusHeader = record.get('ISSUESTATUS');
                if (issueStatusHeader == 1){
                    return 'ongoing';
                }
                else if(issueStatusHeader == 2){
                    return 'clear';
                }
                else{
                    return '';
                }
            }
        },
        columns: [
          { header: 'NO.', xtype: 'rownumberer', width: 55, sortable: false }
          ,{ text: 'JOB NO', dataIndex: 'JOBNO', width: 250, hidden:true }
          ,{ text: 'DATE', dataIndex: 'JOBDATE', width: 80, renderer:combineDate }
          ,{ text: 'FILE NAME', dataIndex: 'JOBFILE', flex:1 }
          ,{ text: 'MODEL NAME', dataIndex: 'JOBMODELNAME', width: 120 }
          ,{ text: 'PWB NO', dataIndex: 'JOBPWBNO', width: 180, hidden:true }
          ,{ text: 'LINE', dataIndex: 'JOBLINE', width: 70 }
          ,{ text: 'LOT SIZE', dataIndex: 'JOBLOTSIZE', width: 70 }
          ,{ text: 'S.SERIAL', dataIndex: 'JOBSTARTSERIAL', width: 80 }
          ,{ text: 'LOT NO', dataIndex: 'JOBLOTNO', width: 70 }
          ,{ text: 'PWB NAME', dataIndex: 'JOBPWBNAME', width: 180, hidden:true }
          ,{ text: 'CAVITY', dataIndex: 'CAVITY', width: 180, hidden:true }
          ,{ text: 'MC RH', dataIndex: 'JOBMCRH', width: 180, hidden:true }
          ,{ text: 'PROCESS', dataIndex: 'PROCESS', width: 70 }
          ,{ text: 'SEQ ID', dataIndex: 'SEQID', width: 50, hidden:true }
          ,{ text: 'ISSUE STATUS', dataIndex: 'ISSUESTATUS', width: 50, hidden:true }
          ,{ text: 'REMARK', dataIndex: 'REMARK', width: 180, hidden:true }
          ,{ text: 'CREATED BY', dataIndex: 'CREATED_BY', width: 180, hidden:true }
          ,{ text: 'CREATED NAME', dataIndex: 'CREATED_NAME', width: 180, hidden:true }
          ,{ text: 'CREATED AT', dataIndex: 'CREATED_AT', width: 180, hidden:true }
          
      ]
    });
    var grid_mcIssueDetail = Ext.create('Ext.grid.Panel',{ // grid data issue
        title       : 'PART ISSUE',
        header     : { titleAlign: 'center', style : 'letter-spacing: 2px' },
        store       : store_mcissueDetail,
        autoScroll  : true,
        width       : '100%',
        height      : 'auto',
        cellWrap    : true,
        selModel    : Ext.create('Ext.selection.CheckboxModel'),
        // plugins  : [Issue_cellEditing],
        viewConfig  : {
            stripeRows          : true,
            emptyText           : '<div class="empty-txt-main">No data to display.</div>',
            deferEmptyText      : false,
            enableTextSelection : true,
            getRowClass         : function(record) {
                var issueStatusDetail = record.get('ISSUESTATUS');
                if (issueStatusDetail == 1){
                    return 'ongoing';
                }
                else if(issueStatusDetail == 2){
                    return 'clear';
                }
                else{
                    return '';
                }
            }
        },
        columns: [
            { header: 'NO.', xtype: 'rownumberer', width: 55, sortable: false }
            ,{ text: 'JOB NO', dataIndex: 'JOBNO', width: 250, hidden:true }
            ,{ text: 'DATE', dataIndex: 'JOBDATE', width: 80, renderer:combineDate }
            ,{ text: 'MODEL NAME', dataIndex: 'MODELNAME', width: 120, hidden:true }
            ,{ text: 'PROCESS', dataIndex: 'PROCESS', width: 70, hidden:true }
            ,{ text: 'MC RH', dataIndex: 'JOBMCRH', width: 180, hidden:true }
            ,{ text: 'LOT SIZE', dataIndex: 'LOTSIZE', width: 70, hidden:true }
            ,{ text: 'CAVITY', dataIndex: 'CAVITY', width: 70, hidden:true }
            ,{ text: 'SEQ ID', dataIndex: 'SEQID', width: 50, hidden:true }
            ,{ text: 'ZFEEDER', dataIndex: 'ZFEEDER', width: 50, hidden:true }
            ,{ text: 'PART NO', dataIndex: 'PARTNO', flex: 1, renderer: whitespace }
            ,{ text: 'DEMAND', dataIndex: 'DEMAND', width: 80 }
            ,{ text: 'QTY ISSUE', dataIndex: 'QTYISSUE', width: 80 }
            ,{ text: 'ISSUE STATUS', dataIndex: 'ISSUESTATUS', width: 50, hidden:true }
            ,{ text: 'REMARK', dataIndex: 'REMARK', width: 180, hidden:true }
            ,{ text: 'SCAN BY', dataIndex: 'SCAN_BY', width: 180 }
            ,{ text: 'SCAN NAME', dataIndex: 'SCAN_NAME', width: 180 }
            ,{ text: 'SCAN AT', dataIndex: 'SCAN_DATE', width: 180 }
        ],
        tbar: [
        {   xtype       : 'button',
            id          : 'refresh',
            html        : '<b style="color:#1664BC">Refresh</b>',
            scale       : 'large',
            icon		: 'resources/reset.png',
            iconAlign   : 'top',
            handler     : function() {
            
                store_mcissueHeader.proxy.setExtraParam('jobno',Ext.getCmp('issDetailJobno').getValue());
                store_mcissueHeader.proxy.setExtraParam('jobdate',Ext.getCmp('issDetailJobdate').getValue());
                store_mcissueHeader.proxy.setExtraParam('jobtime',Ext.getCmp('issDetailJobtime').getValue());
                store_mcissueHeader.proxy.setExtraParam('jobmodelname',Ext.getCmp('issDetailModelName').getValue());
                store_mcissueHeader.loadPage(1);

                store_mcissueDetail.proxy.setExtraParam('jobno',Ext.getCmp('issDetailJobno').getValue());
                store_mcissueDetail.proxy.setExtraParam('jobdate',Ext.getCmp('issDetailJobdate').getValue());
                store_mcissueDetail.proxy.setExtraParam('jobtime',Ext.getCmp('issDetailJobtime').getValue());
                store_mcissueDetail.proxy.setExtraParam('jobmodelname',Ext.getCmp('issDetailModelName').getValue());
                store_mcissueDetail.loadPage(1);

                store_issueHeader.loadPage(1);
                store_issueDetail.loadPage(1);
            }
        },
        {   xtype       : 'button',
            id          : 'issSplitLabel',
            html        : '<b style="color:#1664BC">SPLIT LABEL</b>',
            scale       : 'large',
            icon		: 'resources/split.png',
            iconAlign   : 'top',
            handler: function() {
                Ext.create('Ext.window.Window', {
                    title: 'FORM SPLIT LABEL',
                    // height: 200,
                    border: false,
                    padding: '5 5 5 5',
                    width: 400,
                    layout: 'fit',
                    animateTarget: this,
                    items: form_issSplitlabel,
                    closeAction: 'hide',
                    listeners: {
                        activate: function() {
                            Ext.ComponentQuery.query('button[name=splitQtyLabel]')[0].setDisabled(true);
                        },
                        close: function() {
                            Ext.ComponentQuery.query('button[name=splitQtyLabel]')[0].setDisabled(false);
                        }
                    }
                }).show();
            }
        }
        ,{  id      : 'issDetailJobno'
            ,name   : 'issDetailJobno'
            ,xtype	: 'hiddenfield'
            ,width  : 30
        }
        ,{  id      : 'issDetailJobdate'
            ,name   : 'issDetailJobdate'
            ,xtype	: 'hiddenfield'
            ,width  : 30
        }
        ,{  id      : 'issDetailJobtime'
            ,name   : 'issDetailJobtime'
            ,xtype	: 'hiddenfield'
            ,width  : 30
        }
        ,{  id      : 'issDetailModelName'
            ,name   : 'issDetailModelName'
            ,xtype	: 'hiddenfield'
            ,width  : 30
        }
        ,'->'
        ,{  id              : 'scanIssueNik'
            ,name           : 'scanIssueNik'
            ,xtype	        : 'textfield'
            ,emptyText      : 'SCAN NIK'
            ,allowBlank     : false
            ,height         : 70
            ,fieldStyle     : 'font-size:30px; text-align: center;'
            ,width          : 200
            ,listeners      : {
                specialkey      : function(field, e) {
                    if (e.getKey() == e.ENTER) {
                        var issueNik = field.getValue();
                        var len = issueNik.length;
                        if (len != 0) {
                            Ext.Ajax.request({
                            url		: 'response/checkNik.php',
                            method	: 'POST',
                            params	: {issueNik},
                            success : function(obj, action) {
                                        var resp = obj.responseText;
                                        var warning = JSON.parse(resp);
                                        
                                        if(warning.success == true ){
                                            Ext.getCmp('scanIssuePartno').focus(true);
                                        }
                                        else{
                                            Ext.Msg.show({
                                                title   : 'FAILURE',
                                                icon	: Ext.Msg.ERROR,
                                                msg     :  warning.msg,
                                                buttons : Ext.Msg.OK
                                            });
                                            field.reset();
                                            field.focus();
                                        }
                                    }
                        });
                        }
                    }
                }
            }
        }
        ,{  id              : 'scanIssuePartno'
            ,name           : 'scanIssuePartno'
            ,xtype	        : 'textfield'
            ,allowBlank     : false
            ,emptyText      : 'SCAN PART LABEL'
            ,height         : 70
            ,fieldStyle     : 'font-size:30px; text-align: center;'
            ,width          : 300
            ,listeners      : {
                afterrender : function(field) { field.focus(true); },
                specialkey  : function(field, e, grid, rowIndex, colIndex) {
                    if (e.getKey() == e.ENTER) {
                        var rec = grid_mcIssueDetail.getSelectionModel().getSelection();
                        if (rec!=0){
                            var issID = rec[0].data.ID;
                            var txtval = field.getValue();
                            var len = txtval.length;
                            if (len != 0) {
                                var scanNik     = Ext.getCmp('scanIssueNik').getValue();
                                var scanPart    = field.getValue();
                                var scanJobno   = Ext.getCmp('issDetailJobno').getValue();
                                var scanJobdate = Ext.getCmp('issDetailJobdate').getValue();
                                var scanJobtime = Ext.getCmp('issDetailJobtime').getValue();


                                Ext.Ajax.request({
                                    url		: 'response/MC_scanIssue.php',
                                    method	: 'POST',
                                    params	: {scanNik,scanPart,scanJobno, scanJobdate,scanJobtime,issID},
                                    success : function(obj, action) {
                                        var resp = obj.responseText;
                                        var sts = resp.split("|");
                                        console.log(sts[0]);
                                        console.log(sts[1]);
                                        console.log(sts[2]);
                                        var audio = new Audio('resources/sound/'+sts[2]+'.mp3');
                                        audio.play();   
                                        if(sts[0] == 0 ){
                                            
                                            Ext.Msg.show({
                                                title   : 'Failure',
                                                icon	: Ext.Msg.ERROR,
                                                msg     : '<font style="font-size:20px;color:red;">'+sts[1]+'</font>',
                                                buttons : Ext.Msg.OK
                                            });
                                            if(!scanNik){
                                                Ext.getCmp('scanIssueNik').focus(true);
                                            }
                                            else{
                                                field.reset();
                                                field.focus();
                                            }
                                        }
                                        else{
                                            store_mcissueDetail.loadPage(1);
                                            store_mcissueHeader.loadPage(1);
                                            if(!scanNik){
                                                Ext.getCmp('scanIssueNik').focus(true);
                                            }
                                            else{
                                                field.reset();
                                                field.focus();
                                            }
                                        }
                                    }
                                });
                            }
                        }
                        else{
                            Ext.Msg.show({
                                title   : 'OOPS, AN ERROR JUST HAPPEN !',
                                icons   : Ext.Msg.ERROR,
                                msg     : 'Select any field you desire to issue !',
                                buttons : Ext.Msg.OK
                            });
                            field.reset();
                            field.focus();
                        }
                        
                    }
                }
            }
        }
      ]
    });
    var form_issSplitlabel = Ext.create('Ext.form.Panel',{
		name: 'form_issSplitlabel',
		layout: 'anchor',
		defaults: {
		    anchor: '100%',
		    padding: '5 5 5 5',
		    fieldStyle: 'font-size:20px;text-align:center;'
		},
		defaultType: 'textfield',
		items: [{
			emptyText: 'SCAN NIK',
			id: 'issSplitNik',
			name: 'issSplitNik',
			// value: Ext.getCmp('scanIssueNik').getValue(),
			minLength : 5,
			maxLength : 8,
			allowBlank: false,
			// selectOnFocus: true,
			listeners: {
				afterrender: function(field) { 
                    var scnik = Ext.getCmp('scanIssueNik').getValue();
                    field.setValue(scnik); 
                    field.setEditable(false);
                },
		        specialkey: function(field, e) {
		        	if (e.getKey() == e.ENTER) {
						var form = this.up('form').getForm();
		        		var txtval = field.getValue();
						var len = txtval.length;
						if (len != 0) {
							if(form.isValid()) {
								form.submit({
									url: 'response/mc_issSplitlabel.php',
									waitMsg : 'Checking data, please wait..',
									success : function(form, action) {
				                        Ext.getCmp('issSplitLabel2').setDisabled(false);
										Ext.getCmp('issSplitLabel2').focus(true);
				                    },
				                    failure : function(form, action) {
				                        Ext.Msg.show({
					                        title   : 'OOPS, AN ERROR JUST HAPPEN !',
					                        icons   : Ext.Msg.ERROR,
					                        msg     : action.result.msg,
					                        buttons : Ext.Msg.OK
				                        });
				                         field.setValue("");
				                    }
								});
							}
						} else {
							Ext.getCmp('issSplitLabel2').setDisabled(false);
						}
					}
		        }
			}
		}, {
			emptyText: 'SCAN LABEL',
			id: 'issSplitLabel2',			
			name: 'issSplitLabel2',			
			// disabled: true,
			listeners: {
                afterrender: function(field) { field.focus(true); },
				specialkey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						var form = this.up('form').getForm();
		        		var txtval = field.getValue();
						var len = txtval.length;
						if (len != 0) {
							if(form.isValid()) {
								form.submit({
									url: 'response/mc_issSplitlabel.php',
									waitMsg : 'Checking data, please wait..',
									success : function(form, action) {
				                        var qtynext = txtval.substr(24,5);
										if(qtynext == 1){
											Ext.Msg.show({
						                        title   : 'OOPS, AN ERROR JUST HAPPEN !',
						                        icons   : Ext.Msg.ERROR,
						                        msg     : '<h2 style=\"text-align: center; color: red;\"><b>ZERO QUANTITY</b></h2>',
						                        buttons : Ext.Msg.OK
					                        });
					                        field.setValue("");
										}
										else{
											 Ext.getCmp('issSplitQty').setDisabled(false);
											Ext.getCmp('issSplitQty').focus(true);
											Ext.getCmp('issSplitQty').setValue(qtynext-1);
											Ext.getCmp('issSplitQty').setMaxValue(qtynext-1);
										}
				                    },
				                    failure : function(form, action) {
				                        Ext.Msg.show({
					                        title   : 'OOPS, AN ERROR JUST HAPPEN !',
					                        icons   : Ext.Msg.ERROR,
					                        msg     : action.result.msg,
					                        buttons : Ext.Msg.OK
				                        });
				                        field.setValue("");
				                    }
								});
							}
						} else {
							Ext.getCmp('issSplitQty').setDisabled(true);
						}
					}
		        },
		        change: function(field) {
					var txtval = field.getValue();
					var len = txtval.length;
					if (len == 0) {
						Ext.getCmp('issSplitQty').setDisabled(true);
					} else {
						field.setValue(field.getValue().toUpperCase());
					}

				}
			}
		}, {
			emptyText: 'SPLIT QTY',
			id: 'issSplitQty',
			name: 'issSplitQty',
			xtype: 'numberfield',
			disabled: true,
			listeners: {
				specialkey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						var form = this.up('form').getForm();
						if(form.isValid()) {
							form.submit({
								url: 'response/mc_issSplitlabel.php',
								waitMsg : 'Now transfering data, please wait..',
								success : function(form, action) {
			                        Ext.toast({
									     html: 'Data Saved',
									     title: 'SUCCESS - INOFRMATION',
									     width: 200,
									     align: 't'
									 });
			                        store_expData.loadPage(1);
			                        Ext.getCmp('issSplitQty').setValue("");
			                        Ext.getCmp('issSplitLabel2').setValue("");
			                        // Ext.getCmp('issSplitNik').setValue("");
			                        Ext.getCmp('issSplitQty').setDisabled(true);
			                        Ext.getCmp('issSplitLabel2').focus(true);
			                        // Ext.getCmp('issSplitLabel2').setDisabled(true);
			                        // Ext.getCmp('issSplitNik').focus(true);
									// field.reset();
			                    },
			                    failure : function(form, action) {
			                        Ext.Msg.show({
				                        title   : 'OOPS, AN ERROR JUST HAPPEN !',
				                        icons   : Ext.Msg.ERROR,
				                        msg     : '<h3 style="color:red;">' + action.result.msg + '</h3>',
				                        buttons : Ext.Msg.OK
			                        });
			                    }
							});
						}
					}
		        }
			}
		}, 
        // {
		// 	xtype: 'button',
		// 	text: 'RESET',
		// 	id: 'issSplitReset',
		// 	name: 'issSplitReset',
		// 	handler: function() {
        //         // this.up('form').getForm().reset();
                
        //         Ext.getCmp('issSplitQty').setValue("");
        //         Ext.getCmp('issSplitLabel2').setValue("");
        //         Ext.getCmp('issSplitQty').setDisabled(true);
        //         Ext.getCmp('issSplitLabel2').focus(true);

		// 		// Ext.getCmp('issSplitQty').setDisabled(true);
		// 		// Ext.getCmp('issSplitLabel2').setDisabled(true);
		// 		// Ext.getCmp('issSplitNik').setEditable(true);
		// 		// Ext.getCmp('issSplitNik').focus(true);
		// 	}
		// }, 
		{
			xtype: 'button',
			text: 'RESET',
			id: 'issSplitReset',
			name: 'issSplitReset',
			anchor: '47%',
			margin		: '0 0 0 15',
			handler: function() {
				this.up('form').getForm().reset();
				Ext.getCmp('splitqty').setDisabled(true);
				Ext.getCmp('splitlabel').setDisabled(true);
				Ext.getCmp('splitnik').focus(true);
			}
		},
		{
			xtype: 'button',
			text: 'SPLIT',
			id: 'issSplitStart',
			name: 'issSplitStart',
			anchor: '47%',
			margin		: '0 0 0 10',
			handler: function() {
				var form = this.up('form').getForm();
                if(form.isValid()) {
                    form.submit({
                        url: 'response/mc_issSplitlabel.php',
                        waitMsg : 'Now transfering data, please wait..',
                        success : function(form, action) {
                            Ext.toast({
                                    html: 'Data Saved',
                                    title: 'SUCCESS - INOFRMATION',
                                    width: 200,
                                    align: 't'
                                });
                            store_expData.loadPage(1);
                            Ext.getCmp('issSplitQty').setValue("");
                            Ext.getCmp('issSplitLabel2').setValue("");
                            Ext.getCmp('issSplitQty').setDisabled(true);
                            Ext.getCmp('issSplitLabel2').focus(true);
                        },
                        failure : function(form, action) {
                            Ext.Msg.show({
                                title   : 'OOPS, AN ERROR JUST HAPPEN !',
                                icons   : Ext.Msg.ERROR,
                                msg     : '<h3 style="color:red;">' + action.result.msg + '</h3>',
                                buttons : Ext.Msg.OK
                            });
                        }
                    });
                }
			}
		}]
	});
    var win_mcissue = Ext.create('Ext.window.Window', {
        title       : ''
        ,layout     : 'border'
        ,header     : { titleAlign: 'center', style : 'letter-spacing: 2px' }
        ,closable   : true
        ,closeAction: 'hide'
        ,maximizable: true
        // ,maximized  : true
        ,resizable  : true
        ,modal      : false
        ,constrain  : true
        ,height  : 600
        ,width   : 1000
        ,items      : [
            {
                region  : 'north'
                ,layout : 'fit'
                ,height : 150
                ,items  : [grid_mcIssueHeader]
            }
            ,{
                region  : 'center'
                ,layout : 'fit'
                ,items  : [grid_mcIssueDetail]
            }
        ]
    });
    var grid_oll = Ext.create('Ext.grid.Panel',{
        store       : store_oll
        ,viewConfig : { enableTextSelection : true 
                        ,getRowClass: function(record) {
                                var issueStatusOll = record.get('JOBMCISSUE');
                                if (issueStatusOll == 1){
                                    return 'ongoing';
                                }
                                else if(issueStatusOll == 2){
                                    return 'clear';
                                }
                            }
        }
        ,columns    : [
            { header: 'NO' ,xtype: 'rownumberer' ,width:50 ,sortable: false }
            ,{  header      : 'DATE'
                ,dataIndex  : 'JOBDATE'
                ,width      : 145
                ,renderer   : combineDate
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id              : 'src_olljobdate'
                                    ,xtype          : 'datefield'
                                    ,format		    : 'Y-m-d'
                                    ,submitFormat   : 'Y-m-d'
                                    ,mode		    : 'local'
                                    ,flex           : 1
                                    ,margin         : '0 10 10 10'
                                    ,value 		    : new Date()
                                    ,listeners      : {
                                                     change: function(){
                                                        store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                        store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                        store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                        store_oll.proxy.setExtraParam('jobstartserial', Ext.getCmp('src_ollstartserial').getValue());
                                                        store_oll.proxy.setExtraParam('process', Ext.getCmp('src_ollprocess').getValue());
                                                        store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                        store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                        store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                        store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                        store_oll.proxy.setExtraParam('jobmcrh', Ext.getCmp('src_olljobmcrh').getValue());
                                                        store_oll.loadPage(1);
                                                    }
                                                    ,specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                            store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                            store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                            store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                            store_oll.proxy.setExtraParam('jobstartserial', Ext.getCmp('src_ollstartserial').getValue());
                                                            store_oll.proxy.setExtraParam('process', Ext.getCmp('src_ollprocess').getValue());
                                                            store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                            store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                            store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                            store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                            store_oll.proxy.setExtraParam('jobmcrh', Ext.getCmp('src_olljobmcrh').getValue());
                                                            store_oll.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
            }
            ,{  header      : 'MODEL NAME'
                ,dataIndex  : 'JOBMODELNAME'
                ,width      : 130
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id          : 'src_olljobmodelname'
                                    ,xtype      : 'textfield'
                                    ,emptyText  : 'Search...'
                                    ,flex       : 1
                                    ,margin     : '0 10 10 10'
                                    ,listeners  : {
                                                    specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                            if (Ext.getCmp('src_olljobdate').getValue() == ''){
                                                                store_oll.proxy.setExtraParam('jobdate', today);
                                                            }
                                                            else{
                                                                store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                            }
                                                            store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                            store_oll.proxy.setExtraParam('jobmodelname', field.getValue());
                                                            store_oll.proxy.setExtraParam('jobstartserial', Ext.getCmp('src_ollstartserial').getValue());
                                                            store_oll.proxy.setExtraParam('process', Ext.getCmp('src_ollprocess').getValue());
                                                            store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                            store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                            store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                            store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                            store_oll.proxy.setExtraParam('jobmcrh', Ext.getCmp('src_olljobmcrh').getValue());
                                                            store_oll.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
            }
            ,{  header      : 'START SERIAL'
                ,dataIndex  : 'JOBSTARTSERIAL'
                ,width      : 120
                ,align      : 'right'
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id          : 'src_ollstartserial'
                                    ,xtype      : 'textfield'
                                    ,emptyText  : 'Search...'
                                    ,flex       : 1
                                    ,margin     : '0 10 10 10'
                                    ,listeners  : {
                                                    specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                            if (Ext.getCmp('src_olljobdate').getValue() == ''){
                                                                store_oll.proxy.setExtraParam('jobdate', today);
                                                            }
                                                            else{
                                                                store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                            }
                                                            store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                            store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                            store_oll.proxy.setExtraParam('jobstartserial', field.getValue());
                                                            store_oll.proxy.setExtraParam('process', Ext.getCmp('src_ollprocess').getValue());
                                                            store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                            store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                            store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                            store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                            store_oll.proxy.setExtraParam('jobmcrh', Ext.getCmp('src_olljobmcrh').getValue());
                                                            store_oll.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
            }
            ,{  header      : 'LOT SIZE'
                ,dataIndex  : 'JOBLOTSIZE'
                ,width      : 80
                ,align      : 'right'
            }
            ,{  header      : 'PROCESS'
                ,dataIndex  : 'PROCESS'
                ,width      : 100
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id          : 'src_ollprocess'
                                    ,xtype      : 'textfield'
                                    ,emptyText  : 'Search...'
                                    ,flex       : 1
                                    ,margin     : '0 10 10 10'
                                    ,listeners  : {
                                                    specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                            if (Ext.getCmp('src_olljobdate').getValue() == ''){
                                                                store_oll.proxy.setExtraParam('jobdate', today);
                                                            }
                                                            else{
                                                                store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                            }
                                                            store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                            store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                            store_oll.proxy.setExtraParam('jobstartserial', Ext.getCmp('src_ollstartserial').getValue());
                                                            store_oll.proxy.setExtraParam('process', field.getValue());
                                                            store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                            store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                            store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                            store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                            store_oll.proxy.setExtraParam('jobmcrh', Ext.getCmp('src_olljobmcrh').getValue());
                                                            store_oll.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
            }
            ,{  header      : 'LINE'
                ,dataIndex  : 'JOBLINE'
                ,width      : 90
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id          : 'src_olljobline'
                                    ,xtype      : 'textfield'
                                    ,emptyText  : 'Search...'
                                    ,flex       : 1
                                    ,margin     : '0 10 10 10'
                                    ,listeners  : {
                                                    specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                            if (Ext.getCmp('src_olljobdate').getValue() == ''){
                                                                store_oll.proxy.setExtraParam('jobdate', today);
                                                            }
                                                            else{
                                                                store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                            }
                                                            store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                            store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                            store_oll.proxy.setExtraParam('jobstartserial', Ext.getCmp('src_ollstartserial').getValue());
                                                            store_oll.proxy.setExtraParam('process', Ext.getCmp('src_ollprocess').getValue());
                                                            store_oll.proxy.setExtraParam('jobline', field.getValue());
                                                            store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                            store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                            store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                            store_oll.proxy.setExtraParam('jobmcrh', Ext.getCmp('src_olljobmcrh').getValue());
                                                            store_oll.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
            }
            ,{  header      : 'PWB NO'
                ,dataIndex  : 'JOBPWBNO'
                ,width      : 130
            }
            ,{  header      : 'PWB NAME'
                ,dataIndex  : 'JOBPWBNAME'
                ,width      : 130
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id          : 'src_olljobpwbname'
                                    ,xtype      : 'textfield'
                                    ,emptyText  : 'Search...'
                                    ,flex       : 1
                                    ,margin     : '0 10 10 10'
                                    ,listeners  : {
                                                    specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                            if (Ext.getCmp('src_olljobdate').getValue() == ''){
                                                                store_oll.proxy.setExtraParam('jobdate', today);
                                                            }
                                                            else{
                                                                store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                            }
                                                            store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                            store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                            store_oll.proxy.setExtraParam('jobstartserial', Ext.getCmp('src_ollstartserial').getValue());
                                                            store_oll.proxy.setExtraParam('process', Ext.getCmp('src_ollprocess').getValue());
                                                            store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                            store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                            store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                            store_oll.proxy.setExtraParam('jobpwbname', field.getValue());
                                                            store_oll.proxy.setExtraParam('jobmcrh', Ext.getCmp('src_olljobmcrh').getValue());
                                                            
                                                            store_oll.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
            }

            ,{  header      : 'LOT NO'
                ,dataIndex  : 'JOBLOTNO'
                ,width      : 90
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id          : 'src_olljoblotno'
                                    ,xtype      : 'textfield'
                                    ,emptyText  : 'Search...'
                                    ,flex       : 1
                                    ,margin     : '0 10 10 10'
                                    ,listeners  : {
                                                    specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                            if (Ext.getCmp('src_olljobdate').getValue() == ''){
                                                                store_oll.proxy.setExtraParam('jobdate', today);
                                                            }
                                                            else{
                                                                store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                            }
                                                            store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                            store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                            store_oll.proxy.setExtraParam('jobstartserial', Ext.getCmp('src_ollstartserial').getValue());
                                                            store_oll.proxy.setExtraParam('process', Ext.getCmp('src_ollprocess').getValue());
                                                            store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                            store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                            store_oll.proxy.setExtraParam('joblotno', field.getValue());
                                                            store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                            store_oll.proxy.setExtraParam('jobmcrh', Ext.getCmp('src_olljobmcrh').getValue());
                                                            store_oll.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
            }
            ,{  header      : 'FILE'
                ,dataIndex  : 'JOBFILE'
                ,width      : 250
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id          : 'src_olljobfile'
                                    ,xtype      : 'textfield'
                                    ,emptyText  : 'Search...'
                                    ,flex       : 1
                                    ,margin     : '0 10 10 10'
                                    ,listeners  : {
                                                    specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                            store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                            store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                            store_oll.proxy.setExtraParam('jobstartserial', Ext.getCmp('src_ollstartserial').getValue());
                                                            store_oll.proxy.setExtraParam('process', Ext.getCmp('src_ollprocess').getValue());
                                                            store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                            store_oll.proxy.setExtraParam('jobfile', field.getValue());
                                                            store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                            store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                            store_oll.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
            }
            ,{  header      : 'CAVITY'
                ,dataIndex  : 'CAVITY'
                ,width      : 100
                ,hidden     : true
            }
            ,{  header      : 'MCRH'
                ,dataIndex  : 'JOBMCRH'
                ,width      : 100
                ,hidden     : true
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id          : 'src_olljobmcrh'
                                    ,xtype      : 'textfield'
                                    ,emptyText  : 'Search...'
                                    ,flex       : 1
                                    ,margin     : '0 10 10 10'
                                    ,listeners  : {
                                                    specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                            if (Ext.getCmp('src_olljobdate').getValue() == ''){
                                                                store_oll.proxy.setExtraParam('jobdate', today);
                                                            }
                                                            else{
                                                                store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                            }
                                                            store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                            store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                            store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                            store_oll.proxy.setExtraParam('jobstartserial', Ext.getCmp('src_ollstartserial').getValue());
                                                            store_oll.proxy.setExtraParam('process', Ext.getCmp('src_ollprocess').getValue());
                                                            store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                            store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                            store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                            store_oll.proxy.setExtraParam('jobmcrh', field.getValue());
                                                            store_oll.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
            }
            ,{  header      : 'JOBNO'
                ,dataIndex  : 'JOBNO'
                ,align      : 'center'
                ,width      : 250
                ,layout     : { type    : 'hbox'
                                ,align  : 'stretch'
                                ,pack   : 'center'  }
                ,items      : [
                                {   id          : 'src_olljobno'
                                    ,xtype      : 'textfield'
                                    ,emptyText  : 'Search...'
                                    ,flex       : 1
                                    ,margin     : '0 10 10 10'
                                    ,listeners  : {
                                                    specialkey: function(field, e){
                                                        if (e.getKey() == e.ENTER){
                                                           
                                                            if (Ext.getCmp('src_olljobdate').getValue() == ''){
                                                                store_oll.proxy.setExtraParam('jobdate', today);
                                                            }
                                                            else{
                                                                store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                            }

                                                            store_oll.proxy.setExtraParam('jobno', field.getValue());
                                                            store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                            store_oll.proxy.setExtraParam('jobstartserial', Ext.getCmp('src_ollstartserial').getValue());
                                                            store_oll.proxy.setExtraParam('process', Ext.getCmp('src_ollprocess').getValue());
                                                            store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                            store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                            store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                            store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                            store_oll.proxy.setExtraParam('jobmcrh', Ext.getCmp('src_olljobmcrh').getValue());
                                                            store_oll.loadPage(1);
                                                        }
                                                    }
                                    }
                                }
                ]
            }
            ,{  header      : 'SEQ ID'
                ,dataIndex  : 'SEQID'
                ,width      : 100
                ,hidden     : true
            }
            ,{  header      : 'ISSUE STATUS'
                ,dataIndex  : 'JOBMCISSUE'
                ,width      : 100
                ,hidden     : true
            }
        ]
        // ,bbar       : { xtype       : 'pagingtoolbar'
        //                 ,displayInfo: true 
        //                 ,store      : store_oll
        // }
        // ,handler    : movingdataSql
        ,listeners  : {
            itemdblclick: function(dv,record,item,index,e){
                var srcModel    = Ext.getCmp('src_olljobmodelname').getValue();
                var srcSerial   = Ext.getCmp('src_ollstartserial').getValue();

                if (srcModel == "" || srcSerial == ""){
                    Ext.Msg.show({
                        title   : 'Warning',
                        icon    : Ext.Msg.ERROR,
                        msg     : "<h2 style='color:red'>Filter Model and serial !</h5>",
                        button  : Ext.Msg.OK  
                    });
                }
                else{
                    var rec = grid_oll.getSelectionModel().getSelection();
                    if (rec!=0){
                        var issueJobno  = rec[0].data.JOBNO;
                        var issueJobdate = rec[0].data.JOBDATE;
                        var issueJobtime = rec[0].data.JOBTIME;
                        var issueJobfile = rec[0].data.JOBFILE;
                        var issueJobmodelname = rec[0].data.JOBMODELNAME;
                        var issueJobpwbno = rec[0].data.JOBPWBNO;
                        var issueJobline = rec[0].data.JOBLINE;
                        var issueJoblotsize = rec[0].data.JOBLOTSIZE;
                        var issueJobstartserial = rec[0].data.JOBSTARTSERIAL;
                        var issueJoblotno = rec[0].data.JOBLOTNO;
                        var issueJobpwbname = rec[0].data.JOBPWBNAME;
                        var issueJobcavity = rec[0].data.CAVITY;
                        var issueJobmcrh = rec[0].data.JOBMCRH;

                        console.log(issueJobno);
                        console.log(issueJobdate);
                        console.log(issueJobtime);
                        console.log(issueJobfile);
                        console.log(issueJobmodelname);
                        console.log(issueJobpwbno);
                        console.log(issueJobline);
                        console.log(issueJoblotsize);
                        console.log(issueJobstartserial);
                        console.log(issueJoblotno);
                        console.log(issueJobpwbname);
                        console.log(issueJobcavity);
                        console.log(issueJobmcrh);
                        // if (win.isVisible()) { win.hide(); } else { win.show();}

                        var win_movingSql;
                        if(!win_movingSql){
                            var form_movingSql = Ext.create('Ext.form.Panel', {
                                layout			: {
                                    type	: 'vbox',
                                    align	: 'stretch'
                                },
                                border			: false,
                                bodyPadding		: 10,

                                fieldDefaults	: {
                                    labelWidth	: 150,
                                    labelStyle	: 'font-weight:bold',
                                    msgTarget	: 'side'
                                },
                                defaults		: {
                                    margins	: '0 0 10 0'
                                },
                                items			: [
                                    {
                                        xtype	: 'label',
                                        html	: '<font style="font-size:20px;color:blue;">Are you sure to issuing this OLL '+ issueJobmodelname +' ?<br><br>Scan NIK for Starting this Issue !</font>'
                                    }
                                    ,{
                                        xtype   : 'tbspacer',
                                        height  : 50
                                    }
                                    ,{  id      : 'movingJobno'
                                        ,name   : 'movingJobno'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJobno
                                    }
                                    ,{  id      : 'movingJobdate'
                                        ,name   : 'movingJobdate'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJobdate
                                    }
                                    ,{  id      : 'movingJobtime'
                                        ,name   : 'movingJobtime'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJobtime
                                    }
                                    ,{  id      : 'movingJobfile'
                                        ,name   : 'movingJobfile'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJobfile
                                    }
                                    ,{  id      : 'movingJobmodelname'
                                        ,name   : 'movingJobmodelname'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJobmodelname
                                    }
                                    ,{  id      : 'movingJobpwbno'
                                        ,name   : 'movingJobpwbno'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJobpwbno
                                    }
                                    ,{  id      : 'movingJobline'
                                        ,name   : 'movingJobline'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJobline
                                    }
                                    ,{  id      : 'movingJoblotsize'
                                        ,name   : 'movingJoblotsize'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJoblotsize
                                    }
                                    ,{  id      : 'movingJobstartserial'
                                        ,name   : 'movingJobstartserial'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJobstartserial
                                    }
                                    ,{  id      : 'movingJoblotno'
                                        ,name   : 'movingJoblotno'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJoblotno
                                    }
                                    ,{  id      : 'movingJobpwbname'
                                        ,name   : 'movingJobpwbname'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJobpwbname
                                    }
                                    ,{  id      : 'movingJobcavity'
                                        ,name   : 'movingJobcavity'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJobcavity
                                    }
                                    ,{  id      : 'movingJobmcrh'
                                        ,name   : 'movingJobmcrh'
                                        ,xtype	: 'hiddenfield'
                                        ,value  : issueJobmcrh
                                    }
                                    // ,{
                                    //     xtype   : 'hiddenfield'
                                    //     ,id     : 'movingAction'
                                    //     ,name   : 'movingAction'
                                    // }
                                    ,{  emptyText   : 'SCAN NIK',
                                        xtype       : 'textfield',
                                        id          : 'movingNik',
                                        name        : 'movingNik',
                                        minLength   : 5,
                                        maxLength   : 8,
                                        allowBlank  : false,
                                        selectOnFocus: true,
                                        // layout: 'anchor',
                                        // defaults: {
                                        //     anchor: '100%',
                                        //     padding: '5 5 5 5',
                                        //     fieldStyle: 'font-size:20px;text-align:center;'
                                        // },
                                        height      : 70
                                        ,fieldStyle : 'font-size:30px; text-align: center;'
                                        ,listeners: {
                                            afterrender: function(field) { field.focus(true); },
                                            specialkey: function(field, e) {
                                                if (e.getKey() == e.ENTER) {
                                                    // Ext.getCmp('movingAction').setValue("checkNik");

                                                    var form = this.up('form').getForm();
                                                    var popwindow = this.up('window');
                                                    var txtval = field.getValue();
                                                    var len = txtval.length;
                                                    if (len != 0) {
                                                        if(form.isValid()) {
                                                            form.submit({
                                                                url: 'response/mc_startIssue.php',
                                                                waitMsg : 'Checking data, please wait..',
                                                                success : function(form, action) {
                                                                    // Ext.getCmp('movingAction').setValue("startIssue");
                                                                    // Ext.getCmp('startIssue').setHidden(false);
                                                                    // field.setEditable(false);

                                                                    popwindow.close();
                                                                    if (win_mcissue.isVisible()) { 
                                                                        win_mcissue.hide(); 
                                                                        win_mcissue.show(); 
                                                                        store_mcissueHeader.proxy.setExtraParam('jobno', issueJobno);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobdate', issueJobdate);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobtime', issueJobtime);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobmodelname', issueJobmodelname);
                                                                        store_mcissueHeader.loadPage(1);
                                                                        store_mcissueDetail.proxy.setExtraParam('jobno', issueJobno);
                                                                        store_mcissueDetail.proxy.setExtraParam('jobdate', issueJobdate);
                                                                        store_mcissueDetail.proxy.setExtraParam('jobtime', issueJobtime);
                                                                        store_mcissueDetail.proxy.setExtraParam('jobmodelname', issueJobmodelname);
                                                                        store_mcissueDetail.loadPage(1);
                                                                        Ext.getCmp('issDetailJobno').setValue(issueJobno);
                                                                        Ext.getCmp('issDetailJobdate').setValue(issueJobdate);
                                                                        Ext.getCmp('issDetailJobtime').setValue(issueJobtime);
                                                                        Ext.getCmp('issDetailModelName').setValue(issueJobmodelname);
                                                                        Ext.getCmp('scanIssueNik').setValue(txtval);
                                                                        Ext.getCmp('scanIssueNik').setEditable(false);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobno', issueJobno);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobdate', issueJobdate);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobtime', issueJobtime);
                                                                    } 
                                                                    else { 
                                                                        win_mcissue.show();
                                                                        store_mcissueHeader.proxy.setExtraParam('jobno', issueJobno);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobdate', issueJobdate);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobtime', issueJobtime);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobmodelname', issueJobmodelname);
                                                                        store_mcissueHeader.loadPage(1);
                                                                        store_mcissueDetail.proxy.setExtraParam('jobno', issueJobno);
                                                                        store_mcissueDetail.proxy.setExtraParam('jobdate', issueJobdate);
                                                                        store_mcissueDetail.proxy.setExtraParam('jobtime', issueJobtime);
                                                                        store_mcissueDetail.proxy.setExtraParam('jobmodelname', issueJobmodelname);
                                                                        store_mcissueDetail.loadPage(1);
                                                                        Ext.getCmp('issDetailJobno').setValue(issueJobno);
                                                                        Ext.getCmp('issDetailJobdate').setValue(issueJobdate);
                                                                        Ext.getCmp('issDetailJobtime').setValue(issueJobtime);
                                                                        Ext.getCmp('issDetailModelName').setValue(issueJobmodelname);
                                                                        Ext.getCmp('scanIssueNik').setValue(txtval);
                                                                        Ext.getCmp('scanIssueNik').setEditable(false);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobno', issueJobno);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobdate', issueJobdate);
                                                                        store_mcissueHeader.proxy.setExtraParam('jobtime', issueJobtime);
                                                                    }
                                                                },
                                                                failure : function(form, action) {
                                                                    Ext.Msg.show({
                                                                        title   : 'OOPS, AN ERROR JUST HAPPEN !',
                                                                        icons   : Ext.Msg.ERROR,
                                                                        msg     : action.result.msg,
                                                                        buttons : Ext.Msg.OK
                                                                    });
                                                                    field.setValue("");
                                                                    // Ext.getCmp('startIssue').setHidden(true);
                                                                    field.setEditable(true);
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                ],
                                // buttons			: [
                                //     {   id          : 'startIssue',
                                //         text	    : 'Start Issue',
                                //         scale       : 'large',
                                //         hidden      : true,
                                //         // iconCls	: 'icon-start',
                                //         handler	: function() {
                                //             var form = this.up('form').getForm();
                                //             var popwindow = this.up('window');
                                //             if (form.isValid()) {
                                //                 form.submit({
                                //                     url		: 'response/mc_startIssue.php',
                                //                     waitMsg	: 'Starting Issue...',

                                //                     success	: function(form, action) {
                                //                         popwindow.close();
                                //                         if (win_mcissue.isVisible()) { win_mcissue.hide(); } 
                                //                         else { 
                                //                             win_mcissue.show();
                                //                             store_mcissueHeader.proxy.setExtraParam('jobno', issueJobno);
                                //                             store_mcissueHeader.proxy.setExtraParam('jobdate', issueJobdate);
                                //                             store_mcissueHeader.proxy.setExtraParam('jobtime', issueJobtime);
                                //                             store_mcissueHeader.proxy.setExtraParam('jobmodelname', issueJobmodelname);
                                //                             store_mcissueHeader.loadPage(1);
                                //                             store_mcissueDetail.proxy.setExtraParam('jobno', issueJobno);
                                //                             store_mcissueDetail.proxy.setExtraParam('jobdate', issueJobdate);
                                //                             store_mcissueDetail.proxy.setExtraParam('jobtime', issueJobtime);
                                //                             store_mcissueDetail.proxy.setExtraParam('jobmodelname', issueJobmodelname);
                                //                             store_mcissueDetail.loadPage(1);
                                //                             Ext.getCmp('issDetailJobno').setValue(issueJobno);
                                //                             Ext.getCmp('issDetailJobdate').setValue(issueJobdate);
                                //                             Ext.getCmp('issDetailJobtime').setValue(issueJobtime);
                                //                             Ext.getCmp('issDetailModelName').setValue(issueJobmodelname);
                                //                             store_mcissueHeader.proxy.setExtraParam('jobno', issueJobno);
                                //                             store_mcissueHeader.proxy.setExtraParam('jobdate', issueJobdate);
                                //                             store_mcissueHeader.proxy.setExtraParam('jobtime', issueJobtime);
                                //                         }
                                                        
                                //                     },

                                //                     failure	: function(form, action) {
                                //                         Ext.Msg.show({
                                //                             title		:'Failure - Starting Issue',
                                //                             icon		: Ext.Msg.ERROR,
                                //                             msg			: action.result.msg,
                                //                             buttons		: Ext.Msg.OK
                                //                         });
                                //                     }
                                //                 });
                                //             }
                                //         }
                                //     }
                                // ]
                            });

                            win_movingSql = Ext.widget('window', {
                                title		: 'Starting Issue',
                                width		: 500,
                                heigth		: 300,
                                modal		: true,
                                constrain	: true,
                                layout		: 'fit',
                                bodyStyle   : { background :'rgba(32,154,165,1); padding-bottom: 5px;'},
                                items		: form_movingSql
                            });

                            win_movingSql.show();
                        }
                        
                    }
                    else{
                        Ext.Msg.show({
                            title   : 'Warning',
                            icon    : Ext.Msg.ERROR,
                            msg     : "No data selected.",
                            button  : Ext.Msg.OK  
                        });
                    }
                }
            }
        }
    });
    var form_ollsearch = Ext.create('Ext.form.Panel',{
		name 		: 'form_ollsearch',
		// layout 		: {type: 'hbox', pack:'center', align:'stretch'},
		// bodyStyle 	: { background 	: 'rgba(255, 255, 255, 0)' },
        defaultType : 'textfield',
	    // dockedItems : [toolbar_expData],
	    items 		: [ 
                        {   xtype       : 'button'
                            ,id         : 'bt_reset'
                            ,scale      : 'medium'
                            ,text       : 'RESET'
							,icon		: 'resources/reset.png'
                            ,handler    : function(){
                                Ext.getCmp('src_olljobno').reset();
                                Ext.getCmp('src_olljobfile').reset();
                                Ext.getCmp('src_olljobdate').reset();
                                Ext.getCmp('src_olljobmodelname').reset();
                                Ext.getCmp('src_ollstartserial').reset();
                                Ext.getCmp('src_olljobline').reset();
                                Ext.getCmp('src_olljoblotno').reset();
                                Ext.getCmp('src_olljobpwbname').reset();
                                
                                var srcmcrh = Ext.getCmp('src_olljobmcrh');
                                if (typeof srcmcrh !== 'undefined'){
                                    Ext.getCmp('src_olljobmcrh').reset();
                                    store_oll.proxy.setExtraParam('jobmcrh', '');
                                }
                                if (Ext.getCmp('src_olljobdate').getValue() == ''){
                                    store_oll.proxy.setExtraParam('jobdate', today);
                                }
                                else{
                                    store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                }
                                // Ext.getCmp('mc_ollStartDate').reset();
                                // Ext.getCmp('mc_ollEndDate').reset();

                                // store_oll.proxy.setExtraParam('startdate', '');
                                // store_oll.proxy.setExtraParam('enddate', '');
                                store_oll.proxy.setExtraParam('jobno', '');
                                store_oll.proxy.setExtraParam('jobdate', '');
                                store_oll.proxy.setExtraParam('jobmodelname', '');
                                store_oll.proxy.setExtraParam('jobstartserial', '');
                                store_oll.proxy.setExtraParam('jobfile','');
                                store_oll.proxy.setExtraParam('jobline', '');
                                store_oll.proxy.setExtraParam('joblotno', '');
                                store_oll.proxy.setExtraParam('jobpwbname', '');
                                store_oll.loadPage(1);
                            }
                        }
                        /*,{  xtype   : 'tbspacer'
                            ,width  : 20
                            ,hidden : true
                        }
						,{	xtype 				: 'datefield'
					        ,name 				: 'mc_ollStartDate'
					        ,id 				: 'mc_ollStartDate'
                            ,format 			: 'Y-m-d'
                            ,fieldStyle         : 'font-size: 20px;'
                            ,emptyText          : 'Start Date'          
                            ,height             : 60
                            ,width              : 300
                            // ,hidden : true
                            ,listeners 			: 	{ 	specialkey:function(field, e) {
															if (e.getKey() == e.ENTER) {
																console.log('USING STARTDATE');
                                                                console.log(field.getValue());
                                                                if (Ext.getCmp('src_olljobdate').getValue() == ''){
                                                                    store_oll.proxy.setExtraParam('jobdate', today);
                                                                }
                                                                else{
                                                                    store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                                }
                                                                store_oll.proxy.setExtraParam('startdate', field.getValue());
                                                                store_oll.proxy.setExtraParam('enddate', Ext.getCmp('mc_ollEndDate').getValue());
                                                                store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                                store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                                store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                                store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                                store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                                store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                                store_oll.proxy.setExtraParam('jobmcrh', Ext.getCmp('src_olljobmcrh').getValue());
                                                                store_oll.loadPage(1);
															}
										                }
				           							}                            
                        }
                        ,{  xtype : 'tbspacer'
                            ,width : 10
                            ,hidden : true
                        }
                        ,{	xtype 				: 'datefield'
					        ,name 				: 'mc_ollEndDate'
					        ,id 				: 'mc_ollEndDate'
					        ,format 			: 'Y-m-d'
                            ,fieldStyle         : 'font-size: 20px;'
                            ,emptyText          : 'End Date'     
                            ,height             : 60
                            ,width              : 300
                            // ,hidden : true
                            ,listeners 			: 	{ 	specialkey:function(field, e) {
															if (e.getKey() == e.ENTER) {
																console.log('USING ENDDATE');
                                                                console.log(field.getValue());
                                                                
                                                                if (Ext.getCmp('src_olljobdate').getValue() == ''){
                                                                    store_oll.proxy.setExtraParam('jobdate', today);
                                                                }
                                                                else{
                                                                    store_oll.proxy.setExtraParam('jobdate', Ext.getCmp('src_olljobdate').getValue());
                                                                }
                                                                
                                                                store_oll.proxy.setExtraParam('startdate', Ext.getCmp('mc_ollStartDate').getValue());
                                                                store_oll.proxy.setExtraParam('enddate', field.getValue());
                                                                store_oll.proxy.setExtraParam('jobno', Ext.getCmp('src_olljobno').getValue());
                                                                store_oll.proxy.setExtraParam('jobfile', Ext.getCmp('src_olljobfile').getValue());
                                                                store_oll.proxy.setExtraParam('jobmodelname', Ext.getCmp('src_olljobmodelname').getValue());
                                                                store_oll.proxy.setExtraParam('jobline', Ext.getCmp('src_olljobline').getValue());
                                                                store_oll.proxy.setExtraParam('joblotno', Ext.getCmp('src_olljoblotno').getValue());
                                                                store_oll.proxy.setExtraParam('jobpwbname', Ext.getCmp('src_olljobpwbname').getValue());
                                                                store_oll.proxy.setExtraParam('jobmcrh', Ext.getCmp('src_olljobmcrh').getValue());
                                                                store_oll.loadPage(1);
															}
										                }
				           							}
					    }
                        ,{  xtype   : 'tbspacer'
                            ,width  : 20
                            ,hidden : true
                        }
                        ,{   xtype               : 'button'
                            ,id                 : 'bt_startIssue'
                            ,scale              : 'medium'
                            ,text               : 'START ISSUE'
                            // ,hidden : true
                        }*/
					]
	});
    var panel_issueOll = Ext.create('Ext.panel.Panel',{
        border  : true
        ,layout : 'border'
        ,default: { split: false
                    ,plain: true }
        ,items  : [
            {   region      : 'north'
                ,bodyPadding: 10
                ,layout     : { type: 'hbox'
                                ,pack: 'center' }
                ,bodyStyle  : { background  : 'url("resources/bg-image.jpg") repeat center'
                                ,backgroudSize : 'cover'
                            }
                ,items      :  form_ollsearch      
            }
            ,{  region      : 'center'
                ,layout     : 'fit'
                ,items      : grid_oll
            }
        ]
    });
</script>