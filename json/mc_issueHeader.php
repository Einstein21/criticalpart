<?php
include '../connection.php';
date_default_timezone_set('Asia/Jakarta');
ini_set('max_execution_time',50);
$today = date("Y-m-d");

$allowedFilter = [
    'jobno', 'jobfile', 'jobmodelname', 'jobline', 'joblotno', 'jobpwbname','jobmcrh'
];

$resultQuery = [];
foreach ($allowedFilter as $value) {    
    if (@$_REQUEST[$value] != ""){
        $resultQuery[$value] = $_REQUEST[$value];
    }
}

$jobdate = isset($_REQUEST["jobdate"]) ? $_REQUEST["jobdate"] : "";
if ($jobdate == ""){
    $resultQuery["jobdate"] = $today;
}
else{
    $resultQuery["jobdate"] = substr($_REQUEST["jobdate"],0,10);
}

function selectQuery($where = []){
    $whereStatement = "";
    $i = 0;
    if (count($where) != 0){
        foreach ($where as $key => $value) {
            if ($key == "jobdate"){
                $opt = "= '{$value}'";
            }
            else{
                $opt = "LIKE '%{$value}%'";
            }

            if($i == 0){
                $whereStatement = " Where $key $opt";
            }
            else{
                $whereStatement .= " And $key $opt";
            }
            $i++;
        }
    }
    return $whereStatement;
}
$query      = "SELECT   a.JOBNO
                        ,a.JOBDATE
                        ,CONVERT(VARCHAR(8),a.JOBTIME,8) AS JOBTIME
                        ,a.JOBFILE
                        ,a.JOBMODELNAME
                        ,a.JOBPWBNO
                        ,a.JOBLINE
                        ,a.JOBLOTSIZE
                        ,a.JOBSTARTSERIAL
                        ,a.JOBLOTNO
                        ,a.JOBPWBNAME
                        ,a.CAVITY
                        ,a.JOBMCRH
                        ,a.PROCESS
                        ,a.SEQID
                        ,a.ISSUESTATUS
                        ,a.REMARK
                        ,(select top 1 b.start_by from MC_startIssue b where b.jobno = a.jobno order by b.id desc) as CREATED_BY
                        ,(select top 1 c.start_name from MC_startIssue c where c.jobno = a.jobno order by c.id desc) as CREATED_NAME
                        ,(select top 1 d.start_by from MC_startIssue d where d.jobno = a.jobno order by d.id desc) as CREATED_AT
                        ,a.UPDATED_BY
                        ,a.UPDATED_NAME
                        ,a.UPDATED_AT
                FROM    [CRITICALPART].[dbo].[MC_OLL] a";

$order  = " ORDER BY JOBTIME DESC";

try {
    //code...
    $sql    = $query . selectQuery($resultQuery) . $order;
    $rs1    = $conn->Execute($sql);

} catch (exception $e) {
    //throw $th;
    $var_msg    = $conn->ErrorNo();
    $error      = $conn->ErrorMsg();
    $error_msg  = str_replace(chr(50), "", $error);
    
    echo json_encode([
        "success" => false,
        "msg" => $error_msg
        ]);
    
    $rs1->Close();
    $conn->Close();
    $conn=NULL;
    return;
}

$result     = [];
for ($i=0; !$rs1->EOF; $i++) { 
    $result[] = $rs1->GetRowAssoc();
    $rs1->MoveNext();
}

echo json_encode([
    "success"       => true
    ,"connection"   => $conn->isConnected()
    ,"param"        => $resultQuery
    ,"query"        => $sql
    ,"rows"         => $result
]);

$rs1->Close();
$conn->Close();
$conn=NULL;
?>
