<?php
include '../connection.php';
date_default_timezone_set('Asia/Jakarta');
$today = date("Y-m-d");

$allowedFilter = [
    'jobno', 'jobdate', 'jobtime', 'jobmodelname'
];

$resultQuery = [];
foreach ($allowedFilter as $value) {    
    if (@$_REQUEST[$value] != ""){
        $resultQuery[$value] = $_REQUEST[$value];
    }
}
// print_r($resultQuery[]);

$query = "Exec MC_issueHeader '{$resultQuery['jobno']}','{$resultQuery['jobdate']}','{$resultQuery['jobtime']}','{$resultQuery['jobmodelname']}'";
$rsHeader = $conn->Execute($query);

$resultHeader = [];
for ($i=0; !$rsHeader->EOF; $i++) { 
    $resultHeader[] = $rsHeader->GetRowAssoc();
    $rsHeader->MoveNext();
}

echo json_encode([
    "success"       => true
    ,"connection"   => $conn->isConnected()
    ,"param"        => $resultQuery
    ,"query"        => $query
    ,"rows"         => $resultHeader
]);

$rsHeader->Close();
$conn->Close();
$conn=NULL;
?>
