<?php
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
	session_start();
	include '../connection.php';
    
    function check($key){
        $result = isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
        return $result;
    }

    $page       = check('page');
    $limit      = check('limit');
    $supplier   = check('supplier');
    $partno     = check('partno');
    $po         = check('po');
    $invoice    = check('invoice');
    $stsinsp    = check('stsinsp');
    $location   = check('location');
    $start      = (($page*$limit)-$limit);
    
    // $ADODB_FETCH_MODE = ADODB_FETCH_BOTH;  
    $sql = " declare @totalcount as int; 
             exec MC_dispExpData '{$start}','{$limit}','{$supplier}','{$partno}','{$po}','{$invoice}','{$stsinsp}','{$location}',@totalcount=@totalcount";
    $rs  = $conn->Execute($sql);

    $result = [];
    for($i=0;!$rs->EOF;$i++) {
        $result[] = $rs->GetRowAssoc();
        $rs->MoveNext();
    }
    
    echo json_encode([
        'success' => true,
        'query' => $sql,
        'TOTCOUNT' => $result['0']['TOTCOUNT'],
        'rows' => $result
    ]);
    
    $rs->Close();
    $conn->Close();
