<?php
include '../connection.php';
date_default_timezone_set('Asia/Jakarta');
$today = date("Y-m-d");

$allowedFilter = [
    'jobno', 'jobdate', 'jobtime', 'jobmodelname'
];
// $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "";

$resultQuery = [];
foreach ($allowedFilter as $value) {    
    if (@$_REQUEST[$value] != ""){
        $resultQuery[$value] = $_REQUEST[$value];
    }
}
// print_r($resultQuery[]);

    $query = "Exec MC_issueDetail '{$resultQuery['jobno']}','{$resultQuery['jobdate']}','{$resultQuery['jobtime']}','{$resultQuery['jobmodelname']}'";
    $rsDetail = $conn->Execute($query);
    
    $resultDetail = [];
    for ($i=0; !$rsDetail->EOF; $i++) { 
        $resultDetail[] = $rsDetail->GetRowAssoc();
        $rsDetail->MoveNext();
    }

    echo json_encode([
        "success"       => true
        ,"connection"   => $conn->isConnected()
        ,"param"        => $resultQuery
        ,"query"        => $query
        ,"rows"         => $resultDetail
    ]);

$rsDetail->Close();
$conn->Close();
$conn=NULL;
?>
