<?php
include '../connection.php';
date_default_timezone_set('Asia/Jakarta');
ini_set('max_execution_time',50);
$today = date("Y-m-d");

$allowedFilter = [
    'jobno', 'jobfile', 'jobmodelname', 'jobline', 'joblotno', 'jobpwbname','jobmcrh','jobstartserial','process'
];

$resultQuery = [];
foreach ($allowedFilter as $value) {    
    # code...
    // $resultQuery["jobdetail.zfeeder"] = "TRAY";
    if($value == "process" && @$_REQUEST["process"] != "" ){
        $resultQuery["jobmodel." . $value] = $_REQUEST[$value];
    }
    elseif (@$_REQUEST[$value] != ""){
        $resultQuery["jobheaderinfo." . $value] = $_REQUEST[$value];
    }
}

$jobdate = isset($_REQUEST["jobdate"]) ? $_REQUEST["jobdate"] : "";
if ($jobdate == ""){
    $resultQuery["jobheaderinfo.jobdate"] = $today;
}
else{
    $resultQuery["jobheaderinfo.jobdate"] = substr($_REQUEST["jobdate"],0,10);
}

function selectQuery($where = []){
    $whereStatement = "";
    $i = 0;
    if (count($where) != 0){
        foreach ($where as $key => $value) {
            
            if ($key == "jobdetail.demand"){
                $opt = ">";
            }
            else if ($key == "jobheaderinfo.jobdate"){
                $opt = "=";
            }
            else if ($key == "jobheaderinfo.jobstartserial"){
                $opt = "=";
            }
            else{
                $opt = "CONTAINING";
            }

            if($i == 0){
                $whereStatement = " Where $key $opt '{$value}'";
            }
            else{
                $whereStatement .= " And $key $opt '{$value}'";
            }

            $i++;
        }
    }
    return $whereStatement;
}

$query      = "SELECT DISTINCT
                TRIM(jobheaderinfo.jobno) as jobno,
                jobheaderinfo.jobdate,
                LEFT(jobheaderinfo.jobtime,8) AS JOBTIME,
                TRIM(jobheaderinfo.jobfile) as jobfile,
                TRIM(jobheaderinfo.jobmodelname) as jobmodelname,
                TRIM(jobheaderinfo.jobpwbno) as jobpwbno,
                TRIM(jobheaderinfo.jobline) as jobline,
                jobheaderinfo.joblotsize,
                jobheaderinfo.jobstartserial,
                TRIM(jobheaderinfo.joblotno) as joblotno,
                TRIM(jobmodel.pwb_name) as jobpwbname,
                jobmodel.cavity,
                TRIM(jobheaderinfo.jobmcrh) as jobmcrh,
                jobmodel.process,
                jobmodel.seqid,
                jobheaderinfo.jobmcissue
        FROM    jobheaderinfo
        LEFT JOIN JOBMODEL on jobmodel.jobno = jobheaderinfo.jobno
        AND JOBMODEL.filename = JOBHEADERINFO.jobfile ";
        // LEFT JOIN jobdetail on jobdetail.jobno = jobheaderinfo.jobno";

$order      = " ORDER BY jobheaderinfo.jobtime desc";
$firebird   = $query . selectQuery($resultQuery) . $order;
$rs1        = $db_outset->Execute($firebird);

$result     = [];
for ($i=0; !$rs1->EOF; $i++) { 
    $result[] = $rs1->GetRowAssoc();
    $rs1->MoveNext();
}

echo json_encode([
    "success"       => true
    ,"connection"   => $db_outset->isConnected()
    ,"param"        => $resultQuery
    ,"query"        => $firebird
    ,"rows"         => $result
]);

$rs1->Close();
$db_outset->Close();
$db_outset=NULL;
?>
