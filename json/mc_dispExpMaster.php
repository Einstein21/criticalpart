<?php
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
	session_start();
	include '../connection.php';
    
    $page       = @$_REQUEST["page"];
    $limit      = @$_REQUEST["limit"];
    $start      = (($page*$limit)-$limit);
        
    $supplier  = isset($_REQUEST["supplier"]) ? $_REQUEST["supplier"] : '';
    $duration  = isset($_REQUEST["duration"]) ? $_REQUEST["duration"] : '';
    
    // $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
    $sql = " declare @totalcount as int; 
             exec MC_dispExpMaster '{$start}','{$limit}','{$supplier}','{$duration}',@totalcount=@totalcount";
    $rs  = $conn->Execute($sql);
    
    $result = [];
    for($i=0;!$rs->EOF;$i++) {
        $result[] = $rs->GetRowAssoc();
        $rs->MoveNext();
    }
    
    echo json_encode([
        'success' => true,
        'rows' => $result 
        // 'rows' => $rs->getArray()
    ]);
    return;
    
    /*echo json_encode([
        'success' => true,
        'totalCount' => $rs->fields['totcount'],
        'rows' => $rs->getArray()
    ]);
    return;*/

    $rs->Close();
    $conn->Close();
?>
