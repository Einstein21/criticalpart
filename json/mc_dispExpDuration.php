<?php
	error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
	session_start();
	include '../connection.php';
    
    function check($key){
        $rcheck = isset($_REQUEST[$key]) ? $_REQUEST[$key] : '';
        return $rcheck;
    }

    $page       = check('page');
    $limit      = check('limit');
    $duration   = check('duration');
    $remark     = check('remark');
    $start      = (($page*$limit)-$limit);

    try {
        $sql = "declare @totalcount as int; 
                exec MC_dispExpDuration '{$start}','{$limit}','{$duration}','{$remark}',@totalcount=@totalcount";
        $rs  = $conn->Execute($sql);
        
        $result = [];
        for($i=0;!$rs->EOF;$i++) {
            $result[] = $rs->GetRowAssoc();
            $rs->MoveNext();
        }
        
        echo json_encode([
            'success' => true,
            'rows' => $result
            ]);

    } catch(exception $e) {
        $var_msg    = $conn->ErrorNo();
        $error      = $conn->ErrorMsg();
        $error_msg  = str_replace(chr(50), "", $error);
        
        echo json_encode([
            "success" => false,
            "msg" => $error_msg
            ]);
            return;
    }
    
    $rs->Close();
    $conn->Close();
?>
