<?php
include '../connection.php';
date_default_timezone_set('Asia/Jakarta');
ini_set('max_execution_time',50);
$today = date("Y-m-d");

$allowedFilter = [ 'jobno' ];

$resultQuery = [];
foreach ($allowedFilter as $value) {    
    if (@$_REQUEST[$value] != ""){
        $resultQuery[$value] = $_REQUEST[$value];
    }
}

function selectQuery($where = []){
    $whereStatement = "";
    $i = 0;
    if (count($where) != 0){
        foreach ($where as $key => $value) {
            $opt = "= '{$value}'";
            
            if($i == 0){
                $whereStatement = " Where $key $opt";
            }
            else{
                $whereStatement .= " And $key $opt";
            }
            $i++;
        }
    }
    return $whereStatement;
}

$query = "SELECT a.JOBNO
                ,a.PARTLABEL
                ,a.PARTNO
                ,a.QTY AS QTYSCAN
                ,a.SCAN_BY
                ,a.SCAN_NAME
                ,CONVERT(VARCHAR(20),a.SCAN_DATE,120) as SCAN_DATE
                ,b.SUPPCODE
                ,b.SUPPNAME
                ,b.PO
                ,b.QTY AS QTYSTOCK 
                ,b.INVOICE
                ,CONVERT(VARCHAR(10),b.PRODDATE,120) as PRODDATE
                ,CONVERT(VARCHAR(10),b.EXPDATE,120) as EXPDATE
                ,b.LOCATION
                ,b.STSINSP
            FROM [CRITICALPART].[dbo].[MC_scanIssue] a
            LEFT JOIN [CRITICALPART].[dbo].[MC_expParts] b on b.PARTLABEL = a.PARTLABEL";

$order  = " ORDER BY a.PARTNO DESC";

try {
    //code...
    $sql    = $query . selectQuery($resultQuery) . $order;
    $rs1    = $conn->Execute($sql);

} catch (exception $e) {
    //throw $th;
    $var_msg    = $conn->ErrorNo();
    $error      = $conn->ErrorMsg();
    $error_msg  = str_replace(chr(50), "", $error);
    
    echo json_encode([
        "success" => false,
        "msg" => $error_msg
        ]);
    
    $rs1->Close();
    $conn->Close();
    $conn=NULL;
    return;
}

$result     = [];
for ($i=0; !$rs1->EOF; $i++) { 
    $result[] = $rs1->GetRowAssoc();
    $rs1->MoveNext();
}

echo json_encode([
    "success"       => true
    ,"connection"   => $conn->isConnected()
    ,"param"        => $resultQuery
    ,"query"        => $sql
    ,"rows"         => $result
]);

$rs1->Close();
$conn->Close();
$conn=NULL;
?>
