<?php
include '../connection.php';
date_default_timezone_set("Asia/jakarta");

function check($key){
    $request = isset($_REQUEST[$key]) ? $_REQUEST[$key] : '';
    return $request;
}

$raw_nik    = check('scanNik');
$scanPart   = check('scanPart');
$jobno      = check('scanJobno');
$jobdate    = check('scanJobdate');
$jobtime    = check('scanJobtime');
$idIssue    = check('issID');

$len    = strlen($raw_nik);
if($len == 5){ $nik = $raw_nik; } else{ $nik = substr($raw_nik, 2,5); }

try{
    $query  =  "SELECT  [EMP_NAME]
                FROM    [payroll].[sapayroll].[HCE_access]
                where   lastday is null
                and     emp_no = '{$nik}'";
    $rs         = $db_payroll->Execute($query);
    $empname    = trim($rs->fields['0']);
    $rs->Close();

    if (!$empname){
        echo "0|<font style='font-size:25px;color:red;'>NIK NOT FOUND !</font>";
    }
    else{
        try {
            //code...
            $query2 = "EXEC MC_scanPart '{$nik}','{$empname}','{$jobno}','{$jobdate}','{$jobtime}','{$scanPart}','{$idIssue}'";
            $rs2 = $conn->Execute($query2);
            $warning = $rs2->fields[0];
            $stsIssue = substr($warning,0,1);
            $rs2->Close();

            if ($stsIssue == 2){
                try {
                    //code...
                    $firebird   = "UPDATE jobheaderinfo SET JOBMCISSUE = 2 WHERE JOBDATE = '{$jobdate}' and JOBNO = '{$jobno}'";
                    $rs3        = $db_outset->Execute($firebird);
                    echo '3|DONE';
                } catch (exception $e) {
                    //throw $th;
                    $var_msg    = $db_outset->ErrorNo();
                    $error      = $db_outset->ErrorMsg();
                    $error_msg  = str_replace(chr(50), "", $error);
                    
                    echo "0|{$error_msg}";
                }
            }
            else{
                echo $warning;
            }

        } catch (exception $e) {
            //throw $th;
            $var_msg    = $conn->ErrorNo();
            $error      = $conn->ErrorMsg();
            $error_msg  = str_replace(chr(50), "", $error);
            
            echo "0|{$error_msg}";
       }
    }
}
catch(exception $e) {
    $var_msg    = $db_payroll->ErrorNo();
    $error      = $db_payroll->ErrorMsg();
    $error_msg  = str_replace(chr(50), "", $error);
    
    echo "0|{$error_msg}";
}

$db_payroll->Close();
$db_payroll = NULL;
$conn->Close();
$conn = NULL;
?>