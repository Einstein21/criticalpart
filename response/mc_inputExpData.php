<?php
// session_start();
include '../connection.php';

$raw_nik          = isset($_REQUEST['mc_expDataNik']) ? $_REQUEST['mc_expDataNik'] : " ";
$expDataLabel     = isset($_REQUEST['mc_expDataLabel']) ? $_REQUEST['mc_expDataLabel'] : " ";
$expDataInvoice   = isset($_REQUEST['mc_expDataInvoice']) ? $_REQUEST['mc_expDataInvoice'] : " ";
$expDataProdDate  = isset($_REQUEST['mc_expDataProdDate']) ? $_REQUEST['mc_expDataProdDate'] : " ";
$expDataRemark    = isset($_REQUEST['mc_expDataRemark']) ? $_REQUEST['mc_expDataRemark'] : " ";

if (strlen($raw_nik) == 5) {
    $nik = $raw_nik;
} else {
    $nik = substr($raw_nik,2,5);
}

try {
    $query  =  "SELECT [EMP_NAME]
                FROM [payroll].[sapayroll].[HCE_access]
                where lastday is null
                and emp_no = '{$nik}'";
    $rs     = $db_payroll->Execute($query);
    $empname= trim($rs->fields['0']);
    $rs->Close();

    if($empname == '' || empty($empname)){
        echo "{'success': false,
        'msg': '<h3 style=\"color:#b71c1c;text-align:center\">NIK NOT FOUND !<br> PLEASE SCANNING CARDNETIC</h3>'}";
    }
    else{
        try{
            $query      =  "SELECT count(*)
                            FROM [CRITICALPART].[dbo].[MC_expParts]
                            where [partLabel] = '{$expDataLabel}'";
            $rs         = $conn->Execute($query);
            $chkdata    = trim($rs->fields['0']);
            $rs->Close();

            if($chkdata != 0){
                echo "{'success': false,
                'msg': '<h3 style=\"color:#b71c1c;text-align:center\">THIS LABEL ALREADY SAVED<br> PLEASE SCANNING ANOTHER LABEL</h3>'}";
            }
            else{
                $partno   = substr($expDataLabel, 0,15);
                $po       = trim(substr($expDataLabel, 16,7));
                $qty      = trim(substr($expDataLabel, 24,5));
                $supplier = trim(substr($expDataLabel, 31,6));
                try {
                    $qsupp        = "SELECT SuppName from Supplier where SuppCode = '{$supplier}'";
                    $get_suppname = $dbs_con->Execute($qsupp);
                    $suppname     = trim($get_suppname->fields['0']);
                    $get_suppname->Close();

                    if($suppname == '' || empty($suppname)){
                        echo "{'success': false,
                        'msg': '<h3 style=\"color:#b71c1c;text-align:center\">SUPPLIER NOT FOUND !<br>WRONG SUPPLIER</h3>'}";
                    }
                    else{
                        try {
                            $qexp         ="SELECT expPeriod from [CRITICALPART].[dbo].[MC_masterExpDates] where [suppcode] = '{$supplier}'";
                            $get_expired  = $conn->Execute($qexp);
                            $expiredDate  = trim($get_expired->fields['0']);
                            $get_expired->Close();

                            if($expiredDate == '' || empty($expiredDate)){
                                echo "{'success': false,
                                'msg': '<h3 style=\"color:#b71c1c;text-align:center\">Master Expired NOT FOUND !<br>Please Check Master Expired</h3>'}";
                            }
                            else{
                                try{
                                    $qtype = "select case imincl when '1' then 'DIRECT' else 'INSPECTION' end as sts_insp from sa96t where iprod = '". $partno ."'";
                                    $rs4 = $db_qrinvoice->Execute($qtype);
                                    $sts_inspection = $rs4->fields[0];
                                    $rs4->Close();

                                    $sql = "select lokasi from stdpack where suppcode = '{$supplier}' and partnumber= '{$partno}'";
                                    $nt = $edi->Execute($sql);
                                    $location = $nt->fields[0];
                                    $rs4->Close();

                                    $query2 = "EXEC mc_insertExpData '{$nik}','{$empname}','{$expDataLabel}','{$supplier}','{$suppname}','{$partno}','{$po}','{$qty}','{$expDataInvoice}','{$expDataProdDate}','{$expiredDate}','{$expDataRemark}','{$sts_inspection}','{$location}'";
                                    $sql = $conn->Execute($query2);
                                    $sql->Close();

                                    echo "{
                                    'success': true,
                                    'msg': '<h2 style=\"text-align: center; color: green;\">Successfully save data</h2>'}"; 
                                }
                                catch(exception $e) {
                                    $var_msg = $rs->ErrorNo();
                                    $error = $conn->ErrorMsg();
                                    $error_msg = str_replace(chr(50), "", $error);
                                    echo "{'success':false,'msg':$error_msg}";
                                }
                            }
                        }
                        catch(exception $e) {
                            $var_msg = $get_expired->ErrorNo();
                            $error = $conn->ErrorMsg();
                            $error_msg = str_replace(chr(50), "", $error);

                            echo "{'success':false,'msg':$error_msg}";
                        }
                    }
                }
                catch(exception $e) {
                    $var_msg = $get_suppname->ErrorNo();
                    $error = $dbs_con->ErrorMsg();
                    $error_msg = str_replace(chr(50), "", $error);

                    echo "{'success':false,'msg':$error_msg}";
                }
            }
        }
        catch(exception $e) {
            $var_msg = $rs->ErrorNo();
            $error = $conn->ErrorMsg();
            $error_msg = str_replace(chr(50), "", $error);

            echo "{'success':false,'msg':$error_msg}";
        }
    }
}
catch(exception $e) {
    $var_msg = $rs->ErrorNo();
    $error = $db_payroll->ErrorMsg();
    $error_msg = str_replace(chr(50), "", $error);

    echo "{'success':false,'msg':$error_msg}";
}

$dbs_con->Close();
$conn->Close();
$dbs_con = NULL;
$conn = NULL;
?>
