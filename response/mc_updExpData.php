<?php
// session_start();
include '../connection.php';

$id               = isset($_REQUEST['mc_expDataID']) ? $_REQUEST['mc_expDataID'] : "";
$raw_nik          = isset($_REQUEST['mc_expDataNik']) ? $_REQUEST['mc_expDataNik'] : "";
$expDataLabel     = isset($_REQUEST['mc_expDataLabel']) ? $_REQUEST['mc_expDataLabel'] : "";
$expDataInvoice   = isset($_REQUEST['mc_expDataInvoice']) ? $_REQUEST['mc_expDataInvoice'] : "";
$expDataProdDate  = isset($_REQUEST['mc_expDataProdDate']) ? $_REQUEST['mc_expDataProdDate'] : "";
$expDataRemark    = isset($_REQUEST['mc_expDataRemark']) ? $_REQUEST['mc_expDataRemark'] : "";

if (strlen($raw_nik) == 5) {
    $nik = $raw_nik;
} else {
    $nik = substr($raw_nik,2,5);
}

try {
    $query  =  "SELECT [EMP_NAME]
                FROM [payroll].[sapayroll].[HCE_access]
                where lastday is null
                and emp_no = '{$nik}'";
    $rs     = $db_payroll->Execute($query);
    $empname= trim($rs->fields['0']);
    $rs->Close();

    if($empname == '' || empty($empname)){
        echo "{'success': false,
        'msg': '<h3 style=\"color:#b71c1c;text-align:center\">NIK NOT FOUND !<br> PLEASE SCANNING CARDNETIC</h3>'}";
    }
    else{
        try{
            $query2 = "EXEC mc_updExpData '{$id}','{$nik}','{$empname}','{$expDataInvoice}','{$expDataProdDate}','{$expDataRemark}'"; 
            $rs = $conn->Execute($query2);
            $rs->Close();

            echo "{
            'success': true,
            'msg': '<h2 style=\"text-align: center; color: green;\">Successfully editing data</h2>'}"; 
        }
        catch(exception $e) {
            $var_msg = $rs->ErrorNo();
            $error = $conn->ErrorMsg();
            $error_msg = str_replace(chr(50), "", $error);
            echo "{'success':false,'msg':$error_msg}";
        }
    }
}
catch(exception $e) {
    $var_msg = $rs->ErrorNo();
    $error = $db_payroll->ErrorMsg();
    $error_msg = str_replace(chr(50), "", $error);

    echo "{'success':false,'msg':$error_msg}";
}

$db_payroll->Close();
$conn->Close();
$db_payroll = NULL;
$conn = NULL;
?>
