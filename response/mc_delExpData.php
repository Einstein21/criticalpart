<?php
  // session_start();
  include '../connection.php';

  $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
  
  try {
    $sql = "exec [MC_delExpData] $id";
    $rs = $conn->Execute($sql);
    $rs->Close();

    echo "{
        'success': true,
        'msg': '<h2 style=\"text-align: center; color: green;\">Successfully delete data</h2>'}";
  }

  catch(exception $e) {
    $var_msg = $conn->ErrorNo();
    $error = $conn->ErrorMsg();
    $error_msg = str_replace(chr(50), "", $error);
    $err = $var_msg.'--'.$error_msg;

    echo "{'success':false,'msg':$err}";
  }
  
  $conn->Close();
  $conn = NULL;
?>
