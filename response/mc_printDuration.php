<!DOCTYPE html>
<?php
	include('../asset/phpqrcode/qrlib.php');
	include('../connection.php');

	date_default_timezone_set('Asia/Jakarta');
	$Ymd = gmdate("Ymd");
	$His = date('His');
    
    function check($key){
        $result = isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
        return $result;
    }
    
	$total 		= check('total');
	$cb 		= check('cb');
	$idduration	= explode("/",$cb);
?>
<html>
	<head>
		<style>
			body{
				margin	:0;
				padding	:0;
				clear	:both;
			}
			#mid-table{
				border-collapse: collapse;
				width		: 100%;
				font-size	: 12px;
				font-weight	: bold;
			}
			#ver-table{
				border-collapse: collapse;
				width		: 100%;
				font-size	: 12px;
			}
			
			.fontsize{
				font-size	:	15px;
				padding		:	0; 
				margin		:	0;
			}
			.fontsize_title{
				font-size	:	18px;
				padding		:	0; 
				margin		:	0;
			}
			
			</style>
	</head>
	<body>
		<table border="0">
            <?php
            $count = 0;
            function getPlural2($number, $word){
                $arr = json_decode($word,true);
                if ($number == 1){
                    $number = $number . " " . $arr["one"];
                }
                else{
                    $number = $number . " " . $arr["other"];
                }
                return $number;
            }
            function printDuration($key){
                $months = json_encode(["one" => "MONTH",
                                "other" => "MONTHS"
                            ]);
                $years = json_encode(["one" => "YEAR",
                            "other" => "YEARS"
                            ]);
                $m = $key % 12;
                $y = floor($key / 12);
                
                $result = array();
                $y >= 1 ? array_push($result,getPlural2($y,$years)) : $result;
                $m >= 1 ? array_push($result,getPlural2($m,$months)) : $result;
                return join (" AND ", $result);
            }
            function satoOpen(){
                $satoOpen = chr(27) . 'A';
                return $satoOpen;
            }
            function satoHorizontal($key){
                $horizontal = chr(27) . $key;
                return $horizontal;
            }
            function satoVertical($key){
                $vertical = chr(27) . $key;
                return $vertical;
            }
            function satoFont($key, $text){
                if($key == 'bold'){
                    $key = chr(27) . 'L0202' . chr(27) . 'S' . $text;
                }
                else{
                    $key = chr(27) . 'L0101' . chr(27) . 'M' . $text;
                }
                return $key;
            }
            function satoQrCode($key){
                $qrCode = chr(27) . '2D30,L,03,0,0' . chr(27) . 'DS2,' . $key;
                return $qrCode;
            }
            function satoClose(){
                $satoClose = chr(27) . 'Q1' . chr(27) . 'Z';
                return $satoClose;
            }
            function printLabel($qrcode, $text){
                $qrcode_label = array();
                array_push($qrcode_label,satoOpen());
                array_push($qrcode_label,$qrcode);
                array_push($qrcode_label,$text);
                array_push($qrcode_label,satoClose());
                
                return join("",$qrcode_label);
            }
            $ip		= getenv("REMOTE_ADDR");
            $host	= gethostbyaddr($_SERVER['REMOTE_ADDR']);

            function satoPrint($ip,$host, $filename,$satoCode){
                $ip == '10.230.30.125' ? $host= 'newedp5' : $host;
                $ip == '10.230.36.3' ? $host= 'mc46' : $host;

                $myfile = fopen("\\\\$host\\PrintSato\\print_".$filename.".txt","w") or die("Unable to open file! ".error_get_last());
                $txt 	= $satoCode;
                fwrite($myfile, $txt);
                fclose($myfile);
            }
            for ($k=1;$k<=$total;$k++) {
                echo "<tr>";
                for ($j=1;$j<=1;$j++) {
                    echo "<td>";
                    if($count < $total){
                        $count;
                        
                        //	select data
                        $query = "SELECT DURATION FROM [CRITICALPART].dbo.[MC_expAfterBakings] where id = {$idduration[$count]}";
                        $rs = $conn->Execute($query);

                        $DURATION	    = $rs->fields['0'];
                        $exist	 	    = $rs->RecordCount();
                        $textDuration   = printDuration($DURATION);
                        
                        //	create qrcode
                        if($exist >= 1){
                            $tempDir = '../img_qrcode/';
                            $qrname = 'Duration'.$DURATION.'.png';
                            QRcode::png($DURATION, $tempDir . $qrname, QR_ECLEVEL_L, 3);
                        }
                        $titleLabel = 'AFTER BAKING EXPIRED :';
                        echo '<table width="430px" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="50px" rowspan="2">
                                        <img style="max-height: 50px;" src="../img_qrcode/'.$qrname.'" />
                                    </td>
                                    <td>
                                        &nbsp; '.$titleLabel.'
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fontsize_title">
                                        &nbsp; <b>'.$textDuration.'</b>
                                    </td>
                                </tr>
                            </table>';
                        
                        $qrcode = satoHorizontal('H0050') . satoVertical('V0040') . satoQrCode($DURATION);
                        $text   = satoHorizontal('H0180') . satoVertical('V0030') . satoFont('normal',$titleLabel);
                        $text  .= satoHorizontal('H0180') . satoVertical('V0075') . satoFont('bold',$textDuration);
                        $label  = printLabel($qrcode, $text);
                        
                        satoPrint($ip, $host,$DURATION, $label);
                    }
                    echo "</td>";
                    ++$count;
                }
                echo "</tr>";
            }
            
            
            $conn->Close();
            ?>
		</table>
	</body>
</html>