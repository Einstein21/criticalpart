<!DOCTYPE html>
<?php
//# saat release ganti 
//#	1.	$db_jnccIT >>> $dbjncc
//#	2. 	call dbtes_jncc >>> call db_jncc
//#	3. 	include "../../adodb/con_jncc_IT.php" >>> include "../../adodb/con_jncc.php";
//#	----------------------------------------------------------------------------------
	include('../asset/phpqrcode/qrlib.php');
	include "../connection.php";

	date_default_timezone_set('Asia/Jakarta');
	$Ymd = gmdate("Ymd");
	$His = date('His');
	
	$total 		= $_REQUEST['total'];
	$cb 		= $_REQUEST['cb'];
	$idpart		= explode("/",$cb);
?>
<html>
	<head>
		<!-- <link rel="shortcut icon" href="../asset/icon/Shipping.png"/> -->
		<!-- <title>LABEL PREVIEW CHECKER - JNCC</title> -->
		<style>
			body{
				margin	:0;
				padding	:0;
				clear	:both;
			}
			#mid-table{
				border-collapse: collapse;
				width		: 100%;
				font-size	: 12px;
				font-weight	: bold;
			}
			#ver-table{
				border-collapse: collapse;
				width		: 100%;
				font-size	: 12px;
			}
			
			.fontsize{
				font-size	:	15px;
				padding		:	0; 
				margin		:	0;
			}
			.fontsize_title{
				font-size	:	18px;
				padding		:	0; 
				margin		:	0;
			}
			
			</style>
	</head>
	<body>
		<table border="0">
		<!--	<tr>
				<td>
					<img src="../asset/img/hdr_prv_ckr.png" /> <img src="../asset/img/80.png" />
				</td>
			</tr>	-->
<?php
	$count = 0;
	for ($k=1;$k<=$total;$k++) {
		echo "<tr>";
		for ($j=1;$j<=1;$j++) {
			echo "<td>";
			if($count < $total){
				$count;
				
				//	select data
				$query = "SELECT PARTLABEL, PARTNO, LOCATION, PO, QTY, SUPPNAME, INVOICE, STSINSP FROM [CRITICALPART].dbo.[MC_expParts] where id = {$idpart[$count]}";
				$rs = $conn->Execute($query);

				$PARTLABEL	= $rs->fields['0'];
				$PARTNO 	= $rs->fields['1'];
				$LOCATION 	= $rs->fields['2'];
				$PO			= $rs->fields['3'];
				$QTY		= $rs->fields['4'];
				$SUPPNAME 	= $rs->fields['5'];
				$INVOICE	= $rs->fields['6'];
				$STSINSP	= $rs->fields['7'];
				$exist	 	= $rs->RecordCount();

				//	create qrcode
				if($exist == 0){}
				else{
					//generate
					$tempDir = '../img_qrcode/';
					$qrname = substr($PARTLABEL, 30).'.png';
					QRcode::png($PARTLABEL, $tempDir . $qrname, QR_ECLEVEL_L, 3);
				}

				echo '<table width="430px" cellpadding="0" cellspacing="0">
						<tr>
							<td width="50px" rowspan="5">
								<img style="max-height: 50px;" src="../img_qrcode/'.$qrname.'" />
							</td>
							<td class="fontsize_title" colspan="2">
								&nbsp; <b>'.$PARTNO.'</b>
							</td>
							<td class="fontsize">
								<b>Loc: '.$LOCATION.'</b>
							</td>
						</tr>
						<tr>
							<td class="fontsize">
								&nbsp; PO: '.$PO.'
							</td>
							<td class="fontsize">
								QTY: '.$QTY.'
							</td>
							<td class="fontsize">
								Supp: '.substr($SUPPNAME,0,9).'
							</td>
						</tr>
						<tr>
							<td class="fontsize" colspan="2">
								&nbsp; Invc: '.$INVOICE.'
							</td>
							<td class="fontsize">
								Type: '.$STSINSP.'
							</td>
						</tr>
					</table>';
					//	create format SATO
					/*$labelke = $count+1;
					$e 		 = chr(27);
					$c	   	 = chr(053);
					$space	 = chr(32);
					$label 	 = $e.'A';
					// $barcode= $partno . $c . $qty . $c . $id;
					$barcode = $partno . $c . $id . $c . $exp;
					$sato 	 = '';
					$sato .= $e . 'A';
					$sato .= $e . 'H0040' . $e . 'V0030' . $e . '2D30,H,03,0,0' . $e . 'DS2,' . $barcode;
					$sato .= $e . 'H0180' . $e . 'V0030' . $e . 'L0202' . $e . 'S' . $partno;
					$sato .= $e . 'H0180' . $e . 'V0099' . $e . 'L0101' . $e . 'M' . 'Qty: ' . $qty;
					$sato .= $e . 'H0520' . $e . 'V0069' . $e . 'L0101' . $e . 'M' . 'Prod.: ' . $prod_date;
					$sato .= $e . 'H0520' . $e . 'V0099' . $e . 'L0101' . $e . 'XM' . 'Exp.: ' . $exp_date;
					$sato .= $e . 'H0520' . $e . 'V0030' . $e . 'L0202' . $e . 'S' . 'Critical Part';
					$sato .= $e . 'Q1';
					$sato .= $e . 'Z';
					$qrcode_label = $sato;*/
					$qrcode_label = '';
					$esc = chr(27);
					$data = '';
					$data .= $esc . 'A';
					$data .= $esc . 'H0050' . $esc . 'V0020' . $esc . '2D30,L,03,0,0' . $esc . 'DS2,' . $PARTLABEL;
					$data .= $esc . 'H0180' . $esc . 'V0015' . $esc . 'L0202' . $esc . 'S' . $PARTNO;
					$data .= $esc . 'H0500' . $esc . 'V0015' . $esc . 'L0202' . $esc . 'S' . 'Loc: ' . $LOCATION;
					$data .= $esc . 'H0180' . $esc . 'V0073' . $esc . 'L0101' . $esc . 'M' . 'PO: ' . $PO;
					$data .= $esc . 'H0370' . $esc . 'V0073' . $esc . 'L0101' . $esc . 'M' . 'QTY: ' . $QTY;
					$data .= $esc . 'H0530' . $esc . 'V0073' . $esc . 'L0101' . $esc . 'M' . 'Supp: ' . substr($SUPPNAME,0,9);
					$data .= $esc . 'H0180' . $esc . 'V0108' . $esc . 'L0101' . $esc . 'M' . 'Invc: ' . $INVOICE;
					$data .= $esc . 'H0530' . $esc . 'V0108' . $esc . 'L0101' . $esc . 'M' . 'Type: ' . $STSINSP;
					//$data .= $esc . 'H0340' . $esc . 'V0108' . $esc . 'L0101' . $esc . 'M' . $unique_unx[$j];
					//$data .= $esc . 'Q'. $qtystd .'';
					$data .= $esc . 'Q1';
					$data .= $esc . 'Z';
					$qrcode_label.= $data;
					
					$cekip		= getenv("REMOTE_ADDR");
					$host		= gethostbyaddr($_SERVER['REMOTE_ADDR']);
					// echo '<br>'.$host		= '10.230.30.117';
					
					/*if($cekip == '10.230.30.125') {
						//echo 'pake ip';
					 //	$myfile = fopen("\\\\$host\\PrintSato\\print_". $Ymd . $His . '_'.$code.'_'.$orderno.'_'.$jigno . ".txt","w") or die(error_get_last());
					 	$host 	= 'newedp5';
					 	$myfile = fopen("\\\\$host\\PrintSato\\print_".substr($PARTLABEL, 30).".txt","w") or die("Unable to open file! ".error_get_last());
					 	$txt 	= $qrcode_label;
					 	fwrite($myfile, $txt);
					 	fclose($myfile);
					} else if($cekip == '10.230.36.3') {
						//echo 'pake ip';
					 //	$myfile = fopen("\\\\$host\\PrintSato\\print_". $Ymd . $His . '_'.$code.'_'.$orderno.'_'.$jigno . ".txt","w") or die(error_get_last());
					 	$host 	= 'mc46';
					 	$myfile = fopen("\\\\$host\\PrintSato\\print_".substr($PARTLABEL, 30).".txt","w") or die("Unable to open file! ".error_get_last());
					 	$txt 	= $qrcode_label;
					 	fwrite($myfile, $txt);
					 	fclose($myfile);
					} else {*/
						//echo 'pake host';
						$myfile = fopen("\\\\$host\\PrintSato\\print_".substr($PARTLABEL, 30).".txt","w") or die("Unable to open file! ".error_get_last());
						$txt 	= $qrcode_label;
						fwrite($myfile, $txt);
						fclose($myfile);
					//}
					
			}
			echo "</td>";
			++$count;
		}
		echo "</tr>";
	}
	
	
	$conn->Close();
?>
		</table>
	</body>
</html>