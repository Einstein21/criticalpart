<?php
// session_start();
include('../asset/phpqrcode/qrlib.php');
include '../connection.php';
date_default_timezone_set("Asia/jakarta");

function check($key){
    return $check = isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
}

$raw_nik    = check('updexpnik');
$explabel   = check('updexplabel');
$expduration= check('updexpduration');
$expdate    = check('updexpdate');

if (strlen($expduration) > 0){
    $len = strlen($raw_nik);
    if($len == 5){
        $nik = $raw_nik;
    }
    else{
        $nik = substr($raw_nik, 2,5);
    }
    // $maxqty         = substr($splitlabel, 24,5);
    // $splitqty2      = intval($maxqty) - intval($splitqty);
    // $startlabel     = substr($splitlabel, 0,24);
    // $middlelabel    = substr($splitlabel, 30,22);
    // $date_temp1     = date("YmdHis");
    // $microdate_temp1= microtime();
    // $microdate_temp2= explode(" ",$microdate_temp1);
    // $microdate      = substr($microdate_temp2[0], 2, -4);
    // $date           = $date_temp1 . $microdate;
    // $sequence1      = str_pad(1,6,"0", STR_PAD_LEFT);
    // $sequence2      = str_pad(2,6,"0", STR_PAD_LEFT);
    // $qty1           = str_pad($splitqty,5," ",STR_PAD_RIGHT);
    // $qty2           = str_pad($splitqty2,5," ",STR_PAD_RIGHT);
    // $newlabel1      = $startlabel . $qty1 . ' ' . $middlelabel . $date . $sequence1;
    // $newlabel2      = $startlabel . $qty2 . ' ' . $middlelabel . $date . $sequence2;

    try{
        $query  =  "SELECT  [EMP_NAME]
                    FROM    [payroll].[sapayroll].[HCE_access]
                    where   lastday is null
                    and     emp_no = '{$nik}'";
        $rs     = $db_payroll->Execute($query);
        $empname= trim($rs->fields['0']);
        $rs->Close();

        try{
            $query3 = "EXEC mc_updExpBaking '{$nik}','{$empname}','{$explabel}','{$expduration}','{$expdate}'";
            $rs3    = $conn->Execute($query3);
            $rs3->Close();

            echo json_encode([
                "success" => true
                ,"msg" => "<h2 style='text-align: center; color: green;'>Successfully update expired date.</h2>"
            ]);
        }
        catch(exception $e) {
            $var_msg    = $conn->ErrorNo();
            $error      = $conn->ErrorMsg();
            $error_msg  = str_replace(chr(50), "", $error);
            echo json_encode([
                "success" => false
                ,"msg" => $error_msg
            ]);
        }
    }
    catch(exception $e) {
        $var_msg    = $conn->ErrorNo();
        $error      = $conn->ErrorMsg();
        $error_msg  = str_replace(chr(50), "", $error);

        echo json_encode([
            "success" => false
            ,"msg" => $error_msg
        ]);
    }
}
else if (strlen($explabel) > 0){
    try{
        $query      =  "SELECT  count(*)
                        FROM    [CRITICALPART].[dbo].[MC_expParts]
                        where   [partLabel] = '{$explabel}'";
        $rs         = $conn->Execute($query);
        $chkdata    = trim($rs->fields['0']);
        $rs->Close();

        if($chkdata == 0){
            echo json_encode([
                "success" => false
                ,"msg" => "<h3 style='color:#b71c1c;text-align:center'>LABEL NOT FOUND !<br> PLEASE SCANNING REGISTERED LABEL</h3>"
            ]);
        }
        else{
            echo json_encode([
                "success" => true
                ,"msg" => "<h2 style='text-align: center; color: green;'>label exists</h2>"
            ]);
        }
    }
    catch(exception $e) {
        $var_msg    = $conn->ErrorNo();
        $error      = $conn->ErrorMsg();
        $error_msg  = str_replace(chr(50), "", $error);
        echo "{'success':false,'msg':$error_msg}";
    }
}
else if (strlen($raw_nik) > 0){
    $len = strlen($raw_nik);
    if($len == 5){
        $nik = $raw_nik;
    }
    else{
        $nik = substr($raw_nik, 2,5);
    }
    try{
        $query  =  "SELECT  [EMP_NAME]
                    FROM    [payroll].[sapayroll].[HCE_access]
                    where   lastday is null
                    and     emp_no = '{$nik}'";
        $rs     = $db_payroll->Execute($query);
        $empname= trim($rs->fields['0']);
        $rs->Close();

        if($empname == '' || empty($empname)|| $empname == null){
            echo json_encode([
                "success" => false,
                "msg" => "<h3 style='color:#b71c1c;text-align:center'>NIK NOT FOUND !<br> PLEASE SCANNING CARDNETIC</h3>"
            ]);
        }
        else{
            echo json_encode([
                "success" => true,
                "msg" => "<h2 style='text-align: center; color: green;'>nik exists</h2>"
            ]);
        }
    }
    catch(exception $e) {
        $var_msg    = $conn->ErrorNo();
        $error      = $conn->ErrorMsg();
        $error_msg  = str_replace(chr(50), "", $error);

        echo json_encode([
            "success" => false,
            "msg" => $error_msg
        ]);
    }
}

$conn->Close();
$conn = NULL;
?>
