<?php
include '../connection.php';

function check($key){
    $result = isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
    return $result;
    }

$raw_nik                =  check('mc_expDurationNik');
$mc_expDuration         =  check('mc_expDuration');
$mc_expDurationRemark   =  check('mc_expDurationRemark');

if (strlen($raw_nik) == 5) {
    $nik = $raw_nik;
    } 
else {
    $nik = substr($raw_nik,2,5);
    }

try {
    $query = "SELECT [EMP_NAME]
              FROM [payroll].[sapayroll].[HCE_access]
              where lastday is null
              and emp_no = '{$nik}'";
    $rs     = $db_payroll->Execute($query);
    $empname= trim($rs->fields['0']);
    $rs->Close();

    if($empname == '' || empty($empname)){
        echo json_encode([
            "success" => false,
            "msg" => '<h3 style=\"color:#b71c1c;text-align:center\">NIK NOT FOUND !<br> PLEASE SCANNING CARDNETIC</h3>'
            ]);
        }
    else{
        try {
            $query_duration = "SELECT count(*) from [CRITICALPART].dbo.[MC_expAfterBakings] where duration = {$mc_expDuration} ";
            $get_duration   = $conn->Execute($query_duration);
            $duration       = trim($get_duration -> fields['0']);
            $get_duration->Close();

            if ($duration > 0){
                echo json_encode([
                    "success" => false,
                    "msg" => "<h2 style='text-align: center; color: red;'>This Duration Already exists !</h2>"
                    ]);
                }
            else{
                try {
                    $query2 = "EXEC mc_insertExpDuration '{$nik}','{$empname}',{$mc_expDuration},'{$mc_expDurationRemark}'";
                    $sql    = $conn->Execute($query2);
                    $sql->Close();
                    
                    echo json_encode([
                        "success" => true,
                        "msg" => "<h2 style='text-align: center; color: green;'>Successfully save data</h2>"
                        ]);
                    }
        
                catch(exception $e) {
                    $var_msg    = $conn->ErrorNo();
                    $error      = $conn->ErrorMsg();
                    $error_msg  = str_replace(chr(50), "", $error);
                    
                    echo json_encode([
                        "success" => false,
                        "msg" => $error_msg
                        ]);
                    }
            }
        }
    
        catch(exception $e) {
            $var_msg = $conn->ErrorNo();
            $error = $conn->ErrorMsg();
            $error_msg = str_replace(chr(50), "", $error);
            echo json_encode([
                "success" => false,
                "msg" => $error_msg
                ]);
            }
        }
    }
catch(exception $e) {
    $var_msg    = $rs->ErrorNo();
    $error      = $conn->ErrorMsg();
    $error_msg  = str_replace(chr(50), "", $error);

    echo json_encode([
        "success" => false,
        "msg" => $error_msg
        ]);
    }

$db_payroll->Close();
$db_payroll = NULL;
$conn->Close();
$conn = NULL;
?>
