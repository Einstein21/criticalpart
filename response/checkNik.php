<?php
// session_start();
include '../connection.php';
date_default_timezone_set('Asia/Jakarta');
function check($key){
    $request = isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
    return $request;
}
$raw_nik = check('issueNik');
$len = strlen($raw_nik);
if($len == 5){
    $nik = $raw_nik;
}
else{
    $nik = substr($raw_nik, 2,5);
}

try{
    $query  =  "SELECT  [EMP_NAME]
                FROM    [payroll].[sapayroll].[HCE_access]
                where   lastday is null
                and     emp_no = '{$nik}'";
    $rs     = $db_payroll->Execute($query);
    $empname1= trim($rs->fields['0']);
    $rs->Close();
    if (!$empname1){
        echo json_encode([
            "success" => false
            ,"msg"  => "<font style='font-size:25px;color:red;'>NIK not Found !"
            ,"query" => $query
            ,"empName" => $empname1
            ,"rawnik" =>$raw_nik
        ]);
    }
    else{
        echo json_encode([
            "success" => true
            ,"msg"  => "<font style='font-size:25px;color:green;'>NIK Correctly"
            ,"query" => $query
            ,"empName" => $empname1
            ,"rawnik" =>$raw_nik
        ]);
    }
}
catch(exception $e) {
    $var_msg    = $conn->ErrorNo();
    $error      = $conn->ErrorMsg();
    $error_msg  = str_replace(chr(50), "", $error);
    
    echo json_encode([
        "success" => false
        ,"msg"  => $error_msg
        ,"query" => $query
        ,"rawnik" =>$raw_nik
    ]);
}

$rs->Close();
$db_payroll->Close();
$db_payroll = NULL;

?>
