<?php
// session_start();
include '../connection.php';
date_default_timezone_set('Asia/Jakarta');
function check($key){
    $request = isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
    return $request;
}
$movJobno         = check('movingJobno');
$movJobdate       = check('movingJobdate');
$movJobtime       = check('movingJobtime');
$movJobfile       = check('movingJobfile');
$movJobmodelname  = check('movingJobmodelname');
$movJobpwbno      = check('movingJobpwbno');
$movJobline       = check('movingJobline');
$movJoblotsize    = check('movingJoblotsize');
$movJobstartserial= check('movingJobstartserial');
$movJoblotno      = check('movingJoblotno');
$movJobpwbname    = check('movingJobpwbname');
$movJobcavity     = check('movingJobcavity');
$movJobmcrh       = check('movingJobmcrh');
$raw_nik          = check('movingNik');
// $action           = check('movingAction');
$len = strlen($raw_nik);
if($len == 5){
    $nik = $raw_nik;
}
else{
    $nik = substr($raw_nik, 2,5);
}

try{
    $query  =  "SELECT  [EMP_NAME]
                FROM    [payroll].[sapayroll].[HCE_access]
                where   lastday is null
                and     emp_no = '{$nik}'";
    $rs     = $db_payroll->Execute($query);
    $empname2= trim($rs->fields['0']);
    $rs->Close();

    if (!$empname2){
        echo json_encode([
            "success" => false
            ,"msg"  => "<font style='font-size:25px;color:red;'>NIK not Found !"
            ,"query" => $query
            ,"empName" => $empname2
            ,"rawnik" =>$raw_nik
        ]);
    }
    else{
        // echo json_encode([
        //     "success" => true
        //     ,"msg"  => "NIK CORRECT"
        // ]);
        try{
            $queryCheckData = "EXEC mc_movOll '{$nik}','{$empname2}','{$movJobno}','{$movJobdate}','{$movJobtime}','{$movJobmodelname}'";
            $rsCheckData = $conn->Execute($queryCheckData);
            $updateOLL = $rsCheckData->fields[0];
            $rsCheckData->Close();
            
            if($updateOLL == 2){
                try {
                    //code...
                    $firebird   = "UPDATE jobheaderinfo SET JOBMCISSUE = 1 WHERE JOBDATE = '{$movJobdate}' and JOBNO = '{$movJobno}'";
                    $rs3        = $db_outset->Execute($firebird);
                    $rs3->Close();
                    echo json_encode([
                        "success" => true
                        ,"msg"  => "Issue Started"
                    ]);
                } catch (exception $e) {
                    //throw $th;
                    $var_msg    = $db_outset->ErrorNo();
                    $error      = $db_outset->ErrorMsg();
                    $error_msg  = str_replace(chr(50), "", $error);
                    
                    echo json_encode([
                        "success" => false
                        ,"msg"  => $error_msg
                    ]);
                }
            }
            else{
                echo json_encode([
                    "success" => true
                    ,"msg"  => "Issue Started"
                ]);
            }
            
        }
        catch(exception $e) {
            $var_msg    = $conn->ErrorNo();
            $error      = $conn->ErrorMsg();
            $error_msg  = str_replace(chr(50), "", $error);
            
            echo json_encode([
                "success" => false
                ,"msg"  => $error_msg
            ]);
        }
    }
}
catch(exception $e) {
    $var_msg    = $conn->ErrorNo();
    $error      = $conn->ErrorMsg();
    $error_msg  = str_replace(chr(50), "", $error);
    
    echo json_encode([
        "success" => false
        ,"msg"  => $error_msg
        ,"query" => $queryCheckData
        ,"action" => $action
        ,"rawnik" =>$raw_nik
    ]);
}

$db_payroll->Close();
$db_payroll = NULL;
$db_outset->Close();
$db_outset = NULL;
$conn->Close();
$conn = NULL;



?>
