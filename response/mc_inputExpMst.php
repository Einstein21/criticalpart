<?php
  // session_start();
  include '../connection.php';
  
  $raw_nik  = isset($_REQUEST['mc_expMstNik']) ? $_REQUEST['mc_expMstNik'] : " ";
  $supplier = isset($_REQUEST['mc_expMstSupplier']) ? $_REQUEST['mc_expMstSupplier'] : " ";
  $duration = isset($_REQUEST['mc_expMstDuration']) ? $_REQUEST['mc_expMstDuration'] : " ";
  $remark   = isset($_REQUEST['mc_expMstRemark']) ? $_REQUEST['mc_expMstRemark'] : " ";
  
  if (strlen($raw_nik) == 5) {
    $nik = $raw_nik;
  } else {
    $nik = substr($raw_nik,2,5);
  }
  
  try {
    $query = "SELECT [EMP_NAME]
              FROM [payroll].[sapayroll].[HCE_access]
              where lastday is null
              and emp_no = '{$nik}'";
    $rs = $db_payroll->Execute($query);
    $empname = trim($rs->fields['0']);
    $rs->Close();

    if($empname == '' || empty($empname)){
      echo "{'success': false,
            'msg': '<h3 style=\"color:#b71c1c;text-align:center\">NIK NOT FOUND !<br> PLEASE SCANNING CARDNETIC</h3>'}";
    }
    else{
      try {
        $get_suppname = $dbs_con->Execute("SELECT SuppName from Supplier where SuppCode = '{$supplier}' ");
        $suppname = trim($get_suppname->fields['0']);
        $get_suppname->Close();

        $query2 = "EXEC mc_insertExpMst '{$nik}','{$empname}','{$supplier}','{$suppname}','{$duration}','{$remark}'";
        $sql = $conn->Execute($query2);
        $sql->Close();

        echo "{
        'success': true,
        'msg': '<h2 style=\"text-align: center; color: green;\">Successfully save data</h2>'}";
      }

      catch(exception $e) {
        $var_msg = $conn->ErrorNo();
        $error = $conn->ErrorMsg();
        $error_msg = str_replace(chr(50), "", $error);

        echo "{'success':false,'msg':$error_msg}";
      }
    }
  }
  catch(exception $e) {
    $var_msg = $rs->ErrorNo();
    $error = $conn->ErrorMsg();
    $error_msg = str_replace(chr(50), "", $error);

    echo "{'success':false,'msg':$error_msg}";
  }

  $dbs_con->Close();
  $conn->Close();
  $dbs_con = NULL;
  $conn = NULL;
?>
