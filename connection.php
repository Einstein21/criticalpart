<?php
include 'adodb/adodb.inc.php';
// include 'adodb/adodb-errorhandler.inc.php';
include 'adodb/adodb-exceptions.inc.php';
include 'adodb/adodb-errorpear.inc.php';

$dbasetype  = 'odbc_mssql';

// CONNECTION SVRDBN TRC CRITICAL PART
$source1    = '136.198.117.48\JEINSQL2012trc'; //kalau sudah selesai paki ini
// $source1    = 'SVRDBT\SQL2012';
$dbase1    = 'CRITICALPART';
$user1     = 'sa';
$pass1     = 'JvcSql@123';
$server1   = "Driver={SQL Server};Server=$source1;Database=$dbase1;";
// $server = "Driver={SQL Server};Server=SVRDBT\SQL2012;Database=$dbase;";
$conn     = ADONewConnection($dbasetype);
$conn->Connect($server1, $user1, $pass1);

// CONNECTION SVRDBN JVC for User SMT
$source2    = '136.198.117.48\JEINSQL2012';
$dbase2   = 'JVC';
$user2     = 'sa';
$pass2     = 'JvcSql@123';
$server2  = "Driver={SQL Server};Server=$source2;Database=$dbase2;";
$jvc     = ADONewConnection($dbasetype);
$jvc->Connect($server2, $user2, $pass2);

// CONNECTION SVRDBN EDI for stdPack
$source6    = '136.198.117.48\JEINSQL2012';
$dbase6   = 'EDI';
$user6     = 'sa';
$pass6     = 'JvcSql@123';
$server6  = "Driver={SQL Server};Server=$source6;Database=$dbase6;";
$edi     = ADONewConnection($dbasetype);
$edi->Connect($server6, $user6, $pass6);


// CONNECTION SVRDBN P SMTZDBS
$source3    = '136.198.117.48\JEINSQL2012P';
$dbase3   = 'SMTZDBS';
$user3     = 'sa';
$pass3     = 'JvcSql@123';
$server3   = "Driver={SQL Server};Server=$source3;Database=$dbase3;";
$dbnp_conn   = ADONewConnection($dbasetype);
$dbnp_conn->Connect($server3, $user3, $pass3);


// CONNECTION SVRDBZ S EDI
// $source4    = 'SVRDBZ\JeinSql2017S';
$source4    = '136.198.117.80\JeinSql2017S';
$dbase4   = 'EDI';
$user4     = 'sa';
$pass4     = 'password';
$server4   = "Driver={SQL Server};Server=$source4;Database=$dbase4;";
$dbs_con   = ADONewConnection($dbasetype);
$dbs_con->Connect($server4, $user4, $pass4);

// CONNECTION SVRDBZ P payroll check user
// $source5    = 'SVRDBZ\JeinSql2017P';
$source5    = '136.198.117.80\JeinSql2017P';
$dbase5   = 'payroll';
$user5     = 'sa';
$pass5     = 'JvcSql@123';
$server5   = "Driver={SQL Server};Server=$source5;Database=$dbase5;";
$db_payroll  = ADONewConnection($dbasetype);
$db_payroll->Connect($server5, $user5, $pass5);

// CONNECTION QRINVOICE
include('../../ADODB/con_qrinvoice.php');

// CONNECTION OLL
// include('../../ADODB/con_part_im.php');
$dbase6   = 'OCS_OUTSET';
$user6     = 'SYSDBA';
$pass6     = 'masterkey';
$dbasetype2  = 'odbc';
$db_outset  = ADONewConnection($dbasetype2);
$db_outset->Connect($dbase6, $user6, $pass6);

// Server in the this format: <computer>\<instance name> or
// <server>,<port> when using a non default port number
// $conn = ADONewConnection('mssql');
// $conn->Execute('136.198.117.48\JEINSQL2012', 'sa', 'JvcSql@123', 'db_jncp') or die ("not connecting");
